/*
--------------------------------------------------------------------
EXAM OPTIONS
--------------------------------------------------------------------
*/
// ADD EXAM
function addExam() {

  var errors = [];
  var values = '';

  // exam name
  if (document.forms["myForm"]["name"].value == '') {
    errors.push('<p class="error">class name can\'t be empty</p>');
  }else {
    var name = document.forms["myForm"]["name"].value;
  }

  // term
  if (document.forms["myForm"]["term"].value == '') {
    errors.push('<p class="error">term can\'t be empty</p>');
  }else {
    var term = document.forms["myForm"]["term"].value;
  }

  // if no errors exist
  if (errors == false) {

    // convert to json string format
    var exam_string = '{"name":"'+name+'", "term":"'+term+'"}';

    // push json string to php file
    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("main-message").innerHTML = this.responseText;
                document.getElementById('main').style.background = '#34ca66';
                $(document).ready(function(){
                  $('.post-form').hide();
                  document.forms["myForm"]["name"].value = '';
                });
                window.setTimeout(function(){
                  window.location = '../exams';
                }, 2000);
            }
        };

        xmlhttp.open("POST", "../../../engine/infused_cogs.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("add-exam=" + exam_string);

  }else {
    // if errors exist echo out errors
    for (var i in errors) {
      values += errors[i];
    }
    document.getElementById('messages').innerHTML = values;
  }

  return false;
}


// GET EXAM DATA
function getExamData(value){
  // reset messages to null
  document.getElementById('messages').innerHTML = '';

  $(document).ready(function(){
    $('.add').hide();
    $('.edit').show();
  });

  // get parent info
  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            parentObject = JSON.parse(this.responseText);

            // assign to respective fields
            document.forms["myForm"]["id"].value = parentObject.id;
            document.forms["myForm"]["name"].value = parentObject.name;
            var a = document.getElementById('term').selectedIndex;
            var b = document.getElementById('term').options;
            b[a].text = 'Term ' + parentObject.term;
            b[a].value = parentObject.term;
          }
      };

    xmlhttp.open("GET", "../../../engine/infused_cogs.php?get_exam_data="+value, true);
    xmlhttp.send();

  return false;
}

// EDIT EXAM
function editExam(){
  var errors = [];                                              // errors storage
  var values = '';

  // exam name
  if (document.forms["myForm"]["name"].value == '') {
    errors.push('<p class="error">class name can\'t be empty</p>');
  }else {
    var name = document.forms["myForm"]["name"].value;
  }

  // term
  if (document.forms["myForm"]["term"].value == '') {
    errors.push('<p class="error">term can\'t be empty</p>');
  }else {
    var term = document.forms["myForm"]["term"].value;
  }

  // if no errors exist
  if (errors == false) {
    var id = document.forms["myForm"]["id"].value;

    // convert to json string format
    var term_string = '{"id":"'+id+'", "name":"'+name+'", "term":"'+term+'"}';

    // push json string to php file
    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("main-message").innerHTML = this.responseText;
                document.getElementById('main').style.background = '#34ca66';

                $(document).ready(function(){
                  $('.post-form').hide();
                  document.forms["myForm"]["id"].value = '';
                  document.forms["myForm"]["name"].value = '';
                });
                window.setTimeout(function(){
                  window.location = '../exams';
                }, 2000);
            }
        };

        xmlhttp.open("POST", "../../../engine/infused_cogs.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("edit-exam-update=" + term_string);

  }else {
    // if errors exist echo out errors
    for (var i in errors) {
      values += errors[i];
    }
    document.getElementById('messages').innerHTML = values;
  }

  return false;
}

// DELETE EXAM
function deleteExam(){
  var id = document.forms["deleteForm"]["id"].value;

  // get parent info
  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            document.getElementById('main-message').innerHTML = this.responseText;
            document.getElementById('main').style.background = '#34ca66';
            $(document).ready(function(){
              $('.delete-form').hide();
            });
            window.setTimeout(function(){
              window.location = '../exams';
            }, 2000);
          }
      };

  xmlhttp.open("GET", "../../../engine/infused_cogs.php?delete_exam="+id, true);
  xmlhttp.send();

  return false;
}

// get Deletion value
function deleteValue(value){
  var id = value;

  document.forms["deleteForm"]["id"].value = id;

  return false;
}

// WHEN CLOSE IT IS CLICKED
$(document).ready(function(){
  $('.closeit').click(function(){
    document.forms["myForm"]["id"].value = '';
    var a = document.getElementById('term').selectedIndex;
    var b = document.getElementById('term').options;
    b[a].text = 'choose term';
    b[a].value = '';
  });
});
