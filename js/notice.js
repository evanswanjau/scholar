/*
--------------------------------------------------------------------------------
NOTICE FUNCTIONS /////////////////////////////////////////////////////
--------------------------------------------------------------------------------
*/

// ADD NOTICE
function addNotice() {

  var errors = [];                                              // errors storage
  var values = '';

  // name
  if (document.forms["myForm"]["notice"].value == '') {
    errors.push('<p class="error">notice can\'t be empty</p>');
  }else {
    var notice = document.forms["myForm"]["notice"].value;
  }

  // if no errors exist
  if (errors == false) {

    // convert to json string format
    var notice_string = '{"notice":"'+notice+'"}';

    // push json string to php file
    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("main-message").innerHTML = this.responseText;
                document.getElementById('main').style.background = '#34ca66';
                $(document).ready(function(){
                  $('.post-form').hide();
                  document.forms["myForm"]["notice"].value = '';
                });
                window.setTimeout(function(){
                  window.location = '../noticeboard';
                }, 2000);
            }
        };

        xmlhttp.open("POST", "../../../engine/infused_cogs.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("add-notice=" + notice_string);

  }else {
    // if errors exist echo out errors
    for (var i in errors) {
      values += errors[i];
    }
    document.getElementById('messages').innerHTML = values;
  }

  return false;
}


// GET NOTICE DATA
function getNoticeData(value){
  // reset messages to null
  document.getElementById('messages').innerHTML = '';

  $(document).ready(function(){
    $('.add').hide();
    $('.edit').show();
  });

  // get parent info
  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {

            parentObject = JSON.parse(this.responseText);

            // assign to respective fields
            document.forms["myForm"]["id"].value = parentObject.id;
            document.forms["myForm"]["notice"].value = parentObject.notice;
          }
      };

  xmlhttp.open("GET", "../../../engine/infused_cogs.php?get_notice_data="+value, true);
  xmlhttp.send();

  return false;
}

// EDIT NOTICE
function editNotice(){
  var errors = [];                                              // errors storage
  var values = '';

  // name
  if (document.forms["myForm"]["notice"].value == '') {
    errors.push('<p class="error">name can\'t be empty</p>');
  }else {
    var notice = document.forms["myForm"]["notice"].value;
  }

  // if no errors exist
  if (errors == false) {
    var id = document.forms["myForm"]["id"].value;
    // convert to json string format
    var notice_string = '{"id":"'+id+'", "notice":"'+notice+'"}';

    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("main-message").innerHTML = this.responseText;
                document.getElementById('main').style.background = '#34ca66';
                $(document).ready(function(){
                  $('.post-form').hide();
                  document.forms["myForm"]["notice"].value = '';
                });
                window.setTimeout(function(){
                  window.location = '../noticeboard';
                }, 2000);
            }
        };

        xmlhttp.open("POST", "../../../engine/infused_cogs.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("edit-notice-update=" + notice_string);

  }else {
    // if errors exist echo out errors
    for (var i in errors) {
      values += errors[i];
    }
    document.getElementById('messages').innerHTML = values;
  }

  return false;
}

// DELETE NOTICE
function deleteNotice(){
  var id = document.forms["deleteForm"]["id"].value;

  // get parent info
  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            document.getElementById('main-message').innerHTML = this.responseText;
            document.getElementById('main').style.background = '#34ca66';
            $(document).ready(function(){
              $('.delete-form').hide();
            });
            window.setTimeout(function(){
              window.location = '../noticeboard';
            }, 2000);
          }
      };

  xmlhttp.open("GET", "../../../engine/infused_cogs.php?delete_notice="+id, true);
  xmlhttp.send();

  return false;
}


// get Deletion value
function deleteValue(value){
  var id = value;

  document.forms["deleteForm"]["id"].value = id;

  return false;
}

// WHEN CLOSE IT IS CLICKED
$(document).ready(function(){
  $('.closeit').click(function(){
    document.forms["myForm"]["notice"].value = '';
  });
});
