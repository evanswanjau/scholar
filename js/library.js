/*
--------------------------------------------------------------------------------
LIBRARY FUNCTIONS ///////////////////////////////////////////////////////////////
--------------------------------------------------------------------------------
*/

// ADD BOOK
function addBook() {

  var errors = [];                                              // errors storage
  var values = '';

  // book name
  if (document.forms["myForm"]["name"].value == '') {
    errors.push('<p class="error">book name can\'t be empty</p>');
  }else {
    var bookname = document.forms["myForm"]["name"].value;
  }

  // price
  if (document.forms["myForm"]["price"].value == '') {
    errors.push('<p class="error">price can\'t be empty</p>');
  }else {
    var price = document.forms["myForm"]["price"].value;
  }

  // number of books
  if (document.forms["myForm"]["count"].value == '') {
    errors.push('<p class="error">number of books can\'t be empty</p>');
  }else {
    var count = document.forms["myForm"]["count"].value;
  }


  // if no errors exist
  if (errors == false) {
    var author = document.forms["myForm"]["author"].value;
    var description = document.forms["myForm"]["description"].value;

    // convert to json string format
    var book_string = '{"bookname":"'+bookname+'", "author":"'+author+'", "description":"'+description+'", "price":"'+price+'", "count":"'+count+'"}';
    // push json string to php file
    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("main-message").innerHTML = this.responseText;
                document.getElementById('main').style.background = '#34ca66';
                $(document).ready(function(){
                  $('.post-form').hide();
                  document.forms["myForm"]["name"].value = '';
                  document.forms["myForm"]["author"].value = '';
                  document.forms["myForm"]["description"].value = '';
                  document.forms["myForm"]["price"].value = '';
                  document.forms["myForm"]["count"].value = '';
                });
                window.setTimeout(function(){
                  window.location = '../books';
                }, 2000);
            }
        };

        xmlhttp.open("POST", "../../../../../../engine/infused_cogs.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("add-book=" + book_string);

  }else {
    // if errors exist echo out errors
    for (var i in errors) {
      values += errors[i];
    }
    document.getElementById('messages').innerHTML = values;
  }

  return false;
}

// GET BOOK DATA
function getBookData(value){
  // reset messages to null

  $(document).ready(function(){
    $('.add').hide();
    $('.edit').show();
  });

  // get parent info
  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            parentObject = JSON.parse(this.responseText);

            // assign to respective fields
            document.forms["myForm"]["id"].value = parentObject.id;
            document.forms["myForm"]["name"].value = parentObject.name;
            document.forms["myForm"]["author"].value = parentObject.author;
            document.forms["myForm"]["description"].value = parentObject.description;
            document.forms["myForm"]["price"].value = parentObject.price;
            document.forms["myForm"]["count"].value = parentObject.count;
          }
      };

  xmlhttp.open("GET", "../../../../../../engine/infused_cogs.php?get_book_data="+value, true);
  xmlhttp.send();

  return false;
}

 //EDIT PARENT
function editBook(){
  var errors = [];                                              // errors storage
  var values = '';

  // book name
  if (document.forms["myForm"]["name"].value == '') {
    errors.push('<p class="error">book name can\'t be empty</p>');
  }else {
    var bookname = document.forms["myForm"]["name"].value;
  }

  // price
  if (document.forms["myForm"]["price"].value == '') {
    errors.push('<p class="error">price can\'t be empty</p>');
  }else {
    var price = document.forms["myForm"]["price"].value;
  }

  // number of books
  if (document.forms["myForm"]["count"].value == '') {
    errors.push('<p class="error">number of books can\'t be empty</p>');
  }else {
    var count = document.forms["myForm"]["count"].value;
  }

  // if no errors exist
  if (errors == false) {
    var book_id = document.forms["myForm"]["id"].value;
    var author = document.forms["myForm"]["author"].value;
    var description = document.forms["myForm"]["description"].value;

    // convert to json string format
    var book_string = '{"book_id":"'+book_id+'", "bookname":"'+bookname+'", "author":"'+author+'", "description":"'+description+'", "price":"'+price+'", "count":"'+count+'"}';

    // push json string to php file
    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("main-message").innerHTML = this.responseText;
                document.getElementById('main').style.background = '#34ca66';
                $(document).ready(function(){
                  $('.post-form').hide();
                  document.forms["myForm"]["id"].value = '';
                  document.forms["myForm"]["name"].value = '';
                  document.forms["myForm"]["author"].value = '';
                  document.forms["myForm"]["description"].value = '';
                  document.forms["myForm"]["price"].value = '';
                  document.forms["myForm"]["count"].value = '';
                });
                window.setTimeout(function(){
                  window.location = '../books';
                }, 2000);
            }
        };

        xmlhttp.open("POST", "../../../../../../engine/infused_cogs.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("edit-book-update=" + book_string);

  }else {
    // if errors exist echo out errors
    for (var i in errors) {
      values += errors[i];
    }
    document.getElementById('messages').innerHTML = values;
  }

  return false;
}

// DELETE BOOK
function deleteBook(){
  var id = document.forms["deleteForm"]["id"].value;

  // get parent info
  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            document.getElementById('main-message').innerHTML = this.responseText;
            document.getElementById('main').style.background = '#34ca66';

            $(document).ready(function(){
              $('.delete-form').hide();
            });
            window.setTimeout(function(){
              window.location = '../books';
            }, 2000);
          }
      };

  xmlhttp.open("GET", "../../../../../../engine/infused_cogs.php?delete_book="+id, true);
  xmlhttp.send();

  return false;
}

// get Deletion value
function deleteValue(value){
  var id = value;

  document.forms["deleteForm"]["id"].value = id;

  return false;
}


// GET BOOK DATA
function getBookAssignData(value){
  document.getElementById('messages').innerHTML = 'pepepepe';

  // get book info
  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            parentObject = JSON.parse(this.responseText);
            document.forms["issueForm"]["bookid"].value = parentObject.id;
            document.forms["issueForm"]["serialcode"].value = parentObject.code;
            document.getElementById('code').innerHTML = '<b>Issue Code: </b>' + parentObject.code;
            document.getElementById('bookname').innerHTML = '<b>Book Name: </b>' + parentObject.name;
            document.getElementById('author').innerHTML = '<b>Author: </b>' + parentObject.author;
            document.getElementById('description').innerHTML = '<b>Description: </b>' + parentObject.description;
          }
      };

  xmlhttp.open("GET", "../../../../../../engine/infused_cogs.php?get_book_data="+value, true);
  xmlhttp.send();

  return false;
}

function getAllStudentList(value){

  // get book info
  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            if (value != "") {
              document.getElementById('student_issued').innerHTML = this.responseText;
            }else {
              document.getElementById('student_issued').innerHTML = "";
            }
          }
      };

  xmlhttp.open("GET", "../../../../../../engine/infused_cogs.php?get_student_search_data="+value, true);
  xmlhttp.send();

  return false;
}

function assignBook(value){

  document.forms["issueForm"]["studentid"].value = value.id;
  document.forms["issueForm"]["adm"].value = value.adm;
  document.forms["issueForm"]["studentname"].value = value.name;

  document.getElementById('student_issued').innerHTML = "";
  document.forms["issueForm"]["studentsearch"].value = "";

  return false;
}


function issueBook() {
  var errors = [];                                              // errors storage
  var values = '';

  // book name
  if (document.forms["issueForm"]["studentname"].value == '') {
    errors.push('<p class="error">student name can\'t be empty</p>');
  }else {
    var studentname = document.forms["issueForm"]["studentname"].value;
  }

  // price
  if (document.forms["issueForm"]["adm"].value == '') {
    errors.push('<p class="error">admission number can\'t be empty</p>');
  }else {
    var adm = document.forms["issueForm"]["adm"].value;
  }

  // if no errors exist
  if (errors == false) {
    var studentid = document.forms["issueForm"]["studentid"].value;
    var bookid = document.forms["issueForm"]["bookid"].value;
    var serialcode = document.forms["issueForm"]["serialcode"].value;

    //document.getElementById('issuemessages').innerHTML = studentid;

    // convert to json string format
    var issue_string = '{"serialcode":"'+serialcode+'", "studentid":"'+studentid+'", "bookid":"'+bookid+'"}';
    // push json string to php file
    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {

                document.getElementById("main-message").innerHTML = this.responseText;
                document.getElementById('main').style.background = '#34ca66';

                $(document).ready(function(){
                  $('.issue-form').hide();
                  document.forms["issueForm"]["adm"].value = '';
                  document.forms["issueForm"]["studentname"].value = '';
                  document.forms["issueForm"]["studentid"].value = '';
                  document.forms["issueForm"]["bookid"].value = '';
                  document.forms["issueForm"]["serialcode"].value = '';
                });
                window.setTimeout(function(){
                  window.location = '../books';
                }, 2000);
            }
        };

        xmlhttp.open("POST", "../../../../../../engine/infused_cogs.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("issue_book=" + issue_string);

  }else {
    // if errors exist echo out errors
    for (var i in errors) {
      values += errors[i];
    }
    document.getElementById('issuemessages').innerHTML = values;
  }

  return false;
}

// RETURN BOOK FUNCTION
function returnBook(){
  value = document.forms["deleteForm"]["id"].value;

  // get book info
  var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
        document.getElementById("main-message").innerHTML = this.responseText;
        document.getElementById('main').style.background = '#34ca66';

        $(document).ready(function(){
          $('.delete-form').hide();
          document.forms["issueForm"]["id"].value = '';
        });
        window.setTimeout(function(){
          window.location = '../books';
        }, 2000);
      };

  xmlhttp.open("GET", "../../../../../../engine/infused_cogs.php?return_book="+value, true);
  xmlhttp.send();

  return false;
}


$(document).ready(function(){
  $('.closeit').click(function(){
    document.forms["myForm"]["name"].value = '';
    document.forms["myForm"]["author"].value = '';
    document.forms["myForm"]["description"].value = '';
    document.forms["myForm"]["price"].value = '';
    document.forms["myForm"]["count"].value = '';
  });
});
