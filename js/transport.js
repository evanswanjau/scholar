/*
--------------------------------------------------------------------------------
TRANSPORT ROUTE  FUNCTIONS /////////////////////////////////////////////////////
--------------------------------------------------------------------------------
*/

// ADD ROUTE
function addRoute() {

  var errors = [];                                              // errors storage
  var values = '';

  // transport route
  if (document.forms["myForm"]["route"].value == '') {
    errors.push('<p class="error">transport route can\'t be empty</p>');
  }else {
    var route = document.forms["myForm"]["route"].value;
  }

  // number plate
  if (document.forms["myForm"]["plate"].value == '') {
    errors.push('<p class="error">phone number can\'t be empty</p>');
  }else {
    var plate = document.forms["myForm"]["plate"].value;
  }

  // driver
  if (document.forms["myForm"]["driver"].value == '') {
    errors.push('<p class="error">driver can\'t be empty</p>');
  }else {
    var driver = document.forms["myForm"]["driver"].value;
  }

  // phone number
  if (document.forms["myForm"]["phone"].value == '') {
    errors.push('<p class="error">phone number can\'t be empty</p>');
  }else {
    var phone = document.forms["myForm"]["phone"].value;
  }

  // one way price
  if (document.forms["myForm"]["oneway"].value == '') {
    errors.push('<p class="error">one way price can\'t be empty</p>');
  }else {
    var oneway = document.forms["myForm"]["oneway"].value;
  }

  // two way price
  if (document.forms["myForm"]["twoway"].value == '') {
    errors.push('<p class="error">two way prices can\'t be empty</p>');
  }else {
    var twoway = document.forms["myForm"]["twoway"].value;
  }

  // if no errors exist
  if (errors == false) {

    // convert to json string format
    var transport_string = '{"route":"'+route+'", "plate":"'+plate+'", "driver":"'+driver+'", "phone":"'+phone+'", "oneway":"'+oneway+'", "twoway":"'+twoway+'"}';

    // push json string to php file
    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("main-message").innerHTML = this.responseText;
                document.getElementById('main').style.background = '#34ca66';
                $(document).ready(function(){
                  $('.post-form').hide();
                  document.forms["myForm"]["route"].value = '';
                  document.forms["myForm"]["plate"].value = '';
                  document.forms["myForm"]["driver"].value = '';
                  document.forms["myForm"]["phone"].value = '';
                  document.forms["myForm"]["oneway"].value = '';
                  document.forms["myForm"]["twoway"].value = '';
                });
                window.setTimeout(function(){
                  window.location = '../transport';
                }, 2000);
            }
        };

        xmlhttp.open("POST", "../../../engine/infused_cogs.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("add-route=" + transport_string);

  }else {
    // if errors exist echo out errors
    for (var i in errors) {
      values += errors[i];
    }
    document.getElementById('messages').innerHTML = values;
  }

  return false;
}


// GET ROUTE DATA
function getRouteData(value){
  // reset messages to null
  document.getElementById('messages').innerHTML = '';

  $(document).ready(function(){
    $('.add').hide();
    $('.edit').show();
  });

  // get parent info
  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            parentObject = JSON.parse(this.responseText);
            //document.getElementById('messages').innerHTML = this.responseText;
            // assign to respective fields
            document.forms["myForm"]["id"].value = parentObject.id;
            document.forms["myForm"]["route"].value = parentObject.route;
            document.forms["myForm"]["plate"].value = parentObject.plate;
            document.forms["myForm"]["driver"].value = parentObject.driver;
            document.forms["myForm"]["phone"].value = parentObject.phone;
            document.forms["myForm"]["oneway"].value = parentObject.oneway;
            document.forms["myForm"]["twoway"].value = parentObject.twoway;
          }
      };

  xmlhttp.open("GET", "../../../engine/infused_cogs.php?get_route_data="+value, true);
  xmlhttp.send();

  return false;
}

// EDIT ROUTE
function editRoute(){
  var errors = [];                                              // errors storage
  var values = '';

  // transport route
  if (document.forms["myForm"]["route"].value == '') {
    errors.push('<p class="error">transport route can\'t be empty</p>');
  }else {
    var route = document.forms["myForm"]["route"].value;
  }

  // number plate
  if (document.forms["myForm"]["plate"].value == '') {
    errors.push('<p class="error">phone number can\'t be empty</p>');
  }else {
    var plate = document.forms["myForm"]["plate"].value;
  }

  // driver
  if (document.forms["myForm"]["driver"].value == '') {
    errors.push('<p class="error">driver can\'t be empty</p>');
  }else {
    var driver = document.forms["myForm"]["driver"].value;
  }

  // phone number
  if (document.forms["myForm"]["phone"].value == '') {
    errors.push('<p class="error">phone number can\'t be empty</p>');
  }else {
    var phone = document.forms["myForm"]["phone"].value;
  }

  // one way price
  if (document.forms["myForm"]["oneway"].value == '') {
    errors.push('<p class="error">one way price can\'t be empty</p>');
  }else {
    var oneway = document.forms["myForm"]["oneway"].value;
  }

  // two way price
  if (document.forms["myForm"]["twoway"].value == '') {
    errors.push('<p class="error">two way prices can\'t be empty</p>');
  }else {
    var twoway = document.forms["myForm"]["twoway"].value;
  }

  // if no errors exist
  if (errors == false) {
    var id = document.forms["myForm"]["id"].value;
    // convert to json string format
    var transport_string = '{"id":"'+id+'", "route":"'+route+'", "plate":"'+plate+'", "driver":"'+driver+'", "phone":"'+phone+'", "oneway":"'+oneway+'", "twoway":"'+twoway+'"}';

    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("main-message").innerHTML = this.responseText;
                document.getElementById('main').style.background = '#34ca66';
                $(document).ready(function(){
                  $('.post-form').hide();
                  document.forms["myForm"]["route"].value = '';
                  document.forms["myForm"]["plate"].value = '';
                  document.forms["myForm"]["driver"].value = '';
                  document.forms["myForm"]["phone"].value = '';
                  document.forms["myForm"]["oneway"].value = '';
                  document.forms["myForm"]["twoway"].value = '';
                });
                window.setTimeout(function(){
                  window.location = '../transport';
                }, 2000);
            }
        };

        xmlhttp.open("POST", "../../../engine/infused_cogs.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("edit-route-update=" + transport_string);

  }else {
    // if errors exist echo out errors
    for (var i in errors) {
      values += errors[i];
    }
    document.getElementById('messages').innerHTML = values;
  }

  return false;
}

// DELETE ROUTE
function deleteRoute(){
  var id = document.forms["deleteForm"]["id"].value;

  // get parent info
  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            document.getElementById('main-message').innerHTML = this.responseText;
            document.getElementById('main').style.background = '#34ca66';
            $(document).ready(function(){
              $('.delete-form').hide();
            });
            window.setTimeout(function(){
              window.location = '../transport';
            }, 2000);
          }
      };

  xmlhttp.open("GET", "../../../engine/infused_cogs.php?delete_route="+id, true);
  xmlhttp.send();

  return false;
}

// get Deletion value
function deleteValue(value){
  var id = value;

  document.forms["deleteForm"]["id"].value = id;

  return false;
}

// WHEN CLOSE IT IS CLICKED
$(document).ready(function(){
  $('.closeit').click(function(){
    document.forms["myForm"]["route"].value = '';
    document.forms["myForm"]["plate"].value = '';
    document.forms["myForm"]["driver"].value = '';
    document.forms["myForm"]["phone"].value = '';
    document.forms["myForm"]["oneway"].value = '';
    document.forms["myForm"]["twoway"].value = '';
  });
});
