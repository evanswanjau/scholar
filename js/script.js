$(document).ready(function(){
  $('.post-form').hide();
  $('.edit').hide();
  $('.delete-form').hide();
  $('.class-list').hide();
  $('.this-class').hide();
  $('.issue-form').hide();
  $('.book-list').hide();
  $('.invoice-list').hide();
  $('.mini-class-list').hide();
  $('.receipt-form').hide();
  $('.exam-list').hide();
  $('.term-list').hide();
  $('.exam-options').hide();

  $('.show-form').click(function(){
    $('.post-form').fadeIn();
  });

  $('.show-assign').click(function(){
    $('.issue-form').fadeIn();
  });

  $('.show-delete').click(function(){
    $('.delete-form').fadeIn();
  });

  $('.open-class').click(function(){
    $('.class-list').slideToggle();
  });

  $('.open-mini-class').click(function(){
    $('.mini-class-list').slideToggle();
  });

  $('.open-books').click(function(){
    $('.book-list').slideToggle();
  });

  $('.open-this-class').click(function(){
    $('.this-class').slideToggle();
  });

  $('.open-invoice').click(function(){
    $('.invoice-list').slideToggle();
  });

  $('.no').click(function(){
    $('.delete-form').fadeOut();
  });

  $('.show-rform').click(function(){
    $('.receipt-form').fadeIn();
  });

  $('.open-term-list').click(function(){
    $('.term-list').slideToggle();
  });

  $('.open-exam-options').click(function(){
    $('.exam-options').slideToggle();
  });

  for (var i = 1; i <= 3; i++) {
    $('.exam-list-' + i).hide();
  }

  $('.open-exam-list-1').click(function(){
    $('.exam-list-1').slideToggle();
    $('.exam-list-2').hide();
    $('.exam-list-3').hide();
  });

  $('.open-exam-list-2').click(function(){
    $('.exam-list-2').slideToggle();
    $('.exam-list-1').hide();
    $('.exam-list-3').hide();
  });

  $('.open-exam-list-3').click(function(){
    $('.exam-list-3').slideToggle();
    $('.exam-list-1').hide();
    $('.exam-list-2').hide();
  });

  $('.closeit').click(function(){
    $('.add').show();
    $('.edit').hide();
    $('.post-form').fadeOut();
    $('.delete-form').fadeOut();
    $('.issue-form').fadeOut();
    $('.receipt-form').fadeOut();
    document.getElementById('messages').innerHTML = '';
    /*

    

    document.forms["issueForm"]["adm"].value = '';
    document.forms["issueForm"]["studentname"].value = '';
    document.forms["issueForm"]["studentid"].value = '';
    document.forms["issueForm"]["bookid"].value = '';
    document.forms["issueForm"]["serialcode"].value = '';*/
  });
});
