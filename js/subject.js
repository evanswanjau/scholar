/*
--------------------------------------------------------------------------------
SUBJECT  FUNCTIONS /////////////////////////////////////////////////////
--------------------------------------------------------------------------------
*/

// ADD SUBJECT
function addSubject() {

  var errors = [];                                              // errors storage
  var values = '';

  // subject
  if (document.forms["myForm"]["subject"].value == '') {
    errors.push('<p class="error">subject can\'t be empty</p>');
  }else {
    var subject = document.forms["myForm"]["subject"].value;
  }

  // if no errors exist
  if (errors == false) {
    var code = document.forms["myForm"]["code"].value;
    // convert to json string format
    var subject_string = '{"code":"'+code+'", "subject":"'+subject+'"}';

    // push json string to php file
    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("main-message").innerHTML = this.responseText;
                document.getElementById('main').style.background = '#34ca66';
                $(document).ready(function(){
                  $('.post-form').hide();
                  document.forms["myForm"]["code"].value = '';
                  document.forms["myForm"]["subject"].value = '';
                });
                window.setTimeout(function(){
                  window.location = '../subjects';
                }, 500);
            }
        };

        xmlhttp.open("POST", "../../../engine/infused_cogs.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("add-subject=" + subject_string);

  }else {
    // if errors exist echo out errors
    for (var i in errors) {
      values += errors[i];
    }
    document.getElementById('messages').innerHTML = values;
  }

  return false;
}


// GET SUBJECT DATA
function getSubjectData(value){
  // reset messages to null
  document.getElementById('messages').innerHTML = '';

  $(document).ready(function(){
    $('.add').hide();
    $('.edit').show();
  });

  // get parent info
  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            parentObject = JSON.parse(this.responseText);

            // assign to respective fields
            document.forms["myForm"]["id"].value = parentObject.id;
            document.forms["myForm"]["code"].value = parentObject.code;
            document.forms["myForm"]["subject"].value = parentObject.subject;
          }
      };

  xmlhttp.open("GET", "../../../engine/infused_cogs.php?get_subject_data="+value, true);
  xmlhttp.send();

  return false;
}

// EDIT SUBJECT
function editSubject(){
  var errors = [];                                              // errors storage
  var values = '';

  // subject
  if (document.forms["myForm"]["subject"].value == '') {
    errors.push('<p class="error">subject can\'t be empty</p>');
  }else {
    var subject = document.forms["myForm"]["subject"].value;
  }

  // if no errors exist
  if (errors == false) {
    var id = document.forms["myForm"]["id"].value;
    var code = document.forms["myForm"]["code"].value;
    // convert to json string format
    var subject_string = '{"id":"'+id+'", "code":"'+code+'", "subject":"'+subject+'"}';

    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("main-message").innerHTML = this.responseText;
                document.getElementById('main').style.background = '#34ca66';
                $(document).ready(function(){
                  $('.post-form').hide();
                  document.forms["myForm"]["code"].value = '';
                  document.forms["myForm"]["subject"].value = '';
                });
                window.setTimeout(function(){
                  window.location = '../subjects';
                }, 500);
            }
        };

        xmlhttp.open("POST", "../../../engine/infused_cogs.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("edit-subject-update=" + subject_string);

  }else {
    // if errors exist echo out errors
    for (var i in errors) {
      values += errors[i];
    }
    document.getElementById('messages').innerHTML = values;
  }

  return false;
}

// DELETE SUBJECT
function deleteSubject(){
  var id = document.forms["deleteForm"]["id"].value;

  // get parent info
  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            document.getElementById('main-message').innerHTML = this.responseText;
            document.getElementById('main').style.background = '#34ca66';
            $(document).ready(function(){
              $('.delete-form').hide();
            });
            window.setTimeout(function(){
              window.location = '../subjects';
            }, 500);
          }
      };

  xmlhttp.open("GET", "../../../engine/infused_cogs.php?delete_subject="+id, true);
  xmlhttp.send();

  return false;
}

// get Deletion value
function deleteValue(value){
  var id = value;

  document.forms["deleteForm"]["id"].value = id;

  return false;
}

// WHEN CLOSE IT IS CLICKED
$(document).ready(function(){
  $('.closeit').click(function(){
    document.forms["myForm"]["code"].value = '';
    document.forms["myForm"]["subject"].value = '';
  });
});
