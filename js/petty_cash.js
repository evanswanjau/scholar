// MAKE PAYMENT
function issueCash() {

  var errors = [];                                              // errors storage
  var values = '';

  // amount
  if (document.forms["myForm"]["amount"].value == '') {
    errors.push('<p class="error">amount can\'t be empty</p>');
  }else {
    var amount = document.forms["myForm"]["amount"].value;
  }

  // given to
  if (document.forms["myForm"]["given"].value == '') {
    errors.push('<p class="error">given to can\'t be empty</p>');
  }else {
    var given = document.forms["myForm"]["given"].value;
  }

  // reason
  if (document.forms["myForm"]["reason"].value == '') {
    errors.push('<p class="error">reason can\'t be empty</p>');
  }else {
    var reason = document.forms["myForm"]["reason"].value;
  }



  // if no errors exist
  if (errors == false) {

    // convert to json string format
    var petty_string = '{"amount":"'+amount+'","given":"'+given+'", "reason":"'+reason+'"}';

    // push json string to php file
    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {

              if (this.responseText == 'error') {
                document.getElementById("main-message").innerHTML = 'account balance is insufficient';
                document.getElementById('main').style.background = '#f14949';
                $(document).ready(function(){
                  $('.post-form').hide();
                  document.forms["myForm"]["amount"].value = '';
                  document.forms["myForm"]["given"].value = '';
                  document.forms["myForm"]["reason"].value = '';
                });
                window.setTimeout(function(){
                  window.location = '../petty-cash';
                }, 2000);
              }else {
                document.getElementById("main-message").innerHTML = this.responseText;
                document.getElementById('main').style.background = '#34ca66';
                $(document).ready(function(){
                  $('.post-form').hide();
                  document.forms["myForm"]["amount"].value = '';
                  document.forms["myForm"]["given"].value = '';
                  document.forms["myForm"]["reason"].value = '';
                });
                window.setTimeout(function(){
                  window.location = '../petty-cash';
                }, 2000);
              }
            }
        };

        xmlhttp.open("POST", "../../../../../engine/infused_cogs.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("issue-cash=" + petty_string);

  }else {
    // if errors exist echo out errors
    for (var i in errors) {
      values += errors[i];
    }
    document.getElementById('messages').innerHTML = values;
  }

  return false;
}
