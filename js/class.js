/*
------------------------------------------------------------------------
CLASS FUNCTIONS
------------------------------------------------------------------------
*/
// ADD CLASS
function addClass() {

  var errors = [];
  var values = '';

  // full name
  if (document.forms["myForm"]["name"].value == '') {
    errors.push('<p class="error">class name can\'t be empty</p>');
  }else {
    var name = document.forms["myForm"]["name"].value;
  }

  // phone_number
  if (document.forms["myForm"]["section"].value == '') {
    errors.push('<p class="error">section can\'t be empty</p>');
  }else {
    var section = document.forms["myForm"]["section"].value;
  }

  // if no errors exist
  if (errors == false) {
    var teacher = document.forms["myForm"]["teacher"].value;

    // convert to json string format
    var class_string = '{"name":"'+name+'", "section":"'+section+'", "class_teacher":"'+teacher+'"}';

    // push json string to php file
    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
              if (this.responseText == 1) {
                document.getElementById("main-message").innerHTML = 'that class already exists';
                document.getElementById('main').style.background = '#f14949';
                $(document).ready(function(){
                  $('.post-form').hide();
                  document.forms["myForm"]["name"].value = '';
                  document.forms["myForm"]["section"].value = '';
                  document.forms["myForm"]["teacher"].value = '';
                });
                window.setTimeout(function(){
                  window.location = '../manage-classes';
                }, 1500);
              }else {
                document.getElementById("main-message").innerHTML = this.responseText;
                document.getElementById('main').style.background = '#34ca66';
                $(document).ready(function(){
                  $('.post-form').hide();
                  document.forms["myForm"]["name"].value = '';
                  document.forms["myForm"]["section"].value = '';
                  document.forms["myForm"]["teacher"].value = '';
                });
                window.setTimeout(function(){
                  window.location = '../manage-classes';
                }, 500);
              }
            }
        };

        xmlhttp.open("POST", "../../../../engine/infused_cogs.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("add-class=" + class_string);

  }else {
    // if errors exist echo out errors
    for (var i in errors) {
      values += errors[i];
    }
    document.getElementById('messages').innerHTML = values;
  }

  return false;
}


// GET CLASS DATA
function getClassData(value){
  // reset messages to null
  document.getElementById('messages').innerHTML = '';

  $(document).ready(function(){
    $('.add').hide();
    $('.edit').show();
  });

  // get parent info
  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {

            parentObject = JSON.parse(this.responseText);
            // assign to respective fields
            document.forms["myForm"]["id"].value = parentObject.id;
            var a = document.getElementById('level').selectedIndex;
            var b = document.getElementById('level').options;
            b[a].text = parentObject.level;
            b[a].value = parentObject.levelid;
            var c = document.getElementById('section').selectedIndex;
            var d = document.getElementById('section').options;
            d[c].text = parentObject.section;
            d[c].value = parentObject.sectionid;
            var x = document.getElementById('teacher').selectedIndex;
            var y = document.getElementById('teacher').options;
            y[x].text = parentObject.teacher;
            y[x].value = parentObject.teacherid;
          }
      };

    xmlhttp.open("GET", "../../../../engine/infused_cogs.php?get_class_data="+value, true);
    xmlhttp.send();

  return false;
}

// EDIT CLASS
function editClass(){
  var errors = [];                                              // errors storage
  var values = '';

  // full name
  if (document.forms["myForm"]["name"].value == '') {
    errors.push('<p class="error">name can\'t be empty</p>');
  }else {
    var name = document.forms["myForm"]["name"].value;
  }

  // phone_number
  if (document.forms["myForm"]["section"].value == '') {
    errors.push('<p class="error">section can\'t be empty</p>');
  }else {
    var section = document.forms["myForm"]["section"].value;
  }

  // if no errors exist
  if (errors == false) {
    var id = document.forms["myForm"]["id"].value;
    var teacher = document.forms["myForm"]["teacher"].value;

    // convert to json string format
    var class_string = '{"id":"'+id+'","name":"'+name+'", "section":"'+section+'", "teacher":"'+teacher+'"}';

    // push json string to php file
    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("main-message").innerHTML = this.responseText;
                document.getElementById('main').style.background = '#34ca66';

                $(document).ready(function(){
                  $('.post-form').hide();
                  document.forms["myForm"]["id"].value = '';
                  document.forms["myForm"]["name"].value = '';
                  document.forms["myForm"]["section"].value = '';
                  document.forms["myForm"]["teacher"].value = '';
                });
                window.setTimeout(function(){
                  window.location = '../manage-classes';
                }, 500);
            }
        };

        xmlhttp.open("POST", "../../../../engine/infused_cogs.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("edit-class-update=" + class_string);

  }else {
    // if errors exist echo out errors
    for (var i in errors) {
      values += errors[i];
    }
    document.getElementById('messages').innerHTML = values;
  }

  return false;
}

// DELETE TEACHER
function deleteClass(){
  var id = document.forms["deleteForm"]["id"].value;

  // get parent info
  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            document.getElementById('main-message').innerHTML = this.responseText;
            document.getElementById('main').style.background = '#34ca66';
            $(document).ready(function(){
              $('.delete-form').hide();
            });
            window.setTimeout(function(){
              window.location = '../manage-classes';
            }, 500);
          }
      };

  xmlhttp.open("GET", "../../../../engine/infused_cogs.php?delete_class="+id, true);
  xmlhttp.send();

  return false;
}

// get Deletion value
function deleteValue(value){
  var id = value;

  document.forms["deleteForm"]["id"].value = id;

  return false;
}

// WHEN CLOSE IT IS CLICKED
$(document).ready(function(){
  $('.closeit').click(function(){
    document.forms["myForm"]["id"].value = parentObject.id;
    var a = document.getElementById('level').selectedIndex;
    var b = document.getElementById('level').options;
    b[a].text = 'choose level';
    b[a].value = '';
    var c = document.getElementById('section').selectedIndex;
    var d = document.getElementById('section').options;
    d[c].text = 'choose section';
    d[c].value = '';
    var x = document.getElementById('teacher').selectedIndex;
    var y = document.getElementById('teacher').options;
    y[x].text = 'choose class teacher';
    y[x].value = '';
  });
});
