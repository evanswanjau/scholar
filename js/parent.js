/*
--------------------------------------------------------------------------------
PARENT FUNCTIONS ///////////////////////////////////////////////////////////////
--------------------------------------------------------------------------------
*/

// ADD PARENT
function addParent() {

  var errors = [];                                              // errors storage
  var values = '';

  // full name
  if (document.forms["myForm"]["fullname"].value == '') {
    errors.push('<p class="error">fullname can\'t be empty</p>');
  }else {
    var fullname = document.forms["myForm"]["fullname"].value;
  }

  // phone_number
  if (document.forms["myForm"]["phone_number"].value == '') {
    errors.push('<p class="error">phone number can\'t be empty</p>');
  }else {
    var phone_number = document.forms["myForm"]["phone_number"].value;
  }

  // if no errors exist
  if (errors == false) {
    var email = document.forms["myForm"]["email"].value;
    var location = document.forms["myForm"]["location"].value;
    var profession = document.forms["myForm"]["profession"].value;

    // convert to json string format
    var parent_string = '{"fullname":"'+fullname+'", "email":"'+email+'", "phone_number":"'+phone_number+'", "location":"'+location+'", "profession":"'+profession+'"}';

    // push json string to php file
    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("main-message").innerHTML = this.responseText;
                document.getElementById('main').style.background = '#34ca66';
                $(document).ready(function(){
                  $('.post-form').hide();
                  document.forms["myForm"]["fullname"].value = '';
                  document.forms["myForm"]["email"].value = '';
                  document.forms["myForm"]["phone_number"].value = '';
                  document.forms["myForm"]["location"].value = '';
                  document.forms["myForm"]["profession"].value = '';
                });
                window.setTimeout(function(){
                  window.location = '../parents';
                }, 500);
            }
        };

        xmlhttp.open("POST", "../../../engine/infused_cogs.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("add-parent=" + parent_string);

  }else {
    // if errors exist echo out errors
    for (var i in errors) {
      values += errors[i];
    }
    document.getElementById('messages').innerHTML = values;
  }

  return false;
}

// GET PARENT DATA
function getParentData(value){
  // reset messages to null
  document.getElementById('messages').innerHTML = '';

  $(document).ready(function(){
    $('.add').hide();
    $('.edit').show();
  });

  // get parent info
  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            parentObject = JSON.parse(this.responseText);

            // assign to respective fields
            document.forms["myForm"]["id"].value = parentObject.id;
            document.forms["myForm"]["fullname"].value = parentObject.fullname;
            document.forms["myForm"]["email"].value = parentObject.email;
            document.forms["myForm"]["phone_number"].value = parentObject.phone_number;
            document.forms["myForm"]["location"].value = parentObject.location;
            document.forms["myForm"]["profession"].value = parentObject.profession;
          }
      };

  xmlhttp.open("GET", "../../../engine/infused_cogs.php?get_parent_data="+value, true);
  xmlhttp.send();

  return false;
}

// EDIT PARENT
function editParent(){
  var errors = [];                                              // errors storage
  var values = '';

  // full name
  if (document.forms["myForm"]["fullname"].value == '') {
    errors.push('<p class="error">fullname can\'t be empty</p>');
  }else {
    var fullname = document.forms["myForm"]["fullname"].value;
  }

  // phone_number
  if (document.forms["myForm"]["phone_number"].value == '') {
    errors.push('<p class="error">phone number can\'t be empty</p>');
  }else {
    var phone_number = document.forms["myForm"]["phone_number"].value;
  }

  // if no errors exist
  if (errors == false) {
    var id = document.forms["myForm"]["id"].value;
    var email = document.forms["myForm"]["email"].value;
    var location = document.forms["myForm"]["location"].value;
    var profession = document.forms["myForm"]["profession"].value;

    // convert to json string format
    var parent_string = '{"parent_id":"'+id+'", "fullname":"'+fullname+'", "email":"'+email+'", "phone_number":"'+phone_number+'", "location":"'+location+'", "profession":"'+profession+'"}';

    // push json string to php file
    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("main-message").innerHTML = this.responseText;
                document.getElementById('main').style.background = '#34ca66';
                $(document).ready(function(){
                  $('.post-form').hide();
                  document.forms["myForm"]["id"].value = '';
                  document.forms["myForm"]["fullname"].value = '';
                  document.forms["myForm"]["email"].value = '';
                  document.forms["myForm"]["phone_number"].value = '';
                  document.forms["myForm"]["location"].value = '';
                  document.forms["myForm"]["profession"].value = '';
                });
                window.setTimeout(function(){
                  window.location = '../parents';
                }, 500);
            }
        };

        xmlhttp.open("POST", "../../../engine/infused_cogs.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("edit-parent-update=" + parent_string);

  }else {
    // if errors exist echo out errors
    for (var i in errors) {
      values += errors[i];
    }
    document.getElementById('messages').innerHTML = values;
  }

  return false;
}

// DELETE PARENT
function deleteParent(){
  var id = document.forms["deleteForm"]["id"].value;

  // get parent info
  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            document.getElementById('main-message').innerHTML = this.responseText;
            document.getElementById('main').style.background = '#34ca66';
            $(document).ready(function(){
              $('.delete-form').hide();
            });
            window.setTimeout(function(){
              window.location = '../parents';
            }, 500);
          }
      };

  xmlhttp.open("GET", "../../../engine/infused_cogs.php?delete_parent="+id, true);
  xmlhttp.send();

  return false;
}

// get Deletion value
function deleteValue(value){
  var id = value;

  document.forms["deleteForm"]["id"].value = id;

  return false;
}

// WHEN CLOSE IT IS CLICKED
$(document).ready(function(){
  $('.closeit').click(function(){
    document.forms["myForm"]["fullname"].value = '';
    document.forms["myForm"]["email"].value = '';
    document.forms["myForm"]["phone_number"].value = '';
    document.forms["myForm"]["location"].value = '';
    document.forms["myForm"]["profession"].value = '';
  });
});
