/*
--------------------------------------------------------------------------------
LIBRARY FUNCTIONS ///////////////////////////////////////////////////////////////
--------------------------------------------------------------------------------
*/

// ADD BOOK
function addItem() {

  var errors = [];                                              // errors storage
  var values = '';

  // item name
  if (document.forms["myForm"]["item"].value == '') {
    errors.push('<p class="error">item can\'t be empty</p>');
  }else {
    var item = document.forms["myForm"]["item"].value;
  }

  // price
  if (document.forms["myForm"]["price"].value == '') {
    errors.push('<p class="error">price can\'t be empty</p>');
  }else {
    var price = document.forms["myForm"]["price"].value;
  }

  // if no errors exist
  if (errors == false) {

    // convert to json string format
    var item_string = '{"item":"'+item+'", "price":"'+price+'"}';
    // push json string to php file
    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("main-message").innerHTML = this.responseText;
                document.getElementById('main').style.background = '#34ca66';
                $(document).ready(function(){
                  $('.post-form').hide();
                  document.forms["myForm"]["item"].value = '';
                  document.forms["myForm"]["price"].value = '';
                });
                window.setTimeout(function(){
                  window.location = '../invoice-list';
                }, 2000);
            }
        };

        xmlhttp.open("POST", "../../../../../../engine/infused_cogs.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("add-item=" + item_string);

  }else {
    // if errors exist echo out errors
    for (var i in errors) {
      values += errors[i];
    }
    document.getElementById('messages').innerHTML = values;
  }

  return false;
}


// GET PARENT DATA
function getItemData(value){
  // reset messages to null
  document.getElementById('messages').innerHTML = '';

  $(document).ready(function(){
    $('.add').hide();
    $('.edit').show();
  });

  // get parent info
  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            parentObject = JSON.parse(this.responseText);

            // assign to respective fields
            document.forms["myForm"]["id"].value = parentObject.id;
            document.forms["myForm"]["item"].value = parentObject.item;
            document.forms["myForm"]["price"].value = parentObject.price;
            $('.closeit').click(function(){
              document.forms["myForm"]["price"].value = '';
              document.forms["myForm"]["item"].value = '';
              document.forms["myForm"]["id"].value = '';
            });
          }
      };

  xmlhttp.open("GET", "../../../../../../engine/infused_cogs.php?get_item_data="+value, true);
  xmlhttp.send();

  return false;
}

// EDIT ITEM
function editItem(){
  var errors = [];                                              // errors storage
  var values = '';

  // item name
  if (document.forms["myForm"]["item"].value == '') {
    errors.push('<p class="error">item can\'t be empty</p>');
  }else {
    var item = document.forms["myForm"]["item"].value;
  }

  // price
  if (document.forms["myForm"]["price"].value == '') {
    errors.push('<p class="error">price can\'t be empty</p>');
  }else {
    var price = document.forms["myForm"]["price"].value;
  }

  // if no errors exist
  if (errors == false) {
    var id = document.forms["myForm"]["id"].value;

    // convert to json string format
    var item_string = '{"invoice_id":"'+id+'", "item":"'+item+'", "price":"'+price+'"}';

    // push json string to php file
    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("main-message").innerHTML = this.responseText;
                document.getElementById('main').style.background = '#34ca66';
                $(document).ready(function(){
                  $('.post-form').hide();
                  document.forms["myForm"]["id"].value = '';
                  document.forms["myForm"]["item"].value = '';
                  document.forms["myForm"]["price"].value = '';
                });
                window.setTimeout(function(){
                  window.location = '../invoice-list';
                }, 2000);
            }
        };

        xmlhttp.open("POST", "../../../../../../engine/infused_cogs.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("edit-item-update=" + item_string);

  }else {
    // if errors exist echo out errors
    for (var i in errors) {
      values += errors[i];
    }
    document.getElementById('messages').innerHTML = values;
  }

  return false;
}

// DELETE PARENT
function deleteItem(){
  var id = document.forms["deleteForm"]["id"].value;

  // get parent info
  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            document.getElementById('main-message').innerHTML = this.responseText;
            document.getElementById('main').style.background = '#34ca66';
            $(document).ready(function(){
              $('.delete-form').hide();
            });
            window.setTimeout(function(){
              window.location = '../invoice-list';
            }, 2000);
          }
      };

  xmlhttp.open("GET", "../../../../../../engine/infused_cogs.php?delete_item="+id, true);
  xmlhttp.send();

  return false;
}

// get Deletion value
function deleteValue(value){
  var id = value;

  document.forms["deleteForm"]["id"].value = id;

  return false;
}
