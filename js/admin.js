/*
--------------------------------------------------------------------------------
ADMIN FUNCTIONS ///////////////////////////////////////////////////////////////
--------------------------------------------------------------------------------
*/

// ADD ADMIN
function addAdmin() {

  var errors = [];                                              // errors storage
  var values = '';

  // full name
  if (document.forms["myForm"]["fullname"].value == '') {
    errors.push('<p class="error">fullname can\'t be empty</p>');
  }else {
    var fullname = document.forms["myForm"]["fullname"].value;
  }

  // username
  if (document.forms["myForm"]["username"].value == '') {
    errors.push('<p class="error">username can\'t be empty</p>');
  }else {
    var username = document.forms["myForm"]["username"].value;
  }

  // phone_number
  if (document.forms["myForm"]["phone"].value == '') {
    errors.push('<p class="error">phone number can\'t be empty</p>');
  }else {
    var phone_number = document.forms["myForm"]["phone"].value;
  }

  // phone_number
  if (document.forms["myForm"]["role"].value == '') {
    errors.push('<p class="error">role can\'t be empty</p>');
  }else {
    var role = document.forms["myForm"]["role"].value;
  }

  // if no errors exist
  if (errors == false) {
    var email = document.forms["myForm"]["email"].value;

    // convert to json string format
    var admin_string = '{"name":"'+fullname+'", "email":"'+email+'", "phone":"'+phone_number+'", "username":"'+username+'", "role":"'+role+'"}';

    // push json string to php file
    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("main-message").innerHTML = this.responseText;
                document.getElementById('main').style.background = '#34ca66';
                $(document).ready(function(){
                  $('.post-form').hide();
                  document.forms["myForm"]["fullname"].value = '';
                  document.forms["myForm"]["email"].value = '';
                  document.forms["myForm"]["phone_number"].value = '';
                  document.forms["myForm"]["username"].value = '';
                  document.forms["myForm"]["role"].value = '';
                });
                window.setTimeout(function(){
                  window.location = '../admin-accounts';
                }, 2000);
            }
        };

        xmlhttp.open("POST", "../../../engine/infused_cogs.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("add-admin=" + admin_string);

  }else {
    // if errors exist echo out errors
    for (var i in errors) {
      values += errors[i];
    }
    document.getElementById('messages').innerHTML = values;
  }

  return false;
}


// DELETE ADMIN
function deleteAdmin(){
  var id = document.forms["deleteForm"]["id"].value;

  // get parent info
  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            document.getElementById('main-message').innerHTML = this.responseText;
            document.getElementById('main').style.background = '#34ca66';
            $(document).ready(function(){
              $('.delete-form').hide();
            });
            window.setTimeout(function(){
              window.location = '../admin-accounts';
            }, 2000);
          }
      };

  xmlhttp.open("GET", "../../../engine/infused_cogs.php?delete_admin="+id, true);
  xmlhttp.send();

  return false;
}

// get Deletion value
function deleteValue(value){
  var id = value;

  document.forms["deleteForm"]["id"].value = id;

  return false;
}

// WHEN CLOSE IT IS CLICKED
$(document).ready(function(){
  $('.closeit').click(function(){
    document.forms["myForm"]["fullname"].value = '';
    document.forms["myForm"]["email"].value = '';
    document.forms["myForm"]["phone"].value = '';
    document.forms["myForm"]["username"].value = '';
    document.forms["myForm"]["role"].value = '';
  });
});
