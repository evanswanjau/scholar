/*
----------------------------------------------
PAYMENT FUNCTIONS
----------------------------------------------
*/
// GET STUDENT SEARCH FUNCTIONS
function getAllStudentList(value){

  // get book info
  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            if (value != "") {
              document.getElementById('student_issued').innerHTML = this.responseText;
            }else {
              document.getElementById('student_issued').innerHTML = "";
            }
          }
      };

  xmlhttp.open("GET", "../../../../../engine/infused_cogs.php?get_student_search_data="+value, true);
  xmlhttp.send();

  return false;
}

// ASSIGN VALUES
function assignBook(value){

  document.forms["myForm"]["accountid"].value = value.account;
  document.forms["myForm"]["adm"].value = value.adm;
  document.forms["myForm"]["name"].value = value.name;

  document.getElementById('student_issued').innerHTML = "";
  document.forms["myForm"]["studentsearch"].value = "";

  return false;
}

// MAKE PAYMENT
function makePayment() {

  var errors = [];                                              // errors storage
  var values = '';

  // admission number
  if (document.forms["myForm"]["adm"].value == '') {
    errors.push('<p class="error">admission number can\'t be empty</p>');
  }else {
    var adm = document.forms["myForm"]["adm"].value;
  }

  // book name
  if (document.forms["myForm"]["name"].value == '') {
    errors.push('<p class="error">student name can\'t be empty</p>');
  }else {
    var name = document.forms["myForm"]["name"].value;
  }

  // amount
  if (document.forms["myForm"]["amount"].value == '') {
    errors.push('<p class="error">amount can\'t be empty</p>');
  }else {
    var amount = document.forms["myForm"]["amount"].value;
  }

  // amount
  if (document.forms["myForm"]["purpose"].value == '') {
    errors.push('<p class="error">purpose of payment can\'t be empty</p>');
  }else {
    var purpose = document.forms["myForm"]["purpose"].value;
  }

  // amount
  if (document.forms["myForm"]["method"].value == '') {
    errors.push('<p class="error">method of payment can\'t be empty</p>');
  }else {
    var method = document.forms["myForm"]["method"].value;
  }


  // if no errors exist
  if (errors == false) {
    var account = document.forms["myForm"]["accountid"].value;

    // convert to json string format
    var payment_string = '{"account":"'+account+'","adm":"'+adm+'", "name":"'+name+'", "amount":"'+amount+'", "purpose":"'+purpose+'", "method":"'+method+'"}';
    // push json string to php file
    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("main-message").innerHTML = this.responseText;
                document.getElementById('main').style.background = '#34ca66';
                $(document).ready(function(){
                  $('.post-form').hide();
                  document.forms["myForm"]["accountid"].value = '';
                  document.forms["myForm"]["adm"].value = '';
                  document.forms["myForm"]["name"].value = '';
                  document.forms["myForm"]["amount"].value = '';
                  document.forms["myForm"]["purpose"].value = '';
                  document.forms["myForm"]["method"].value = '';
                });
                window.setTimeout(function(){
                  window.location = '../payments';
                }, 2000);
            }
        };

        xmlhttp.open("POST", "../../../../../engine/infused_cogs.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("make-payment=" + payment_string);

  }else {
    // if errors exist echo out errors
    for (var i in errors) {
      values += errors[i];
    }
    document.getElementById('messages').innerHTML = values;
  }

  return false;
}


// FUNCTION TO GENERATE RECEIPT
function genReceipt(value) {

  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            var paymentObject = JSON.parse(this.responseText);
            document.getElementById('account').innerHTML = paymentObject.accountno;
            document.getElementById('ref').innerHTML = paymentObject.ref;
            document.getElementById('name').innerHTML = paymentObject.name;
            document.getElementById('amount').innerHTML = 'Ksh' + paymentObject.amount_paid;
            document.getElementById('purpose').innerHTML = paymentObject.purpose;
            document.getElementById('method').innerHTML = paymentObject.method;
            document.getElementById('date').innerHTML = paymentObject.date;
          }
      };

  xmlhttp.open("GET", "../../../../../engine/infused_cogs.php?get_payment_data="+value, true);
  xmlhttp.send();

  return false;

}

// PRINT FUNCTION
function printDiv() {

  var divToPrint = document.getElementById('printableArea');
  var popupWin = window.open('', '_blank', 'width=500,height=700');
  popupWin.document.open();
  popupWin.document.write('<html><head><title>Print Receipt</title>');
  popupWin.document.write('<style type="text/css">.print {padding: 2%;background-color: #fff;text-align: left; }.print p {text-transform: uppercase; }.print h6 \
  {border-top: 1px solid #555;text-align: center;width:50%;margin-top: 5%;padding: 1%;font-style: italic; }.print input {display: none; } .closeit{display:none;}.bold{background-color:#999;width:50%;text-align: center;padding:1%;}</style>');
  popupWin.document.write('<link rel="stylesheet" href="../../../../../css/main.css" type="text/css" media="print"/>');
  popupWin.document.write('</head><body onload="window.print()">' + divToPrint.innerHTML + '</div></body></html>');
  popupWin.document.close();
}
