/*
--------------------------------------------------------------------------------
DORMITORY  FUNCTIONS /////////////////////////////////////////////////////
--------------------------------------------------------------------------------
*/

// ADD DORM
function addDorm() {

  var errors = [];                                              // errors storage
  var values = '';

  // name
  if (document.forms["myForm"]["name"].value == '') {
    errors.push('<p class="error">name can\'t be empty</p>');
  }else {
    var name = document.forms["myForm"]["name"].value;
  }

  // beds
  if (document.forms["myForm"]["beds"].value == '') {
    errors.push('<p class="error">total bed count can\'t be empty</p>');
  }else {
    var beds = document.forms["myForm"]["beds"].value;
  }

  // if no errors exist
  if (errors == false) {

    // convert to json string format
    var dorm_string = '{"name":"'+name+'", "beds":"'+beds+'"}';

    // push json string to php file
    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("main-message").innerHTML = this.responseText;
                document.getElementById('main').style.background = '#34ca66';
                $(document).ready(function(){
                  $('.post-form').hide();
                  document.forms["myForm"]["name"].value = '';
                  document.forms["myForm"]["beds"].value = '';
                });
                window.setTimeout(function(){
                  window.location = '../dormitories';
                }, 500);
            }
        };

        xmlhttp.open("POST", "../../../engine/infused_cogs.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("add-dorm=" + dorm_string);

  }else {
    // if errors exist echo out errors
    for (var i in errors) {
      values += errors[i];
    }
    document.getElementById('messages').innerHTML = values;
  }

  return false;
}


// GET ROUTE DATA
function getDormData(value){
  // reset messages to null
  document.getElementById('messages').innerHTML = '';

  $(document).ready(function(){
    $('.add').hide();
    $('.edit').show();
  });

  // get parent info
  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            parentObject = JSON.parse(this.responseText);

            // assign to respective fields
            document.forms["myForm"]["id"].value = parentObject.id;
            document.forms["myForm"]["name"].value = parentObject.name;
            document.forms["myForm"]["beds"].value = parentObject.beds;
          }
      };

  xmlhttp.open("GET", "../../../engine/infused_cogs.php?get_dorm_data="+value, true);
  xmlhttp.send();

  return false;
}

// EDIT ROUTE
function editDorm(){
  var errors = [];                                              // errors storage
  var values = '';

  // name
  if (document.forms["myForm"]["name"].value == '') {
    errors.push('<p class="error">name can\'t be empty</p>');
  }else {
    var name = document.forms["myForm"]["name"].value;
  }

  // beds
  if (document.forms["myForm"]["beds"].value == '') {
    errors.push('<p class="error">total bed count can\'t be empty</p>');
  }else {
    var beds = document.forms["myForm"]["beds"].value;
  }

  // if no errors exist
  if (errors == false) {
    var id = document.forms["myForm"]["id"].value;
    // convert to json string format
    var dorm_string = '{"id":"'+id+'", "name":"'+name+'", "beds":"'+beds+'"}';

    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("main-message").innerHTML = this.responseText;
                document.getElementById('main').style.background = '#34ca66';
                $(document).ready(function(){
                  $('.post-form').hide();
                  document.forms["myForm"]["name"].value = '';
                  document.forms["myForm"]["beds"].value = '';
                });
                window.setTimeout(function(){
                  window.location = '../dormitories';
                }, 500);
            }
        };

        xmlhttp.open("POST", "../../../engine/infused_cogs.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("edit-dorm-update=" + dorm_string);

  }else {
    // if errors exist echo out errors
    for (var i in errors) {
      values += errors[i];
    }
    document.getElementById('messages').innerHTML = values;
  }

  return false;
}

// DELETE DORM
function deleteDorm(){
  var id = document.forms["deleteForm"]["id"].value;

  // get parent info
  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            document.getElementById('main-message').innerHTML = this.responseText;
            document.getElementById('main').style.background = '#34ca66';
            $(document).ready(function(){
              $('.delete-form').hide();
            });
            window.setTimeout(function(){
              window.location = '../dormitories';
            }, 500);
          }
      };

  xmlhttp.open("GET", "../../../engine/infused_cogs.php?delete_dorm="+id, true);
  xmlhttp.send();

  return false;
}

// get Deletion value
function deleteValue(value){
  var id = value;

  document.forms["deleteForm"]["id"].value = id;

  return false;
}

// WHEN CLOSE IT IS CLICKED
$(document).ready(function(){
  $('.closeit').click(function(){
    document.forms["myForm"]["name"].value = '';
    document.forms["myForm"]["beds"].value = '';
  });
});
