/*
--------------------------------------------
INVOICE RESOURCES
--------------------------------------------
*/
function removeFromInvoice(value, level){

  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            window.setTimeout(function(){
              window.location = '../invoices/?level=' + level;
            }, 1);
          }
      };

  xmlhttp.open("GET", "../../../../../../engine/infused_cogs.php?remove_from_list="+value+"&level="+level, true);
  xmlhttp.send();

  return false;
}


function addToInvoice(value, level){

  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            if (this.responseText != '') {
              document.getElementById("main-message").innerHTML = this.responseText;
              document.getElementById('main').style.background = '#f14949';
              window.setTimeout(function(){
                window.location = '../invoices/?level=' + level;
              }, 2000);
            }else {
              window.setTimeout(function(){
                window.location = '../invoices/?level=' + level;
              }, 1);
            }
          }
      };

  xmlhttp.open("GET", "../../../../../../engine/infused_cogs.php?add_to_list="+value+"&level="+level, true);
  xmlhttp.send();

  return false;
}

// GET STUDENT SEARCH FUNCTIONS
function getAllStudentList(value){

  // get book info
  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            if (value != "") {
              document.getElementById('student_issued').innerHTML = this.responseText;
            }else {
              document.getElementById('student_issued').innerHTML = "";
            }
          }
      };

  xmlhttp.open("GET", "../../../../../../engine/infused_cogs.php?get_student_search_data="+value, true);
  xmlhttp.send();

  return false;
}

// ASSIGN VALUES
function assignBook(value){

  document.forms["myForm"]["accountid"].value = value.account;
  document.forms["myForm"]["adm"].value = value.adm;
  document.forms["myForm"]["name"].value = value.name;

  document.getElementById('student_issued').innerHTML = "";
  document.forms["myForm"]["studentsearch"].value = "";

  return false;
}
