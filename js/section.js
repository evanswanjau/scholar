/*
--------------------------------------------------------------------------------
SECTION FUNCTIONS /////////////////////////////////////////////////////
--------------------------------------------------------------------------------
*/

// ADD SECTION
function addSection() {

  var errors = [];                                              // errors storage
  var values = '';

  // name
  if (document.forms["myForm"]["name"].value == '') {
    errors.push('<p class="error">name can\'t be empty</p>');
  }else {
    var section = document.forms["myForm"]["name"].value;
  }

  // if no errors exist
  if (errors == false) {

    // convert to json string format
    var section_string = '{"section":"'+section+'"}';

    // push json string to php file
    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("main-message").innerHTML = this.responseText;
                document.getElementById('main').style.background = '#34ca66';
                $(document).ready(function(){
                  $('.post-form').hide();
                  document.forms["myForm"]["name"].value = '';
                });
                window.setTimeout(function(){
                  window.location = '../manage-sections';
                }, 2000);
            }
        };

        xmlhttp.open("POST", "../../../../engine/infused_cogs.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("add-section=" + section_string);

  }else {
    // if errors exist echo out errors
    for (var i in errors) {
      values += errors[i];
    }
    document.getElementById('messages').innerHTML = values;
  }

  return false;
}


// GET SECTION DATA
function getSectionData(value){
  // reset messages to null
  document.getElementById('messages').innerHTML = '';

  $(document).ready(function(){
    $('.add').hide();
    $('.edit').show();
  });

  // get parent info
  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            parentObject = JSON.parse(this.responseText);

            // assign to respective fields
            document.forms["myForm"]["id"].value = parentObject.id;
            document.forms["myForm"]["name"].value = parentObject.section;
          }
      };

  xmlhttp.open("GET", "../../../../engine/infused_cogs.php?get_section_data="+value, true);
  xmlhttp.send();

  return false;
}

// EDIT SECTION
function editSection(){
  var errors = [];                                              // errors storage
  var values = '';

  // name
  if (document.forms["myForm"]["name"].value == '') {
    errors.push('<p class="error">name can\'t be empty</p>');
  }else {
    var section = document.forms["myForm"]["name"].value;
  }

  // if no errors exist
  if (errors == false) {
    var id = document.forms["myForm"]["id"].value;
    // convert to json string format
    var section_string = '{"id":"'+id+'", "section":"'+section+'"}';

    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("main-message").innerHTML = this.responseText;
                document.getElementById('main').style.background = '#34ca66';
                $(document).ready(function(){
                  $('.post-form').hide();
                  document.forms["myForm"]["name"].value = '';
                });
                window.setTimeout(function(){
                  window.location = '../manage-sections';
                }, 2000);
            }
        };

        xmlhttp.open("POST", "../../../../engine/infused_cogs.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("edit-section-update=" + section_string);

  }else {
    // if errors exist echo out errors
    for (var i in errors) {
      values += errors[i];
    }
    document.getElementById('messages').innerHTML = values;
  }

  return false;
}

// DELETE SECTION
function deleteSection(){
  var id = document.forms["deleteForm"]["id"].value;

  // get parent info
  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            document.getElementById('main-message').innerHTML = this.responseText;
            document.getElementById('main').style.background = '#34ca66';
            $(document).ready(function(){
              $('.delete-form').hide();
            });
            window.setTimeout(function(){
              window.location = '../manage-sections';
            }, 2000);
          }
      };

  xmlhttp.open("GET", "../../../../engine/infused_cogs.php?delete_section="+id, true);
  xmlhttp.send();

  return false;
}

// get Deletion value
function deleteValue(value){
  var id = value;

  document.forms["deleteForm"]["id"].value = id;

  return false;
}

// WHEN CLOSE IT IS CLICKED
$(document).ready(function(){
  $('.closeit').click(function(){
    document.forms["myForm"]["name"].value = '';
  });
});
