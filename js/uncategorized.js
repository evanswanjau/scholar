// function to reveal way
function chooseWay(){
  document.getElementById('way').style.visibility = 'visible';
  return false;
}


// hide items
function hideItems(value, inputvalue, update){

  setTimeout(function(){
    document.getElementById(value).style.display = 'inline';
    document.getElementById(inputvalue).style.display = 'none';
    document.getElementById(update).style.opacity = 0;
  }, 20000);

  return false;
}

// reveal edit items
function revealEditItems(value, inputvalue, update){

  document.getElementById(value).style.display = 'none';
  document.getElementById(inputvalue).style.display = 'inline';
  document.getElementById(update).style.opacity = 1;

  return false;
}

// PRINT FUNCTION
function printDiv() {

  var divToPrint = document.getElementById('printableArea');
  var popupWin = window.open('', '_blank', 'width=1000,height=1000');
  popupWin.document.open();
  popupWin.document.write('<html><head><title>Print Exam Perfomance</title>');
  popupWin.document.write('<link rel="stylesheet" href="../../../../../css/main.css" type="text/css" media="print"/>');
  popupWin.document.write('</head><body onload="window.print()">' + divToPrint.innerHTML + '</div></body></html>');
  popupWin.document.close();
}
