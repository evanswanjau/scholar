/*
--------------------------------------------------------------------------------
CLUB FUNCTIONS /////////////////////////////////////////////////////
--------------------------------------------------------------------------------
*/

// ADD CLUB
function addClub() {

  var errors = [];                                              // errors storage
  var values = '';

  // name
  if (document.forms["myForm"]["name"].value == '') {
    errors.push('<p class="error">name can\'t be empty</p>');
  }else {
    var name = document.forms["myForm"]["name"].value;
  }


  // if no errors exist
  if (errors == false) {
    var description = document.forms["myForm"]["description"].value;
    var chair = document.forms["myForm"]["chair"].value;
    var vice = document.forms["myForm"]["vice"].value;
    var patron = document.forms["myForm"]["patron"].value;

    // convert to json string format
    var club_string = '{"name":"'+name+'", "description":"'+description+'","chair":"'+chair+'", "vice":"'+vice+'", "patron":"'+patron+'"}';

    // push json string to php file
    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("main-message").innerHTML = this.responseText;
                document.getElementById('main').style.background = '#34ca66';
                $(document).ready(function(){
                  $('.post-form').hide();
                  document.forms["myForm"]["name"].value = '';
                  document.forms["myForm"]["description"].value = '';
                  document.forms["myForm"]["chair"].value = '';
                  document.forms["myForm"]["vice"].value = '';
                  document.forms["myForm"]["patron"].value = '';
                });
                window.setTimeout(function(){
                  window.location = '../clubs';
                }, 500);
            }
        };

        xmlhttp.open("POST", "../../../engine/infused_cogs.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("add-club=" + club_string);

  }else {
    // if errors exist echo out errors
    for (var i in errors) {
      values += errors[i];
    }
    document.getElementById('messages').innerHTML = values;
  }

  return false;
}


// GET CLUB DATA
function getClubData(value){
  // reset messages to null
  document.getElementById('messages').innerHTML = '';

  $(document).ready(function(){
    $('.add').hide();
    $('.edit').show();
  });

  // get parent info
  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            parentObject = JSON.parse(this.responseText);

            // assign to respective fields
            document.forms["myForm"]["id"].value = parentObject.id;
            document.forms["myForm"]["name"].value = parentObject.name;
            document.forms["myForm"]["description"].value = parentObject.description;
            document.forms["myForm"]["chair"].value = parentObject.chair;
            document.forms["myForm"]["vice"].value = parentObject.vice;
            document.forms["myForm"]["patron"].value = parentObject.patron;
          }
      };

  xmlhttp.open("GET", "../../../engine/infused_cogs.php?get_club_data="+value, true);
  xmlhttp.send();

  return false;
}

// EDIT CLUB
function editClub(){
  var errors = [];                                              // errors storage
  var values = '';

  // name
  if (document.forms["myForm"]["name"].value == '') {
    errors.push('<p class="error">name can\'t be empty</p>');
  }else {
    var name = document.forms["myForm"]["name"].value;
  }

  // if no errors exist
  if (errors == false) {
    var id = document.forms["myForm"]["id"].value;
    var description = document.forms["myForm"]["description"].value;
    var chair = document.forms["myForm"]["chair"].value;
    var vice = document.forms["myForm"]["vice"].value;
    var patron = document.forms["myForm"]["patron"].value;
    // convert to json string format
    var club_string = '{"id":"'+id+'", "name":"'+name+'", "description":"'+description+'","chair":"'+chair+'", "vice":"'+vice+'", "patron":"'+patron+'"}';

    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("main-message").innerHTML = this.responseText;
                document.getElementById('main').style.background = '#34ca66';
                $(document).ready(function(){
                  $('.post-form').hide();
                  document.forms["myForm"]["id"].value = '';
                  document.forms["myForm"]["name"].value = '';
                  document.forms["myForm"]["description"].value = '';
                  document.forms["myForm"]["chair"].value = '';
                  document.forms["myForm"]["vice"].value = '';
                  document.forms["myForm"]["patron"].value = '';
                });
                window.setTimeout(function(){
                  window.location = '../clubs';
                }, 500);
            }
        };

        xmlhttp.open("POST", "../../../engine/infused_cogs.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("edit-club-update=" + club_string);

  }else {
    // if errors exist echo out errors
    for (var i in errors) {
      values += errors[i];
    }
    document.getElementById('messages').innerHTML = values;
  }

  return false;
}

// DELETE CLUB
function deleteClub(){
  var id = document.forms["deleteForm"]["id"].value;

  // get parent info
  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            document.getElementById('main-message').innerHTML = this.responseText;
            document.getElementById('main').style.background = '#34ca66';
            $(document).ready(function(){
              $('.delete-form').hide();
            });
            window.setTimeout(function(){
              window.location = '../clubs';
            }, 500);
          }
      };

  xmlhttp.open("GET", "../../../engine/infused_cogs.php?delete_club="+id, true);
  xmlhttp.send();

  return false;
}

// get Deletion value
function deleteValue(value){
  var id = value;

  document.forms["deleteForm"]["id"].value = id;

  return false;
}

// WHEN CLOSE IT IS CLICKED
$(document).ready(function(){
  $('.closeit').click(function(){
    document.forms["myForm"]["id"].value = '';
    document.forms["myForm"]["name"].value = '';
    document.forms["myForm"]["description"].value = '';
    document.forms["myForm"]["chair"].value = '';
    document.forms["myForm"]["vice"].value = '';
    document.forms["myForm"]["patron"].value = '';
  });
});
