/*
--------------------------------------------------------------------------------
BUDGET FUNCTIONS /////////////////////////////////////////////////////
--------------------------------------------------------------------------------
*/

// ADD BUDGET
function addBudget() {

  var errors = [];                                              // errors storage
  var values = '';

  // item
  if (document.forms["myForm"]["item"].value == '') {
    errors.push('<p class="error">item can\'t be empty</p>');
  }else {
    var item = document.forms["myForm"]["item"].value;
  }

  // price
  if (document.forms["myForm"]["price"].value == '') {
    errors.push('<p class="error">price can\'t be empty</p>');
  }else {
    var price = document.forms["myForm"]["price"].value;
  }

  // term
  if (document.forms["myForm"]["term"].value == '') {
    errors.push('<p class="error">term can\'t be empty</p>');
  }else {
    var term = document.forms["myForm"]["term"].value;
  }


  // if no errors exist
  if (errors == false) {

    // convert to json string format
    var budget_string = '{"item":"'+item+'", "price":"'+price+'","term":"'+term+'"}';

    // push json string to php file
    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("main-message").innerHTML = this.responseText;
                document.getElementById('main').style.background = '#34ca66';
                $(document).ready(function(){
                  $('.post-form').hide();
                  document.forms["myForm"]["item"].value = '';
                  document.forms["myForm"]["price"].value = '';
                  document.forms["myForm"]["term"].value = '';
                });
                window.setTimeout(function(){
                  window.location = '../budget';
                }, 2000);
            }
        };

        xmlhttp.open("POST", "../../../../../engine/infused_cogs.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("add-budget=" + budget_string);

  }else {
    // if errors exist echo out errors
    for (var i in errors) {
      values += errors[i];
    }
    document.getElementById('messages').innerHTML = values;
  }

  return false;
}


// GET BUDGET DATA
function getBudgetData(value){
  // reset messages to null
  document.getElementById('messages').innerHTML = '';

  $(document).ready(function(){
    $('.add').hide();
    $('.edit').show();
  });

  // get parent info
  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            parentObject = JSON.parse(this.responseText);

            // assign to respective fields
            document.forms["myForm"]["id"].value = parentObject.id;
            document.forms["myForm"]["item"].value = parentObject.item;
            document.forms["myForm"]["price"].value = parentObject.price;
            var a = document.getElementById('term').selectedIndex;
            var b = document.getElementById('term').options;
            b[a].text = 'Term' + parentObject.term;
            b[a].value = parentObject.term;
          }
      };

  xmlhttp.open("GET", "../../../../../engine/infused_cogs.php?get_budget_data="+value, true);
  xmlhttp.send();

  return false;
}

// EDIT BUDGET
function editBudget(){
  var errors = [];                                              // errors storage
  var values = '';

  // item
  if (document.forms["myForm"]["item"].value == '') {
    errors.push('<p class="error">item can\'t be empty</p>');
  }else {
    var item = document.forms["myForm"]["item"].value;
  }

  // price
  if (document.forms["myForm"]["price"].value == '') {
    errors.push('<p class="error">price can\'t be empty</p>');
  }else {
    var price = document.forms["myForm"]["price"].value;
  }

  // term
  if (document.forms["myForm"]["term"].value == '') {
    errors.push('<p class="error">term can\'t be empty</p>');
  }else {
    var term = document.forms["myForm"]["term"].value;
  }

  // if no errors exist
  if (errors == false) {
    var id = document.forms["myForm"]["id"].value;

    // convert to json string format
    var budget_string = '{"id":"'+id+'", "item":"'+item+'", "price":"'+price+'","term":"'+term+'"}';

    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("main-message").innerHTML = this.responseText;
                document.getElementById('main').style.background = '#34ca66';
                $(document).ready(function(){
                  $('.post-form').hide();
                  document.forms["myForm"]["id"].value = '';
                  document.forms["myForm"]["item"].value = '';
                  document.forms["myForm"]["price"].value = '';
                  document.forms["myForm"]["term"].value = '';
                });
                window.setTimeout(function(){
                  window.location = '../budget';
                }, 2000);
            }
        };

        xmlhttp.open("POST", "../../../../../engine/infused_cogs.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("edit-budget-update=" + budget_string);

  }else {
    // if errors exist echo out errors
    for (var i in errors) {
      values += errors[i];
    }
    document.getElementById('messages').innerHTML = values;
  }

  return false;
}

// DELETE BUDGET
function deleteBudget(){
  var id = document.forms["deleteForm"]["id"].value;

  // get parent info
  var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            document.getElementById('main-message').innerHTML = this.responseText;
            document.getElementById('main').style.background = '#34ca66';
            $(document).ready(function(){
              $('.delete-form').hide();
            });
            window.setTimeout(function(){
              window.location = '../budget';
            }, 2000);
          }
      };

  xmlhttp.open("GET", "../../../../../engine/infused_cogs.php?delete_budget="+id, true);
  xmlhttp.send();

  return false;
}

// get Deletion value
function deleteValue(value){
  var id = value;

  document.forms["deleteForm"]["id"].value = id;

  return false;
}

// WHEN CLOSE IT IS CLICKED
$(document).ready(function(){
  $('.closeit').click(function(){
    document.forms["myForm"]["id"].value = '';
    document.forms["myForm"]["item"].value = '';
    document.forms["myForm"]["price"].value = '';
    var a = document.getElementById('term').selectedIndex;
    var b = document.getElementById('term').options;
    b[a].text = 'choose term';
    b[a].value = '';
  });
});
