<?php
# administrative functions
require_once 'infused_cogs.php';            # resource link to all the other cognitive files

$count = 0;  # count variable to echo out on line up


/*
--------------------------------------------
general collective functions
--------------------------------------------
*/

# function clean our data
function clean_data($data) {
  $data = str_replace("'", "`", $data);
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}


# function to get current date
function getCurrentDate(){
  date_default_timezone_set('Africa/Nairobi');
  $date = date('Y-m-d H:i:s');

  return $date;
}


# funtion to shuffle string
function stringShuffle($divide = 4, $string='abcdefghijklmnopqrst0123456789'){

  $string = str_shuffle($string);

  $code = substr($string, 0, strlen($string)/$divide);

  return $code;
}


/*
------------------------------------------------
PARENT FUNCTIONS
------------------------------------------------
*/

# GET PARENT VALUES FROM DB TO DOM
function getParents($admin=true){

  global $conn, $count;

  $sql = "SELECT * FROM parents ORDER BY name ASC";
  $result = $conn->query($sql);

  # get fields into variables
  if($result->num_rows > 0){
    echo '
    <button class="button right show-form">add new parent</button>
    <br><br>
    <div class="parent formal">
      <ul><b>
        <span> # </span>
        <li>name</li>
        <li>email</li>
        <li>phone number</li>
        <li>location</li>
        <li>profession</li></b>
      </ul>
    </div>';
    while($row = $result->fetch_assoc()){
      $count += 1;
      $id = $row['parent_id'];
      $name = $row['name'];
      $email = $row['email'];
      $phone = $row['phone'];
      $location = $row['location'];
      $profession = $row['profession'];

      if ($admin == true) {
        $value = "<a class='material-icons right show-delete' title='delete' style='cursor:pointer' onclick='return deleteValue($id)'>delete</a>
        <a class='material-icons right show-form' title='edit' style='cursor:pointer' onclick='return getParentData($id)'>edit</a>";
      }else {
        $value = null;
      }

      echo "
      <div class='col-sm-12 parent'>
        <ul>
          <span>$count</span>
          <li class='cap'>$name</li>
          <li>$email</li>
          <li class='cap'>$phone</li>
          <li class='cap'>$location</li>
          <li class='cap'>$profession</li>
          $value
        </ul>
      </div>";
    }
  }else {
    echo '
    <div class="add-items">
      <h1>Your parent list is empty</h1>
      <button class="show-form">add new parent</button>
    </div>';
  }
}


# GET PARENT VALUES FROM DB TO JS
if (isset($_GET['get_parent_data'])) {
  $id = $_GET['get_parent_data'];
  $sql = "SELECT * FROM parents WHERE parent_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $count += 1;
    $id = $row['parent_id'];
    $name = $row['name'];
    $email = $row['email'];
    $phone = $row['phone'];
    $location = $row['location'];
    $profession = $row['profession'];
  }

  $parent = new stdClass;
  $parent->id = $id;
  $parent->fullname = $name;
  $parent->email = $email;
  $parent->phone_number = $phone;
  $parent->location = $location;
  $parent->profession = $profession;

  echo json_encode($parent);
}


# ADD PARENT TO DB
if (isset($_POST['add-parent'])) {
  header("Content-Type: application/json; charset=UTF-8");

  global $conn;

  $parents = json_decode($_POST['add-parent']);

  $fullname = clean_data(strtolower($parents->fullname));
  $email = clean_data(strtolower($parents->email));
  $phone_number = clean_data(strtolower($parents->phone_number));
  $location = clean_data(strtolower($parents->location));
  $profession = clean_data(strtolower($parents->profession));

  #Inserting the user's data into our database
  $sql = "INSERT INTO parents ( name, email, phone, location, profession)
  VALUES ('$fullname', '$email', '$phone_number', '$location', '$profession')";

  if ($conn->query($sql) === TRUE) {
    echo "parent added successfully";
  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }
}


# EDIT PARENT
if (isset($_POST['edit-parent-update'])) {
  header("Content-Type: application/json; charset=UTF-8");

  global $conn;

  $parents = json_decode($_POST['edit-parent-update']);

  $parent_id = clean_data(strtolower($parents->parent_id));
  $fullname = clean_data(strtolower($parents->fullname));
  $email = clean_data(strtolower($parents->email));
  $phone_number = clean_data(strtolower($parents->phone_number));
  $location = clean_data(strtolower($parents->location));
  $profession = clean_data(strtolower($parents->profession));

  #Inserting the user's data into our database
  $sql = "UPDATE parents SET `name` = '$fullname', `email` = '$email',  `phone`='$phone_number', `location`='$location', `profession` = '$profession' WHERE parent_id = '$parent_id'";

  if ($conn->query($sql) === TRUE) {
    echo "parent edited successfully";
  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }
}

# DELETE PARENT
if (isset($_GET['delete_parent'])) {
  $id = $_GET['delete_parent'];

  deleteUser('parents', 'parent_id', $id, 'parent');
}


/*
------------------------------------------------
TEACHER FUNCTIONS
------------------------------------------------
*/

// GET TEACHER VALUES FROM DB TO DOM
function getTeachers($admin=true){

  global $conn, $count;

  $value = null;

  $sql = "SELECT * FROM teachers ORDER BY name ASC";
  $result = $conn->query($sql);

  # get fields into variables
  if($result->num_rows > 0){
    echo '
    <button class="button right show-form">add new teacher</button>
    <br><br>
    <div class="parent formal">
      <ul><b>
        <span> # </span>
        <li>name</li>
        <li>email</li>
        <li>phone number</li>
        <li>location</li>
        <li>class teacher</li>
      </b>
      </ul>
    </div>
    ';
    while($row = $result->fetch_assoc()){
      $count += 1;
      $id = $row['teacher_id'];
      $name = $row['name'];
      $email = $row['email'];
      $phone = $row['phone'];
      $location = $row['location'];

      $class = getClassForClassTeacher($id);

      if ($admin == true) {
        $value = "<a class='material-icons right show-delete' title='delete' style='cursor:pointer' onclick='return deleteValue($id)'>delete</a>
        <a class='material-icons right show-form' title='edit' style='cursor:pointer' onclick='return getTeacherData($id)'>edit</a>";
      }else {
        $value = null;
      }

      echo "
      <div class='col-sm-12 parent'>
        <ul>
          <span>$count</span>
          <li class='cap'>$name</li>
          <li>$email</li>
          <li class='cap'>$phone</li>
          <li class='cap'>$location</li>
          <li class='cap'  style='padding:0%!important;'>$class</li>
          $value
        </ul>
      </div>";
    }
  }else {
    echo '
    <div class="add-items">
      <h1>Your teacher list is empty</h1>
      <button class="show-form">add new teacher</button>
    </div>';
  }
}


# GET TEACHER VALUES FROM DB TO JS
if (isset($_GET['get_teacher_data'])) {
  $id = $_GET['get_teacher_data'];
  $sql = "SELECT * FROM teachers WHERE teacher_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $count += 1;
    $id = $row['teacher_id'];
    $name = $row['name'];
    $email = $row['email'];
    $phone = $row['phone'];
    $location = $row['location'];
  }

  $teacher = new stdClass;
  $teacher->id = $id;
  $teacher->fullname = $name;
  $teacher->email = $email;
  $teacher->phone_number = $phone;
  $teacher->location = $location;

  echo json_encode($teacher);
}

# ADD TEACHER TO DB
if (isset($_POST['add-teacher'])) {
  header("Content-Type: application/json; charset=UTF-8");

  global $conn;

  $parents = json_decode($_POST['add-teacher']);

  $fullname = clean_data(strtolower($parents->fullname));
  $email = clean_data(strtolower($parents->email));
  $phone_number = clean_data(strtolower($parents->phone_number));
  $location = clean_data(strtolower($parents->location));

  #Inserting the user's data into our database
  $sql = "INSERT INTO teachers ( name, email, phone, location)
  VALUES ('$fullname', '$email', '$phone_number', '$location')";

  if ($conn->query($sql) === TRUE) {
    echo "teacher added successfully";
  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }

}


// EDIT TEACHER
if (isset($_POST['edit-teacher-update'])) {
  header("Content-Type: application/json; charset=UTF-8");

  global $conn;

  $teachers = json_decode($_POST['edit-teacher-update']);

  $teacher_id = clean_data(strtolower($teachers->parent_id));
  $fullname = clean_data(strtolower($teachers->fullname));
  $email = clean_data(strtolower($teachers->email));
  $phone_number = clean_data(strtolower($teachers->phone_number));
  $location = clean_data(strtolower($teachers->location));

  #Inserting the user's data into our database
  $sql = "UPDATE teachers SET `name` = '$fullname', `email` = '$email',  `phone`='$phone_number', `location`='$location' WHERE teacher_id = '$teacher_id'";

  if ($conn->query($sql) === TRUE) {
    echo "teacher edited successfully";
  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }

}

// DELETE TEACHER
if (isset($_GET['delete_teacher'])) {
  $id = $_GET['delete_teacher'];

  deleteUser('teachers', 'teacher_id', $id, 'teacher');
}


/*
------------------------------------------------
CLASS FUNCTIONS
------------------------------------------------
*/

// GET CLASS VALUES FROM DB TO DOM
function getClasses($admin=true){

  global $conn, $count;

  $sql = "SELECT * FROM classes ORDER BY level ASC";
  $result = $conn->query($sql);

  # get fields into variables
  if($result->num_rows > 0){
    echo '
    <button class="button right show-form">add new class</button>
    <br><br>
    <div class="parent formal">
      <ul><b>
        <span> # </span>
        <li>name</li>
        <li>class teacher</li></b>
      </ul>
    </div>
    ';
    while($row = $result->fetch_assoc()){
      $count += 1;
      $id = $row['class_id'];
      $level = $row['level'];
      $section = $row['section'];
      $class_teacher = $row['class_teacher'];

      if ($admin == true) {
        echo "
        <div class='col-sm-12 parent'>
          <ul>
            <span>$count</span>
            <li class='cap'>".getLevel($level).' '.getSection($section)."</li>
            <li class='cap'>".getTeacher($class_teacher)."</li>
            <a class='material-icons right show-delete' title='delete' style='cursor:pointer' onclick='return deleteValue($id)'>delete</a>
            <a class='material-icons right show-form' title='edit' style='cursor:pointer' onclick='return getClassData($id)'>edit</a>
          </ul>
        </div>
        ";
      }else {
        echo "
        <a href='?class=$id' style='padding:0px;'>
        <div class='col-sm-12 parent' style='cursor:pointer;'>
          <ul>
            <span>$count</span>
            <li class='cap'>".getLevel($level).' '.getSection($section)."</li>
            <li class='cap'>".getTeacher($class_teacher)."</li>
          </ul>
        </div>
        </a>";
      }
    }
  }else {
    echo '
    <div class="add-items">
      <h1>Your class list is empty</h1>
      <button class="show-form">add new class</button>
    </div>';
  }
}


# ADD CLASS TO DB
if (isset($_POST['add-class'])) {
  header("Content-Type: application/json; charset=UTF-8");

  global $conn;

  $class= json_decode($_POST['add-class']);

  $name = clean_data(strtolower($class->name));
  $section = clean_data(strtolower($class->section));
  $class_teacher = clean_data(strtolower($class->class_teacher));

  $sql = "SELECT * FROM classes WHERE level = '$name' AND section = '$section'";
  $result = $conn->query($sql);

  # get fields into variables
  if($result->num_rows == 1){
    echo 1;
  }else if($result->num_rows < 1){
    #Inserting the user's data into our database
    $sql = "INSERT INTO classes ( level, section, class_teacher)
    VALUES ('$name', '$section', '$class_teacher')";

    if ($conn->query($sql) === TRUE) {
      echo "class added successfully";
    }else {
      echo "Error: " . $sql . "<br>" . $conn->error;
    }
  }

}


# GET CLASS VALUES FROM DB TO JS
if (isset($_GET['get_class_data'])) {

  $id = $_GET['get_class_data'];
  $sql = "SELECT * FROM classes WHERE class_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $count += 1;
    $id = $row['class_id'];
    $level_id = $row['level'];
    $level = getLevel($row['level']);
    $section_id = $row['section'];
    $section = getSection($row['section']);
    $teacher_id = $row['class_teacher'];
    $teacher = getTeacher($row['class_teacher']);
  }

  $class = new stdClass;
  $class->id = $id;
  $class->levelid = $level_id;
  $class->level = $level;
  $class->sectionid = $section_id;
  $class->section = $section;
  $class->teacherid = $teacher_id;
  $class->teacher = $teacher;

  echo json_encode($class);
}


// EDIT CLASS
if (isset($_POST['edit-class-update'])) {
  header("Content-Type: application/json; charset=UTF-8");

  global $conn;

  $class = json_decode($_POST['edit-class-update']);

  $id = clean_data(strtolower($class->id));
  $name = clean_data(strtolower($class->name));
  $section = clean_data(strtolower($class->section));
  $class_teacher = clean_data(strtolower($class->teacher));

  #Inserting the user's data into our database
  $sql = "UPDATE classes SET `level` = '$name', `section` = '$section',  `class_teacher`='$class_teacher' WHERE class_id = '$id'";

  if ($conn->query($sql) === TRUE) {
    echo "class edited successfully";
  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }

}

// DELETE TEACHER
if (isset($_GET['delete_class'])) {
  $id = $_GET['delete_class'];

  deleteUser('classes', 'class_id', $id, 'class');
}


/*
------------------------------------------------
STUDENT FUNCTIONS
------------------------------------------------
*/

// GET STUDENT VALUES FROM DB TO DOM
function getStudents($admin=true, $clause='', $id, $link=''){

  global $conn, $count;

  $sql = "SELECT * FROM students WHERE $clause = '$id' ORDER BY name ASC";
  $result = $conn->query($sql);

  if($result->num_rows > 0){
    # get fields into variables
    while($row = $result->fetch_assoc()){
      $count += 1;
      $id = $row['student_id'];
      $admission_number = $row['admission_number'];
      $name = $row['name'];
      $location = $row['location'];
      $phone = $row['phone'];
      $email = $row['email'];
      $birthday = $row['birthday'];
      $parent_id = $row['parent_id'];
      $dormitory_id = $row['dormitory_id'];
      $transport_id = $row['transport_id'];

      if ($admin == true) {
        $value = "<a class='material-icons right show-delete' title='delete' style='cursor:pointer'>delete</a>
        <a href='add-student/?studentid=$id' class='material-icons right show-form' title='edit' style='cursor:pointer' onclick='return getTeacherData($id)'>edit</a>";

        $value2 = "<li class='cap'>".getParent($parent_id)."</li>
        <li class='cap'>".getParentPhone($parent_id)."</li>";
      }else {
        $value = null;
        $value2 = null;
      }

      echo "
      <a href='$link/profile/?student_id=$id' style='padding:0%;'>
      <div class='col-sm-12 parent' style='cursor:pointer'>
        <ul>
          <span>$count</span>
          <li class='cap'>$name</li>
          <li class='cap' style='width:10%;'>$admission_number</li>
          <li>$email</li>
          <li class='cap' style='width:10%;'>$phone</li>
          $value2
          $value
        </ul>
      </div>
      </a>
      ";
    }
  }else {
    echo "<p style='color:grey'>No students in this category</p>";
  }
}


# ADD STUDENT FUNCTIONS
function addStudent(){

  global $conn;
  $errors = array();
  $adm = null;
  $way = null;

  if (isset($_POST['add-student'])) {

    # admission number
    if ($_POST['adm'] == '') {
      $errors[] = "<p class='error'>admission number is required</p>";
    }else {
      $adm = clean_data(strtolower($_POST['adm']));
    }

    # full name
    if ($_POST['name'] == '') {
      $errors[] = "<p class='error'>full name is required</p>";
    }else {
      $name = clean_data(strtolower($_POST['name']));
    }

    # phone number
    if (isset($_POST['phone'])) {
      $phone = clean_data(strtolower($_POST['phone']));
    }

    # email
    if (isset($_POST['email'])) {
      $email = clean_data(strtolower($_POST['email']));
    }

    # location
    if (isset($_POST['location'])) {
      $location = clean_data(strtolower($_POST['location']));
    }

    # birthday
    if (isset($_POST['birthday'])) {
      $birthday = clean_data(strtolower($_POST['birthday']));
    }

    # medical info
    if (isset($_POST['medicalinfo'])) {
      $medicalinfo = clean_data($_POST['medicalinfo']);
    }

    # medical contact
    if (isset($_POST['medicalcontact'])) {
      $medicalcontact = clean_data(strtolower($_POST['medicalcontact']));
    }

    # parent id
    if (isset($_POST['parent'])) {
      $parent_id = clean_data(strtolower($_POST['parent']));
    }

    # class id
    if ($_POST['class'] == '') {
      $errors[] = "<p class='error'>class is required</p>";
    }else {
      $class_id = clean_data(strtolower($_POST['class']));
    }

    # club id
    if (isset($_POST['club'])) {
      $club_id = clean_data(strtolower($_POST['club']));
    }

    # dormitory
    if (isset($_POST['dormitory'])) {
      $dormitory_id = clean_data(strtolower($_POST['dormitory']));
    }

    # transport
    if (isset($_POST['transport'])) {
      $transport_id = clean_data(strtolower($_POST['transport']));
    }

    # way
    if ($_POST['way'] != '') {
      if ($_POST['transport'] == '') {
        $errors[] = "<p class='error'>You need to enter transport route first</p>";
      }else {
        $way = clean_data($_POST['way']);
      }
      $way = clean_data($_POST['way']);
    }elseif ($_POST['way'] == '') {
      if ($_POST['transport'] != '') {
        $errors[] = "<p class='error'>You need to choose way</p>";
      }
    }


    if ($adm != null) {
      # confirm unique student admission number
      $sql = "SELECT * FROM students";
      $result = $conn->query($sql);

      # get fields into variables
      while($row = $result->fetch_assoc()){
        $dbadm = $row['admission_number'];

        if ($dbadm == $adm) {
          $errors[] = "<p class='error'>That admission number is already in use</p>";
          break;
        }
      }
    }

    # account number
    $accountno = stringShuffle($divide = 1, $string = '0123456789');

    if ($errors == []) {
      $sql = "INSERT INTO students ( admission_number, name, phone, location, email, parent_id, birthday, medical_information, medical_contact, class_id, accountno, club_id, dormitory_id, transport_id, way)
      VALUES ('$adm', '$name', '$phone', '$location', '$email', '$parent_id', '$birthday', '$medicalinfo', '$medicalcontact',  '$class_id', '$accountno', '$club_id', '$dormitory_id', '$transport_id', '$way')";

      if ($conn->query($sql) === TRUE) {

        #Inserting the user's data into our database
        $sql = "INSERT INTO student_accounts ( accountno)
        VALUES ('$accountno')";

        if ($conn->query($sql) === TRUE) {
          echo "<p class='success'>student added successfully</p>";
        }else {
          echo "Error: " . $sql . "<br>" . $conn->error;
        }

      }else {
        echo "Error: " . $sql . "<br>" . $conn->error;
      }
    }else {
      foreach ($errors as $error) {
        echo $error;
      }
    }
  }
}

# ADD STUDENT FUNCTIONS
function editStudent($id){

  global $conn;
  $errors = array();
  $adm = null;
  $way = null;

  if (isset($_POST['edit-student'])) {

    # admission number
    if ($_POST['adm'] == '') {
      $errors[] = "<p class='error'>admission number is required</p>";
    }else {
      $adm = clean_data(strtolower($_POST['adm']));
    }

    # full name
    if ($_POST['name'] == '') {
      $errors[] = "<p class='error'>full name is required</p>";
    }else {
      $name = clean_data(strtolower($_POST['name']));
    }

    # phone number
    if (isset($_POST['phone'])) {
      $phone = clean_data(strtolower($_POST['phone']));
    }

    # email
    if (isset($_POST['email'])) {
      $email = clean_data(strtolower($_POST['email']));
    }

    # location
    if (isset($_POST['location'])) {
      $location = clean_data(strtolower($_POST['location']));
    }

    # birthday
    if (isset($_POST['birthday'])) {
      $birthday = clean_data(strtolower($_POST['birthday']));
    }

    # medical info
    if (isset($_POST['medicalinfo'])) {
      $medicalinfo = clean_data($_POST['medicalinfo']);
    }

    # medical contact
    if (isset($_POST['medicalcontact'])) {
      $medicalcontact = clean_data(strtolower($_POST['medicalcontact']));
    }

    # parent id
    if (isset($_POST['parent'])) {
      $parent_id = clean_data(strtolower($_POST['parent']));
    }

    # class id
    if ($_POST['class'] == '') {
      $errors[] = "<p class='error'>class is required</p>";
    }else {
      $class_id = clean_data(strtolower($_POST['class']));
    }

    # club id
    if (isset($_POST['club'])) {
      $club_id = clean_data(strtolower($_POST['club']));
    }

    # dormitory
    if (isset($_POST['dormitory'])) {
      $dormitory_id = clean_data(strtolower($_POST['dormitory']));
    }

    # transport
    if (isset($_POST['transport'])) {
      $transport_id = clean_data(strtolower($_POST['transport']));
    }

    # way
    if ($_POST['way'] != '') {
      if ($_POST['transport'] == '') {
        $errors[] = "<p class='error'>You need to enter transport route first</p>";
      }else {
        $way = clean_data($_POST['way']);
      }
      $way = clean_data($_POST['way']);
    }


    if ($errors == []) {
      $sql = "UPDATE students SET `admission_number` = '$adm', `name` = '$name', `phone` = '$phone' , `location` = '$location' , `email` = '$email', `parent_id` = '$parent_id',
       `birthday` = '$birthday', `medical_information` = '$medicalinfo', `medical_contact` = '$medicalcontact', `class_id` = '$class_id',
       `club_id` ='$club_id', `dormitory_id` ='$dormitory_id', `transport_id` ='$transport_id', `way` ='$way' WHERE student_id = $id";

      if ($conn->query($sql) === TRUE) {
        echo "<p class='success'>student edited successfully</p>";
      }else {
        echo "Error: " . $sql . "<br>" . $conn->error;
      }
    }else {
      foreach ($errors as $error) {
        echo $error;
      }
    }
  }
}



# ADD BULK STUDENTS
function addBulkStudents(){

  global $conn;

  if (isset($_POST['add-bulk-students'])) {

    $file_name = $_FILES["excelfile"]["tmp_name"];
    $file_open = fopen($file_name, "r");

    while (($csv = fgetcsv($file_open, 1000, ",")) !== false) {
      $adm = $csv[0];
      $name = $csv[0];
      $phone = $csv[1];
      $location = $csv[2];
      $email = $csv[3];
      $parent_id = $csv[4];
      $birthday = $csv[5];
      $class_id = $csv[6];
      $accountno = $csv[7];
      $club_id = $csv[8];
      $dormitory_id = $csv[9];
      $transport_id = $csv[10];
      $way = $csv[11];

      $stmt = $conn->prepare("INSERT INTO students ( admission_number, name, phone, location, email, parent_id, birthday, class_id, accountno, club_id, dormitory_id, transport_id, way)
      VALUES ('$adm', '$name', '$phone', '$location', '$email', '$parent_id', '$birthday', '$class_id', '$accountno', '$club_id', '$dormitory_id', '$transport_id', '$way')");

      $stmt->bind_param('admission_number', $adm);
      $stmt->bind_param(':name', $name);
      $stmt->bind_param(':phone', $phone);
      $stmt->bind_param(':location', $location);
      $stmt->bind_param(':email', $email);
      $stmt->bind_param(':parent_id', $parent_id);
      $stmt->bind_param(':birthday', $birthday);
      $stmt->bind_param(':class_id', $class_id);
      $stmt->bind_param(':accountno', $accountno);
      $stmt->bind_param(':accountno', $accountno);
      $stmt->bind_param(':club_id', $club_id);
      $stmt->bind_param(':dormitory_id', $dormitory_id);
      $stmt->bind_param(':transport_id', $transport_id);
      $stmt->bind_param(':way', $way);

      $stmt->execute();

      echo "success";
    }

  }
}










/*
--------------------------------------------
LEVELS FUNCTIONS
--------------------------------------------
*/
function getLevels($id){

  global $conn, $count;

  $sql = "SELECT * FROM classes WHERE level = '$id' ORDER BY section ASC";
  $result = $conn->query($sql);

  if($result->num_rows > 0){
    # get fields into variables
    while($row = $result->fetch_assoc()){
      $count += 1;
      $id = $row['class_id'];
      $level = $row['level'];
      $section = $row['section'];
      $class_teacher = $row['class_teacher'];

      echo "
      <a href='?class=$id&level=$level&section=$section'>
      <div class='col-sm-12 parent' style='cursor:pointer;padding-bottom:1%;'>
        <span>$count</span>
        <li class='cap'>".getLevel($level)."</li>
        <li class='cap'>".getSection($section)."</li>
        <li class='cap'>".getTeacher($class_teacher)."</li>
      </div>";
    }
  }else {
    echo "<p style='color:grey'>No assigned sections in this level</p>";
  }
}

# GET ADMINISTRATOR LEVELS
function getAdminLevels(){

  global $conn, $count;

  $sql = "SELECT * FROM levels WHERE level_name != 'admission' ORDER BY level_name ASC";
  $result = $conn->query($sql);

  if($result->num_rows > 0){
    echo '
    <button class="button right show-form">add new level</button>
    <br><br>
    <div class="parent formal">
      <ul><b>
        <span> # </span>
        <li>name</li></b>
      </ul>
    </div>
    ';
    # get fields into variables
    while($row = $result->fetch_assoc()){
      $count += 1;
      $id = $row['level_id'];
      $level = $row['level_name'];

      echo "
      <div class='col-sm-12 parent' style='cursor:pointer;padding-bottom:1%;'>
        <span>$count</span>
        <li class='cap'>$level</li>
        <a class='material-icons right show-delete' title='delete' style='cursor:pointer' onclick='return deleteValue($id)'>delete</a>
        <a class='material-icons right show-form' title='edit' style='cursor:pointer' onclick='return getLevelData($id)'>edit</a>
      </div>";
    }
  }else {
    echo '
    <div class="add-items">
      <h1>Your level list is empty</h1>
      <button class="show-form">add new level</button>
    </div>';
  }
}

# ADD LEVEL TO DB
if (isset($_POST['add-level'])) {
  header("Content-Type: application/json; charset=UTF-8");

  global $conn;

  $levels = json_decode($_POST['add-level']);

  $level = clean_data(strtolower($levels->level));

  #Inserting the user's data into our database
  $sql = "INSERT INTO levels (level_name)
  VALUES ('$level')";

  if ($conn->query($sql) === TRUE) {
    echo "level added successfully";
  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }

}

# GET LEVEL VALUES FROM DB TO JS
if (isset($_GET['get_level_data'])) {

  $id = $_GET['get_level_data'];

  $sql = "SELECT * FROM levels WHERE level_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $id = $row['level_id'];
    $level = $row['level_name'];
  }

  $levels = new stdClass;
  $levels->id = $id;
  $levels->level = $level;

  echo json_encode($levels);

}


// EDIT LEVEL
if (isset($_POST['edit-level-update'])) {
  header("Content-Type: application/json; charset=UTF-8");

  global $conn;

  $levels = json_decode($_POST['edit-level-update']);

  $id = clean_data(strtolower($levels->id));
  $level = clean_data(strtolower($levels->level));

  #Inserting the user's data into our database
  $sql = "UPDATE levels SET `level_name` = '$level' WHERE level_id = '$id'";

  if ($conn->query($sql) === TRUE) {
    echo "level edited successfully";
  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }

}

// DELETE LEVEL
if (isset($_GET['delete_level'])) {
  $id = $_GET['delete_level'];

  deleteUser('levels', 'level_id', $id, 'level');
}




/*
--------------------------------------------
SECTION FUNCTIONS
--------------------------------------------
*/
# function to get Sections
function getSections(){
  global $conn, $count;

  $sql = "SELECT * FROM sections";
  $result = $conn->query($sql);

  # get fields into variables
  if($result->num_rows > 0){
    echo '
    <button class="button right show-form">add new section</button>
    <br><br>
    <div class="parent formal">
      <ul><b>
        <span> # </span>
        <li>name</li></b>
      </ul>
    </div>
    ';
    while($row = $result->fetch_assoc()){
      $count += 1;
      $id = $row['section_id'];
      $name = $row['name'];

      echo "
      <div class='col-sm-12 parent'>
        <ul>
          <span>$count</span>
          <li class='cap'>$name</li>
          <a class='material-icons right show-delete' title='delete' style='cursor:pointer' onclick='return deleteValue($id)'>delete</a>
          <a class='material-icons right show-form' title='edit' style='cursor:pointer' onclick='return getSectionData($id)'>edit</a>
        </ul>
      </div>";
    }
  }else {
    echo '
    <div class="add-items">
      <h1>Your section list is empty</h1>
      <button class="show-form">add new section</button>
    </div>';
  }
}

# ADD SECTION TO DB
if (isset($_POST['add-section'])) {
  header("Content-Type: application/json; charset=UTF-8");

  global $conn;

  $sections = json_decode($_POST['add-section']);

  $section = clean_data(strtolower($sections->section));

  #Inserting the user's data into our database
  $sql = "INSERT INTO sections (name)
  VALUES ('$section')";

  if ($conn->query($sql) === TRUE) {
    echo "section added successfully";
  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }

}

# GET SECTIONS VALUES FROM DB TO JS
if (isset($_GET['get_section_data'])) {

  $id = $_GET['get_section_data'];

  $sql = "SELECT * FROM sections WHERE section_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $id = $row['section_id'];
    $section = $row['name'];
  }

  $sections = new stdClass;
  $sections->id = $id;
  $sections->section = $section;

  echo json_encode($sections);

}


// EDIT SECTION
if (isset($_POST['edit-section-update'])) {
  header("Content-Type: application/json; charset=UTF-8");

  global $conn;

  $sections = json_decode($_POST['edit-section-update']);

  $id = clean_data(strtolower($sections->id));
  $section = clean_data(strtolower($sections->section));

  #Inserting the user's data into our database
  $sql = "UPDATE sections SET `name` = '$section' WHERE section_id = '$id'";

  if ($conn->query($sql) === TRUE) {
    echo "section edited successfully";
  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }

}

// DELETE SECTION
if (isset($_GET['delete_section'])) {
  $id = $_GET['delete_section'];

  deleteUser('sections', 'section_id', $id, 'section');
}











/*
--------------------------------------------
LIBRARY FUNCTIONS
--------------------------------------------
*/
# GET BOOKS
function getBooks($admin=true){
  global $conn, $count;

  $sql = "SELECT * FROM books ORDER BY name ASC";
  $result = $conn->query($sql);

  # get fields into variables
  if($result->num_rows > 0){
    echo '
    <div class="parent formal">
      <ul><b>
        <span> # </span>
        <li>name</li>
        <li>author</li>
        <li>description</li>
        <li style="width:10%;">price</li>
        <li style="width:7%;">stock</li>
        <li style="width:7%;">current</li>
        <li style="width:7%;">issued</li>
      </b>
      </ul>
    </div>
    ';
    while($row = $result->fetch_assoc()){
      $count += 1;
      $id = $row['book_id'];
      $name = $row['name'];
      $author = $row['author'];
      $description = $row['description'];
      $cut_description = substr($description, 0, 30);
      $cut_description = substr($cut_description, 0, strrpos($cut_description, ' ')) . '...';
      $price = $row['price'];
      $input_count = $row['input_count'];
      $book_count = $row['count'];
      $issued = $row['issued'];

      $issued = $input_count - $book_count;

      $sql = "UPDATE books SET `issued` = '$issued' WHERE book_id = '$id'";

      if ($conn->query($sql) === TRUE) {
      }else {
        echo "Error: " . $sql . "<br>" . $conn->error;
      }

      if ($book_count == 0) {
        $point = null;
      }else {
        $point = "<a class='material-icons right show-assign' title='assign book' style='cursor:pointer' onclick='getBookAssignData($id)'>add_box</a>";
      }

      if ($admin == true) {
        $value = "
        <a class='material-icons right show-delete' title='delete' style='cursor:pointer' onclick='return deleteValue($id)'>delete</a>
        <a class='material-icons right show-form' title='edit' style='cursor:pointer' onclick='return getBookData($id)'>edit</a>
        $point
        ";
        $button = "<button class='show-form'>add new section</button>";
      }else {
        $value = null;
        $button = null;
      }

      echo "
      <div class='col-sm-12 parent'>
        <ul>
          <span>$count</span>
          <li class='cap'>$name</li>
          <li class='cap'>$author</li>
          <li title='$description'>$cut_description</li>
          <li class='cap' style='width:10%'>ksh $price</li>
          <li style='width:7%;'>$input_count</li>
          <li style='width:7%;'>$book_count</li>
          <li style='width:7%;'>$issued</li>
          $value
        </ul>
      </div>";
    }
  }else {

    if ($admin == true) {
      $button = "<button class='show-form'>add new section</button>";
    }else {
      $button = null;
    }

    echo '
    <div class="add-items">
      <h1>No books in the library</h1>
      '.$button.'
    </div>';
  }
}

# ADD BOOK TO DB
if (isset($_POST['add-book'])) {
  header("Content-Type: application/json; charset=UTF-8");

  global $conn;

  $books = json_decode($_POST['add-book']);

  $bookname = clean_data(strtolower($books->bookname));
  $author = clean_data(strtolower($books->author));
  $description = clean_data($books->description);
  $price = clean_data(strtolower($books->price));
  $count = clean_data(strtolower($books->count));

  #Inserting the user's data into our database
  $sql = "INSERT INTO books ( name, author, description, price, input_count, count)
  VALUES ('$bookname', '$author', '$description', '$price', '$count', '$count')";

  if ($conn->query($sql) === TRUE) {
    echo "book added successfully";
  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }

}

# GET BOOK VALUES FROM DB TO JS
if (isset($_GET['get_book_data'])) {
  $id = $_GET['get_book_data'];
  $sql = "SELECT * FROM books WHERE book_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $count += 1;
    $id = $row['book_id'];
    $name = $row['name'];
    $author = $row['author'];
    $description = $row['description'];
    $price = $row['price'];
    $book_count = $row['count'];
  }

  $books = new stdClass;
  $books->id = $id;
  $books->code = strtoupper(stringShuffle($divide = 3));
  $books->name = $name;
  $books->author = $author;
  $books->description = $description;
  $books->price = $price;
  $books->count = $book_count;

  echo json_encode($books);
}

// EDIT BOOK
if (isset($_POST['edit-book-update'])) {
  header("Content-Type: application/json; charset=UTF-8");

  global $conn;

  $books = json_decode($_POST['edit-book-update']);

  $book_id = clean_data(strtolower($books->book_id));
  $bookname = clean_data(strtolower($books->bookname));
  $author = clean_data(strtolower($books->author));
  $description = clean_data(strtolower($books->description));
  $price = clean_data(strtolower($books->price));
  $count = clean_data(strtolower($books->count));

  #Inserting the user's data into our database
  $sql = "UPDATE books SET `name` = '$bookname', `author` = '$author',  `description`='$description', `price`='$price', `count`='$count' WHERE book_id = '$book_id'";

  if ($conn->query($sql) === TRUE) {
    echo "book edited successfully";
  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }
}

// STUDENT NAMES FOR BOOK GIVING
if (isset($_GET['get_student_search_data'])) {
  $value = $_GET['get_student_search_data'];

  $sql = "SELECT * FROM students WHERE name LIKE '%".$value."%'";
  $result = $conn->query($sql);

  # get fields into variables
  if($result->num_rows > 0){
    while($row = $result->fetch_assoc()){
      $count += 1;
      $id = $row['student_id'];
      $name = $row['name'];
      $adm = $row['admission_number'];
      $accountno = $row['accountno'];

      $studentdata = new stdClass;
      $studentdata->id = $id;
      $studentdata->name = $name;
      $studentdata->adm = $adm;
      $studentdata->account = $accountno;

      $value = json_encode($studentdata);

      echo "<p class='cap'><b>Adm no:</b> $adm <b>Name:</b> $name <button class='button' onclick='return assignBook($value)'>add student</button></p>";
    }
  }else {
    echo "<p style='color:grey'>No student exists by that name</p>";
  }
}

// DELETE BOOK
if (isset($_GET['delete_book'])) {
  $id = $_GET['delete_book'];

  deleteUser('books', 'book_id', $id, 'book');
}


// ISSUE BOOK
if (isset($_POST['issue_book'])) {
  header("Content-Type: application/json; charset=UTF-8");

  global $conn;

  $issue = json_decode($_POST['issue_book']);

  $serialcode = clean_data(strtolower($issue->serialcode));
  $studentid = clean_data(strtolower($issue->studentid));
  $bookid = clean_data($issue->bookid);

  $sql = "SELECT count FROM books WHERE book_id = '$bookid'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $book_count = $row['count'];
  }

  if ($book_count > 0) {

    $book_count = $book_count - 1;

    #Inserting the user's data into our database
    $sql = "INSERT INTO borrowed_books ( serial_code, student_id, book_id)
    VALUES ('$serialcode', '$studentid', '$bookid')";

    if ($conn->query($sql) === TRUE) {

      $sql = "UPDATE books SET `count`='$book_count' WHERE book_id = '$bookid'";

      if ($conn->query($sql) === TRUE) {
        echo "book assigned successfully";
      }else {
        echo "Error: " . $sql . "<br>" . $conn->error;
      }

    }else {
      echo "Error: " . $sql . "<br>" . $conn->error;
    }
  }else {
    echo 0;
  }
}


# GET BORROWED BOOKS
function getBorrowedBooks($admin=true){
  global $conn, $count;

  $sql = "SELECT * FROM borrowed_books WHERE status = 'borrowed' ORDER BY date_issued ASC";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $count += 1;
    $id = $row['issue_id'];
    $serialcode = $row['serial_code'];
    $bookid = $row['book_id'];
    $studentid = $row['student_id'];
    $date_issued = date_format(new dateTime($row['date_issued']), "jS M Y H:i:s");
    // Assign variables
    $bookname = getBook($bookid);
    $borrower = getStudent($studentid);

    if ($admin == true) {
      $value = "
      <a class='material-icons right show-delete' title='delete' style='cursor:pointer'>delete</a>";
    }else {
      $value = null;
    }

    if (timeSpan($date_issued) < 1) {
      $value = 'overdue';
      $style = "style='background-color:#ff3c3c; color:#fff'";
    }else {
      $value = timeSpan($date_issued).' left';
      $style = "";
    }

    echo "
    <div class='col-sm-12 parent' $style>
      <ul>
        <span>$count</span>
        <li>".strtoupper($serialcode)."</li>
        <li class='cap'>".getBook($bookid)."</li>
        <li class='cap'>".getStudentAdm($studentid)."</li>
        <li class='cap'>".getStudent($studentid)."</li>
        <li>$value</li>
        <a class='material-icons right show-delete' title='return book' style='cursor:pointer' onclick='return deleteValue($id)'>label</a>
      </ul>
    </div>";
  }
}

# GET BORROWED BOOKS
function getBorrowHistory(){
  global $conn, $count;

  $sql = "SELECT * FROM borrowed_books ORDER BY date_issued ASC";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $count += 1;
    $id = $row['issue_id'];
    $serialcode = $row['serial_code'];
    $bookid = $row['book_id'];
    $studentid = $row['student_id'];
    $date_issued = date_format(new dateTime($row['date_issued']), "jS M Y H:i:s");
    $status = $row['status'];

    echo "
    <div class='col-sm-12 parent'>
      <ul>
        <span>$count</span>
        <li>".strtoupper($serialcode)."</li>
        <li class='cap'>".getBook($bookid)."</li>
        <li class='cap' style='width:10%;'>".getStudentAdm($studentid)."</li>
        <li class='cap'>".getStudent($studentid)."</li>
        <li class='cap'><b>".$date_issued."</b></li>
        <li class='cap'>".$status."</li>
      </ul>
    </div>";
  }
}

# return book
if (isset($_GET['return_book'])) {

  $issue_id = $_GET['return_book'];

  global $conn;

  // get book id
  $sql = "SELECT * FROM borrowed_books WHERE issue_id = '$issue_id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $book_id = $row['book_id'];
  }

  //
  $sql = "SELECT * FROM books WHERE book_id = '$book_id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $book_count = $row['count'];
  }

  $book_count = $book_count + 1;

  $sql = "UPDATE borrowed_books SET `status` = 'returned' WHERE issue_id = '$issue_id'";

  if ($conn->query($sql) === TRUE) {
    $sql = "UPDATE books SET `count`='$book_count' WHERE book_id = '$book_id'";

    if ($conn->query($sql) === TRUE) {
      echo "book returned successfully";
    }else {
      echo "Error: " . $sql . "<br>" . $conn->error;
    }
  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }
}








/*
--------------------------------------------
CLUBS
--------------------------------------------
*/
# GET CLUBS
function getClubs($admin=true){
  global $conn, $count;

  $sql = "SELECT * FROM clubs ORDER BY name ASC";
  $result = $conn->query($sql);

  # get fields into variables
  if($result->num_rows > 0){
    echo "
    <button class='button right show-form'>add new club</button>
    <br><br>
    <div class='parent formal'>
      <ul><b>
        <span> # </span>
        <li>club</li>
        <li>description</li>
        <li>chairman</li>
        <li>vice chairman</li>
        <li>patron</li>
      </b>
      </ul>
    </div>
    ";
    while($row = $result->fetch_assoc()){
      $count += 1;
      $id = $row['club_id'];
      $name = $row['name'];
      $description = $row['description'];
      $cut_description = substr($description, 0, 30);
      $cut_description = substr($cut_description, 0, strrpos($cut_description, ' ')) . '...';
      $chair = $row['chair'];
      $vice = $row['vice'];
      $patron = $row['patron'];

      if ($admin == true) {
        $value = "
        <a class='material-icons right show-delete' title='delete' style='cursor:pointer' onclick='return deleteValue($id)'>delete</a>
        <a class='material-icons right show-form' title='edit' style='cursor:pointer' onclick='return getClubData($id)'>edit</a>";
      }else {
        $value = null;
      }

      echo "
      <a href='?club=$id' style='padding:0%;'>
      <div class='col-sm-12 parent'>
        <ul>
          <span>$count</span>
          <li class='cap'>$name</li>
          <li title='$description'>$cut_description</li>
          <li class='cap'>".getStudent($chair)."</li>
          <li class='cap'>".getStudent($vice)."</li>
          <li class='cap'>".getTeacher($patron)."</li>
          $value
        </ul>
      </div>
      </a>";
    }
  }else {
    echo '
    <div class="add-items">
      <h1>Your club list is empty</h1>
      <button class="show-form">add new club</button>
    </div>';
  }
}

# ADD CLUB TO DB
if (isset($_POST['add-club'])) {
  header("Content-Type: application/json; charset=UTF-8");

  global $conn;

  $club = json_decode($_POST['add-club']);

  $name = clean_data(strtolower($club->name));
  $description = clean_data($club->description);
  $chair = clean_data(strtolower($club->chair));
  $vice = clean_data(strtolower($club->vice));
  $patron = clean_data(strtolower($club->patron));

  #Inserting the user's data into our database
  $sql = "INSERT INTO clubs ( name, description, chair, vice, patron)
  VALUES ('$name', '$description', '$chair', '$vice', '$patron')";

  if ($conn->query($sql) === TRUE) {
    echo "club added successfully";
  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }

}

# GET CLUB VALUES FROM DB TO JS
if (isset($_GET['get_club_data'])) {

  $id = $_GET['get_club_data'];

  $sql = "SELECT * FROM clubs WHERE club_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $count += 1;
    $id = $row['club_id'];
    $name = $row['name'];
    $description = $row['description'];
    $chair = $row['chair'];
    $vice = $row['vice'];
    $patron = $row['patron'];
  }

  $club = new stdClass;
  $club->id = $id;
  $club->name = $name;
  $club->description = $description;
  $club->chair = $chair;
  $club->vice = $vice;
  $club->patron = $patron;

  echo json_encode($club);

}


// EDIT CLUB
if (isset($_POST['edit-club-update'])) {
  header("Content-Type: application/json; charset=UTF-8");

  global $conn;

  $club = json_decode($_POST['edit-club-update']);

  $id = clean_data(strtolower($club->id));
  $name = clean_data(strtolower($club->name));
  $description = clean_data($club->description);
  $chair = clean_data(strtolower($club->chair));
  $vice = clean_data(strtolower($club->vice));
  $patron = clean_data(strtolower($club->patron));

  #Inserting the user's data into our database
  $sql = "UPDATE clubs SET `name` = '$name', `description` = '$description', `chair` = '$chair',  `vice`='$vice', `patron`='$patron' WHERE club_id = '$id'";

  if ($conn->query($sql) === TRUE) {
    echo "club edited successfully";
  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }

}

// DELETE CLUB
if (isset($_GET['delete_club'])) {
  $id = $_GET['delete_club'];

  deleteUser('clubs', 'club_id', $id, 'club');
}








/*
--------------------------------------------
TRANSPORT FUNCTIONS
--------------------------------------------
*/
// GET ALL TRANSPORT ROUTES
function getTransportRoutes(){
  global $conn, $count;

  $sql = "SELECT * FROM transport ORDER BY name ASC";
  $result = $conn->query($sql);

  # get fields into variables
  if($result->num_rows > 0){
    echo "
    <button class='button right show-form'>add new route</button>
    <br><br>
    <div class='parent formal'>
      <ul><b>
        <span> # </span>
        <li>route</li>
        <li style='width:15%;'>number plate</li>
        <li>driver</li>
        <li>phone</li>
        <li style='width:10%;'>one way</li>
        <li style='width:10%;'>two way</li>
      </b>
      </ul>
    </div>
    ";
    while($row = $result->fetch_assoc()){
      $count += 1;
      $id = $row['transport_id'];
      $name = $row['name'];
      $plate = $row['number_plate'];
      $driver= $row['driver'];
      $phone= $row['phone'];
      $one_way= $row['one_way'];
      $two_way = $row['two_way'];


      echo "
      <a href='?route=$id' style='padding:0%;'>
      <div class='col-sm-12 parent'>
        <ul>
          <span>$count</span>
          <li>$name</li>
          <li class='cap' style='width:15%;'>".strtoupper($plate)."</li>
          <li class='cap'>$driver</li>
          <li class='cap'>$phone</li>
          <li class='cap' style='width:10%;'>Ksh $one_way</li>
          <li class='cap' style='width:10%;'>Ksh $two_way</li>
          <a class='material-icons right show-delete' title='delete' style='cursor:pointer' onclick='return deleteValue($id)'>delete</a>
          <a class='material-icons right show-form' title='edit' style='cursor:pointer' onclick='return getRouteData($id)'>edit</a>
        </ul>
      </div></a>";
    }
  }else {
    echo '
    <div class="add-items">
      <h1>Your transport routes are empty</h1>
      <button class="show-form">add new route</button>
    </div>';
  }
}

# ADD ROUTE TO DB
if (isset($_POST['add-route'])) {
  header("Content-Type: application/json; charset=UTF-8");

  global $conn;

  $parents = json_decode($_POST['add-route']);

  $route = clean_data(strtolower($parents->route));
  $plate = clean_data(strtolower($parents->plate));
  $driver = clean_data(strtolower($parents->driver));
  $phone = clean_data(strtolower($parents->phone));
  $oneway = clean_data(strtolower($parents->oneway));
  $twoway = clean_data(strtolower($parents->twoway));

  #Inserting the user's data into our database
  $sql = "INSERT INTO transport ( name, number_plate, driver, phone, one_way, two_way)
  VALUES ('$route', '$plate', '$driver', '$phone', '$oneway', '$twoway')";

  if ($conn->query($sql) === TRUE) {
    echo "route added successfully";
  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }

}

# GET ROUTE VALUES FROM DB TO JS
if (isset($_GET['get_route_data'])) {

  $id = $_GET['get_route_data'];

  $sql = "SELECT * FROM transport WHERE transport_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $id = $row['transport_id'];
    $name = $row['name'];
    $plate = $row['number_plate'];
    $driver= $row['driver'];
    $phone= $row['phone'];
    $one_way= $row['one_way'];
    $two_way = $row['two_way'];
  }

  $troute = new stdClass;
  $troute->id = $id;
  $troute->route = $name;
  $troute->plate = $plate;
  $troute->driver = $driver;
  $troute->phone = $phone;
  $troute->oneway = $one_way;
  $troute->twoway = $two_way;

  echo json_encode($troute);

}


// EDIT ROUTE
if (isset($_POST['edit-route-update'])) {
  header("Content-Type: application/json; charset=UTF-8");

  global $conn;

  $troute = json_decode($_POST['edit-route-update']);

  $id = clean_data(strtolower($troute->id));
  $route = clean_data(strtolower($troute->route));
  $plate = clean_data(strtolower($troute->plate));
  $driver = clean_data(strtolower($troute->driver));
  $phone = clean_data(strtolower($troute->phone));
  $oneway = clean_data(strtolower($troute->oneway));
  $twoway = clean_data(strtolower($troute->twoway));

  #Inserting the user's data into our database
  $sql = "UPDATE transport SET `name` = '$route', `number_plate` = '$plate', `driver` = '$driver',  `phone`='$phone', `one_way`='$oneway', `two_way`='$twoway' WHERE transport_id = '$id'";

  if ($conn->query($sql) === TRUE) {
    echo "route edited successfully";
  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }

}

// DELETE ROUTE
if (isset($_GET['delete_route'])) {
  $id = $_GET['delete_route'];

  deleteUser('transport', 'transport_id', $id, 'route');
}







/*
--------------------------------------------
DORMITORY FUNCTIONS
--------------------------------------------
*/
// GET DORMITORIES
function getDormitories(){
  global $conn, $count;

  $sql = "SELECT * FROM dormitories ORDER BY name ASC";
  $result = $conn->query($sql);

  # get fields into variables
  if($result->num_rows > 0){
    echo "
    <button class='button right show-form'>add new dorm</button>
    <br><br>
    <div class='parent formal'>
      <ul><b>
        <span> # </span>
        <li>dorm name</li>
        <li>total bed count</li>
        <li>occupied</li>
        <li>vacant</li>
      </b>
      </ul>
    </div>
    ";
    while($row = $result->fetch_assoc()){
      $count += 1;
      $id = $row['dormitory_id'];
      $name = $row['name'];
      $beds = $row['beds'];
      $occupied = $row['occupied'];
      $vacant = $row['vacant'];

      $occupied = getDormitoryCount($id);
      $vacant = $beds - $occupied;

      $sql = "UPDATE dormitories SET `vacant` = '$vacant', `occupied` = '$occupied'  WHERE dormitory_id = '$id'";

      if ($conn->query($sql) === TRUE) {
      }else {
        echo "Error: " . $sql . "<br>" . $conn->error;
      }

      echo "
      <a href='?dorm=$id' style='padding:0%;'>
      <div class='col-sm-12 parent'>
        <ul>
          <span>$count</span>
          <li class='cap'>$name</li>
          <li class='cap'>$beds</li>
          <li class='cap'>$occupied</li>
          <li class='cap'>$vacant</li>
          <a class='material-icons right show-delete' title='delete' style='cursor:pointer' onclick='return deleteValue($id)'>delete</a>
          <a class='material-icons right show-form' title='edit' style='cursor:pointer' onclick='return getDormData($id)'>edit</a>
        </ul>
      </div></a>";
    }
  }else {
    echo '
    <div class="add-items">
      <h1>Your dormitory list is empty</h1>
      <button class="show-form">add new dormitory</button>
    </div>';
  }
}

# ADD DORM TO DB
if (isset($_POST['add-dorm'])) {
  header("Content-Type: application/json; charset=UTF-8");

  global $conn;

  $dorm = json_decode($_POST['add-dorm']);

  $name = clean_data(strtolower($dorm->name));
  $beds = clean_data(strtolower($dorm->beds));

  #Inserting the user's data into our database
  $sql = "INSERT INTO dormitories ( name, beds)
  VALUES ('$name', '$beds')";

  if ($conn->query($sql) === TRUE) {
    echo "dorm added successfully";
  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }

}

# GET DORM VALUES FROM DB TO JS
if (isset($_GET['get_dorm_data'])) {

  $id = $_GET['get_dorm_data'];

  $sql = "SELECT * FROM dormitories WHERE dormitory_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $id = $row['dormitory_id'];
    $name = $row['name'];
    $beds = $row['beds'];
  }

  $dorm = new stdClass;
  $dorm->id = $id;
  $dorm->name = $name;
  $dorm->beds = $beds;

  echo json_encode($dorm);

}


// EDIT DORM
if (isset($_POST['edit-dorm-update'])) {
  header("Content-Type: application/json; charset=UTF-8");

  global $conn;

  $dorm = json_decode($_POST['edit-dorm-update']);

  $id = clean_data(strtolower($dorm->id));
  $name = clean_data(strtolower($dorm->name));
  $beds = clean_data(strtolower($dorm->beds));

  #Inserting the user's data into our database
  $sql = "UPDATE dormitories SET `name` = '$name', `beds` = '$beds' WHERE dormitory_id = '$id'";

  if ($conn->query($sql) === TRUE) {
    echo "dorm edited successfully";
  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }

}

// DELETE DORM
if (isset($_GET['delete_dorm'])) {
  $id = $_GET['delete_dorm'];

  deleteUser('dormitories', 'dormitory_id', $id, 'dorm');
}











/*
--------------------------------------------
ADMIN FUNCTIONS
--------------------------------------------
*/
// GET ADMINISTRATORS
function getAdmins(){
  global $conn, $count;

  $sql = "SELECT * FROM admin WHERE role != 'admin' ORDER BY name ASC";
  $result = $conn->query($sql);

  # get fields into variables
  if($result->num_rows > 0){
    echo '
    <button class="button right show-form">add new admin</button>
    <br><br>
    <div class="parent formal">
      <ul><b>
        <span> # </span>
        <li>name</li>
        <li>username</li>
        <li>email</li>
        <li>phone</li>
        <li>role</li>
      </b>
      </ul>
    </div>
    ';
    while($row = $result->fetch_assoc()){
      $count += 1;
      $id = $row['admin_id'];
      $name = $row['name'];
      $username = $row['username'];
      $email = $row['email'];
      $phone_number = $row['phone_number'];
      $role = $row['role'];

      echo "
      <div class='col-sm-12 parent'>
        <ul>
          <span>$count</span>
          <li class='cap'>$name</li>
          <li>$username</li>
          <li>$email</li>
          <li>$phone_number</li>
          <li class='cap'>$role</li>
          <a class='material-icons right show-delete' title='delete' style='cursor:pointer' onclick='return deleteValue($id)'>delete</a>
        </ul>
      </div>";
    }
  }else {
    echo '
    <div class="add-items">
      <h1>Administrator account list empty</h1>
      <button class="show-form">add new account</button>
    </div>';
  }
}


# ADD ADMIN TO DB
if (isset($_POST['add-admin'])) {
  header("Content-Type: application/json; charset=UTF-8");

  global $conn;

  $admin = json_decode($_POST['add-admin']);

  $name = clean_data(strtolower($admin->name));
  $username = clean_data(strtolower($admin->username));
  $email = clean_data(strtolower($admin->email));
  $phone = clean_data(strtolower($admin->phone));
  $role = clean_data(strtolower($admin->role));

  #Inserting the user's data into our database
  $sql = "INSERT INTO admin ( name, username, email, phone_number, role)
  VALUES ('$name', '$username', '$email', '$phone', '$role')";

  if ($conn->query($sql) === TRUE) {
    echo "admin added successfully";
  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }
}

  // DELETE ADMIN
  if (isset($_GET['delete_admin'])) {
    $id = $_GET['delete_admin'];

    deleteUser('admin', 'admin_id', $id, 'admin');
  }


/*
--------------------------------------------
NOTICEBORAD FUNCTIONS
--------------------------------------------
*/
// GET NOTCIEBOARD
function getNoticeboard($admin=false){
  global $conn, $count;

  $value = null;

  $sql = "SELECT * FROM noticeboard ORDER BY date DESC";
  $result = $conn->query($sql);

  # get fields into variables
  if($result->num_rows > 0){
    echo '
    <button class="button right show-form" style="background-color:#555;">add new notice</button>
    <br><br>
    ';
    while($row = $result->fetch_assoc()){
      $count += 1;
      $id = $row['notice_id'];
      $notice = $row['notice'];
      $date = $row['date'];

      if (timeDifference($date) == 'month') {
        $sql = "DELETE FROM noticeboard WHERE notice_id = '$id'";
        if ($conn->query($sql) === TRUE) {

          echo "deletion successful";

        }else {
          echo "Error: " . $sql . "<br>" . $conn->error;
        }
      }

      if ($admin == true) {
        $value = "<a class='material-icons right show-delete' title='delete' style='cursor:pointer' onclick='return deleteValue($id)'>delete</a>
        <a class='material-icons right show-form' title='edit' style='cursor:pointer' onclick='return getNoticeData($id)'>edit</a>";
      }

      echo "
        <div class='notice col-sm-6'>
        <div class='tip col-sm-6'></div>
          <p>$notice</p>
          <span class='right'><b>posted ".otherTimeSpan($date)."</b></span><br><br>
          $value
        </div>
      ";
    }
  }else {
    echo '
    <div class="add-items">
      <h1 style="color:#fff;">Your noticeboard is empty</h1>
      <button class="show-form">add new notice</button>
    </div>';
  }
}

  # ADD NOTICE TO DB
  if (isset($_POST['add-notice'])) {
    header("Content-Type: application/json; charset=UTF-8");
    global $conn;

    $note = json_decode($_POST['add-notice']);

    $notice = clean_data(strtolower($note->notice));

    #Inserting the user's data into our database
    $sql = "INSERT INTO noticeboard (notice)
    VALUES ('$notice')";

    if ($conn->query($sql) === TRUE) {
      echo "note added successfully";
    }else {
      echo "Error: " . $sql . "<br>" . $conn->error;
    }

  }

  # GET NOTICE VALUES FROM DB TO JS
  if (isset($_GET['get_notice_data'])) {

    $id = $_GET['get_notice_data'];

    $sql = "SELECT * FROM noticeboard WHERE notice_id = '$id'";
    $result = $conn->query($sql);

    # get fields into variables
    while($row = $result->fetch_assoc()){
      $id = $row['notice_id'];
      $notice = $row['notice'];
    }

    $note = new stdClass;
    $note->id = $id;
    $note->notice = $notice;

    echo json_encode($note);

  }


  // EDIT NOTICE
  if (isset($_POST['edit-notice-update'])) {
    header("Content-Type: application/json; charset=UTF-8");
    global $conn;

    $note= json_decode($_POST['edit-notice-update']);

    $id = clean_data(strtolower($note->id));
    $notice = clean_data($note->notice);

    #Inserting the user's data into our database
    $sql = "UPDATE noticeboard SET `notice` = '$notice' WHERE notice_id = '$id'";

    if ($conn->query($sql) === TRUE) {
      echo "note edited successfully";
    }else {
      echo "Error: " . $sql . "<br>" . $conn->error;
    }

  }

  // DELETE NOTICE
  if (isset($_GET['delete_notice'])) {
    $id = $_GET['delete_notice'];

    deleteUser('noticeboard', 'notice_id', $id, 'note');
  }



/*
--------------------------------------------
SETTINGS
---------------------------------------------
*/
function getSchoolSettings(){
  global $conn;

  $sql = "SELECT * FROM school_info WHERE item != 'logo'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $id = $row['item_id'];
    $item = $row['item'];
    $description = $row['description'];

    echo "<tr>
    <td><span class='cap'>$item</span></td>
    <td><input type='text' name='$id' value='$description' placeholder='$item'></td>
    <tr>";
  }
}

# UPLOAD NEW IMAGE
function uploadNewImage(){

  global $conn;
  $errors = array();

  if(isset($_FILES['image'])){
    #Verify Image upload data
    $image_name = $_FILES['image']['name'];
    $image_size =$_FILES['image']['size'];
    $image_tmp =$_FILES['image']['tmp_name'];
    $image_type=$_FILES['image']['type'];
    $image_ext=strtolower(end((explode('.',$_FILES['image']['name']))));

    $extensions= array("jpeg","jpg","png");

    if ($image_name == '') {
      $errors[] = "<p class='error'>You have not uploaded an image</p>";
    }else {

      if(in_array($image_ext,$extensions)=== false){
         $errors[] = "<p class='error'>Please choose a JPEG or PNG file</p>";
      }else {
        if($image_size > 4194304){
           $errors[]='<p class="error">File is too large</p>';
        }
      }
    }

    if ($errors == []) {
      $sql = "UPDATE school_info SET `description` = '$image_name' WHERE item = 'logo'";
      move_uploaded_file($image_tmp,'../../../images/'.$image_name.'');
      if ($conn->query($sql) === TRUE) {
        echo "<p class='success' id='point'>image upload successful</p>";
      }else {
        echo "Error: " . $sql . "<br>" . $conn->error;
      }
    }else {
      foreach ($errors as $error) {
        echo $error;
      }
    }

    }
}

# UPDATE SCHOOL SETTINGS FUNCTIONS
function updateSchoolSettings(){
  global $conn;
  $success = null;

  $errors = array();

  if (isset($_POST['update-changes'])) {

    $sql = "SELECT * FROM school_info WHERE item != 'logo'";
    $result = $conn->query($sql);

    # get fields into variables
    while($row = $result->fetch_assoc()){
      $id = $row['item_id'];
      $description = $_POST[$id];

      if (isset($_POST[$id])) {
        $sql = "UPDATE school_info SET `description` = '$description' WHERE item_id = $id";

        if ($conn->query($sql) === TRUE) {
          $success = true;
        }else {
          $success = false;
        }
      }
    }

    if ($success === false) {
      echo "Error: " . $sql . "<br>" . $conn->error;
    }else if ($success === true) {
      echo "<p class= success>update successful</p>";
    }

  }
}


/*
--------------------------------------------
SUBJECT FUNCTIONS
--------------------------------------------
*/
// GET SUBJECTS
function getSubjects(){
  global $conn, $count;

  $sql = "SELECT * FROM subjects ORDER BY name ASC";
  $result = $conn->query($sql);

  # get fields into variables
  if($result->num_rows > 0){
    echo "
    <button class='button right show-form'>add new subject</button>
    <br><br>
    <div class='parent formal'>
      <ul><b>
        <span> # </span>
        <li>code</li>
        <li>subject</li>
      </b>
      </ul>
    </div>
    ";
    while($row = $result->fetch_assoc()){
      $count += 1;
      $id = $row['subject_id'];
      $code = $row['code'];
      $subject = $row['name'];

      echo "
      <div class='col-sm-12 parent'>
        <ul>
          <span>$count</span>
          <li class='cap'>$code</li>
          <li class='cap'>$subject</li>
          <a class='material-icons right show-delete' title='delete' style='cursor:pointer' onclick='return deleteValue($id)'>delete</a>
          <a class='material-icons right show-form' title='edit' style='cursor:pointer' onclick='return getSubjectData($id)'>edit</a>
        </ul>
      </div>";
    }
  }else {
    echo '
    <div class="add-items">
      <h1>Your subject list is empty</h1>
      <button class="show-form">add new subject</button>
    </div>';
  }
}

# ADD SUBJECT TO DB
if (isset($_POST['add-subject'])) {
  header("Content-Type: application/json; charset=UTF-8");

  global $conn;

  $subjects = json_decode($_POST['add-subject']);

  $code = clean_data(strtolower($subjects->code));
  $subject = clean_data(strtolower($subjects->subject));

  #Inserting the user's data into our database
  $sql = "INSERT INTO subjects ( code, name)
  VALUES ('$code', '$subject')";

  if ($conn->query($sql) === TRUE) {
    echo "subject added successfully";
  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }

}

# GET SUBJECT VALUES FROM DB TO JS
if (isset($_GET['get_subject_data'])) {

  $id = $_GET['get_subject_data'];

  $sql = "SELECT * FROM subjects WHERE subject_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $id = $row['subject_id'];
    $code = $row['code'];
    $subject = $row['name'];
  }

  $subjects = new stdClass;
  $subjects->id = $id;
  $subjects->code = $code;
  $subjects->subject = $subject;

  echo json_encode($subjects);

}


// EDIT SUBJECT
if (isset($_POST['edit-subject-update'])) {
  header("Content-Type: application/json; charset=UTF-8");

  global $conn;

  $subjects = json_decode($_POST['edit-subject-update']);

  $id = clean_data(strtolower($subjects->id));
  $code = clean_data(strtolower($subjects->code));
  $subject = clean_data(strtolower($subjects->subject));

  #Inserting the user's data into our database
  $sql = "UPDATE subjects SET `code` = '$code', `name` = '$subject' WHERE subject_id = '$id'";

  if ($conn->query($sql) === TRUE) {
    echo "subject edited successfully";
  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }

}

// DELETE SUBJECT
if (isset($_GET['delete_subject'])) {
  $id = $_GET['delete_subject'];

  deleteUser('subjects', 'subject_id', $id, 'subject');
}


 ?>
