<?php

require_once 'infused_cogs.php';

// GET TABLE COUNT
function getCount($table=''){

  global $conn;

  $sql = "SELECT * FROM $table";
  $result = $conn->query($sql);
  $count = $result->num_rows;

  echo $count;
}

function getTodayIncome(){
  global $conn;
  $total = null;

  $current_day = date('j');

  $sql = "SELECT * FROM student_paymentS WHERE DAY(date)='$current_day'";
  $result = $conn->query($sql);

  if($result->num_rows > 0){
    while($row = $result->fetch_assoc()) {
      $amount = $row['amount_paid'];
      $total += $amount;
    }
  }else {
    $total = 0;
  }


  return $total;
}

function getTodayExpenditure(){
  global $conn;
  $total = null;
  $overheads = null;

  $current_day = date('j');

  $sql = "SELECT * FROM petty_cash WHERE DAY(date)='$current_day'";
  $result = $conn->query($sql);
  if($result->num_rows > 0){
    while($row = $result->fetch_assoc()) {
      $amount = $row['amount'];
      $total += $amount;
    }
  }else {
    $total = 0;
  }


  $sql = "SELECT * FROM payroll WHERE received = 1 AND DAY(date)='$current_day'";
  $result = $conn->query($sql);

  if($result->num_rows > 0){
    while($row = $result->fetch_assoc()){
      $amount = $row['amount_payable'];
      $overheads += $amount;
    }
  }else {
    $overheads = 0;
  }

  $grandtotal = $overheads + $total;

  return $grandtotal;
}


 ?>
