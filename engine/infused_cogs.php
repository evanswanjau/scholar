<?php
session_start();

// call all files together
require_once 'connection.php';
require_once 'cogs.php';
require_once 'admin_cogs.php';
require_once 'profilecogs.php';
require_once 'financial_cogs.php';
require_once 'exam_cogs.php';
require_once 'manager_cogs.php';
require_once 'template_cogs.php';

?>
