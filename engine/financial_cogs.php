<?php

require_once 'infused_cogs.php';

/*
-------------------------------------
INVOICES CRUD FUNCTIONS
-------------------------------------
*/
# function to get invoice list
function getInvoiceItems($admin=true, $level = ''){
  global $conn;

  $count = 0;

  $total = null;

  $sql = "SELECT * FROM invoice ORDER BY item ASC";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $count += 1;
    $id = $row['invoice_id'];
    $item = $row['item'];
    $cost = $row['cost'];

    $total += $cost;

    if ($admin == true) {
      $value = "<a class='material-icons right show-delete' title='delete' style='cursor:pointer' onclick='return deleteValue($id)'>delete</a>
      <a class='material-icons right show-form' title='edit' style='cursor:pointer' onclick='return getItemData($id)'>edit</a>";
    }else {
      $value = "<a class='material-icons right show-form' title='add to invoice' style='cursor:pointer' onclick='return addToInvoice($id, $level)'>keyboard_backspace</a>";
    }


    echo "
    <div class='col-sm-12 parent'>
      <ul>
        <span>$count</span>
        <li class='cap' style='width:30%;'>$item</li>
        <li class='cap'>Ksh $cost</li>
        $value
      </ul>
    </div>";
  }

  echo "
  <div class='col-sm-12 parent' style='background-color:#000; color:#fff;'>
    <ul>
      <b>
      <span>#</span>
      <li style='width:30%;'>Total Fees</li>
      <li>Ksh ".$total."</li>
      </b>
    </ul>
  </div>";
}

# add item to invoice list
if (isset($_POST['add-item'])) {
  header("Content-Type: application/json; charset=UTF-8");

  global $conn;

  $items = json_decode($_POST['add-item']);

  $item = clean_data(strtolower($items->item));
  $price = clean_data(strtolower($items->price));

  #Inserting the user's data into our database
  $sql = "INSERT INTO invoice ( item, cost)
  VALUES ('$item', '$price')";

  if ($conn->query($sql) === TRUE) {
    echo "item added successfully";
  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }
}

# GET ITEM VALUES FROM DB TO JS
if (isset($_GET['get_item_data'])) {
  $id = $_GET['get_item_data'];
  $sql = "SELECT * FROM invoice WHERE invoice_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $id = $row['invoice_id'];
    $item = $row['item'];
    $price = $row['cost'];
  }

  $invoice = new stdClass;
  $invoice->id = $id;
  $invoice->item = $item;
  $invoice->price = $price;

  echo json_encode($invoice);
}

// EDIT ITEM
if (isset($_POST['edit-item-update'])) {
  header("Content-Type: application/json; charset=UTF-8");

  global $conn;

  $invoice = json_decode($_POST['edit-item-update']);

  $invoice_id = clean_data(strtolower($invoice->invoice_id));
  $item = clean_data(strtolower($invoice->item));
  $price = clean_data(strtolower($invoice->price));

  #Inserting the user's data into our database
  $sql = "UPDATE invoice SET `item` = '$item', `cost` = '$price' WHERE invoice_id = '$invoice_id'";

  if ($conn->query($sql) === TRUE) {
    echo "item edited successfully";
  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }

}

# DELETING AN ITEM IN AN INVOICE
// DELETE ITEM
if (isset($_GET['delete_item'])) {
  $id = $_GET['delete_item'];

  deleteUser('invoice', 'invoice_id', $id, 'item');
}





/*
--------------------------------------------
INVOICE FUNCTIONS
--------------------------------------------
*/
# get list of invoices
function getListOfInvoices(){
  global $conn, $count;

  $total = null;

  $sql = "SELECT * FROM levels";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $count += 1;
    $id = $row['level_id'];
    $name = $row['level_name'];

    echo "
    <a href='?level=$id'>
    <div class='col-sm-12 parent' style='cursor:pointer'>
      <ul>
        <span>$count</span>
        <li class='cap'>$name invoice</li>
      </ul>
    </div>
    </a>
    ";
  }
}

// get single invoice item cost
function getInvoiceItemCost($value){

  global $conn;

  $sql = "SELECT cost FROM invoice WHERE invoice_id = '$value'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $cost = $row['cost'];
  }

  return $cost;
}


// get single invoice item cost
function getSingleInvoiceItem($value, $count, $admin=true, $level_id=''){
  global $conn;

  $sql = "SELECT * FROM invoice WHERE invoice_id = '$value'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $id = $row['invoice_id'];
    $item = $row['item'];
    $cost = $row['cost'];

    if ($admin == true) {
      $newvalue = "<a class='material-icons right show-form' title='remove' style='cursor:pointer' onclick='return removeFromInvoice($id, $level_id)'>cancel</a>";
    }

    echo "
    <div class='col-sm-12 parent'>
      <ul>
        <span>$count</span>
        <li class='cap' style='width:30%;'>$item</li>
        <li class='cap'>Ksh $cost</li>
        $newvalue
      </ul>
    </div>
    ";
  }
}


// get level invoice
function getLevelInvoices($id, $admin=true){
  global $conn, $count;

  $total = null;

  $sql = "SELECT * FROM levels WHERE level_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $id = $row['level_id'];
    $values = $row['invoice_items'];

    if ($values != '') {
      $values = explode(',',$values);

      foreach ($values as $value) {
        $count += 1;
        getSingleInvoiceItem($value, $count, $admin=true, $level_id=$id);
      }
    }else {
      echo "<p style='color:grey'>No items in invoice</p>";
    }
  }
}

function getStudentInvoices($id, $admin=true){
  global $conn, $count;

  $total = null;

  $sql = "SELECT * FROM students WHERE student_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $id = $row['level_id'];
    $class_id = $row['class_id'];
    $values = $row['invoice_items'];

    if ($values != '') {
      $values = explode(',',$values);

      foreach ($values as $value) {
        $count += 1;
        getSingleInvoiceItem($value, $count, $admin=true, $level_id=$id);
      }
    }else {
      echo "<p style='color:grey'>No items in invoice</p>";
    }
  }
}

// get level invoice total
function getLevelInvoiceTotal($id){
  global $conn, $count;

  $total = null;

  $sql = "SELECT * FROM levels WHERE level_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $id = $row['level_id'];
    $values = $row['invoice_items'];

    if ($values != '') {
      $values = explode(',',$values);

      foreach ($values as $value) {
        $total += getInvoiceItemCost($value);
      }
    }
  }

  return $total;
}

// SPECIAL STUDENT CHECKBOX
function getSpecialCheckBox(){
  global $conn, $count;

  $sql = "SELECT * FROM invoice ORDER BY item ASC";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $count += 1;
    $id = $row['invoice_id'];
    $item = $row['item'];
    $cost = $row['cost'];


    echo "
    <div class='parent col-sm-12'>
      <ul>
        <li style='border-left:none;'>$item</li>
        <li>$cost</li>
        <li><input type='checkbox' name='$item' value='$id'></li>
      </ul>
    </div>";
  }
}

// UPDATE SPECIAL STUDENT'S INVOICE
function updateSpecialInvoice(){
  global $conn;

  $item_values = array();
  $errors = array();

  if (isset($_POST['update-invoice'])) {

    if ($_POST['adm'] == '') {
      $errors[] = "<p class='error'>admission number is required</p>";
    }else {
      $adm = clean_data(strtolower($_POST['adm']));
    }

    # full name
    if ($_POST['name'] == '') {
      $errors[] = "<p class='error'>full name is required</p>";
    }else {
      $name = clean_data(strtolower($_POST['name']));
    }

    $sql = "SELECT * FROM invoice ORDER BY item ASC";
    $result = $conn->query($sql);

    # get fields into variables
    while($row = $result->fetch_assoc()){
      $item = $row['item'];

      if (isset($_POST[$item])) {
        $item_values[] = $_POST[$item];
      }
    }

    $new_string = implode(',', $item_values);

    if ($errors == []) {
      $sql = "UPDATE students SET `invoice_items` = '$new_string' WHERE admission_number = '$adm'";

      if ($conn->query($sql) === TRUE) {
        echo "<p class='success'>Invoice updated successfully</p>";
      }else {
        echo "Error: " . $sql . "<br>" . $conn->error;
      }
    }else {
      foreach ($errors as $error) {
        echo $error;
      }
    }
  }

}


/*
-------------------------------------
ADD AND REMOVE INVOICE ITEMS
-------------------------------------
*/
// REMOVE ITEM FROM INVOICE LIST
if (isset($_GET['remove_from_list'])) {
  $id = $_GET['remove_from_list'];
  $level = $_GET['level'];

  global $conn;

  $new_string = array();

  $sql = "SELECT invoice_items FROM levels WHERE level_id = $level";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $invoice_items = $row['invoice_items'];
  }

  $values = explode(',',$invoice_items);

  for ($i=0; $i < count($values); $i++) {
    if ($values[$i] == $id) {
      array_splice($values,$i,1);
      break;
    }
  }

  $new_string = implode(',', $values);

  $sql = "UPDATE levels SET `invoice_items` = '$new_string' WHERE level_id = $level";

  if ($conn->query($sql) === TRUE) {
    updateStudentInvoiceList($level, $new_string);
  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }

}

// ADD ITEM TO LIST
if (isset($_GET['add_to_list'])) {
  $id = $_GET['add_to_list'];
  $level = $_GET['level'];

  global $conn;

  $new_string = array();
  $rest = false;

  $sql = "SELECT invoice_items FROM levels WHERE level_id = $level";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $invoice_items = $row['invoice_items'];
  }

  if ($invoice_items != '') {
    $values = explode(',',$invoice_items);

    foreach ($values as $value) {
      if ($value != $id) {
        $rest = true;
      }else {
        echo "item already exists";
        $rest = false;
        break;
      }
    }

    if ($rest == true) {
      $values[] = $id;
    }

    $new_string = implode(',', $values);
  }else {
    $new_string = $id;
  }


  $sql = "UPDATE levels SET `invoice_items` = '$new_string' WHERE level_id = $level";

  if ($conn->query($sql) === TRUE) {
    updateStudentInvoiceList($level, $new_string);
  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }

}


// UPDATE STUDENT WITH LEVEL INVOICES
function updateStudentInvoiceList($level, $new_string){
  global $conn;

  $sql = "SELECT class_id FROM classes WHERE level = $level";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $class_id = $row['class_id'];

    $sql = "UPDATE students SET `invoice_items` = '$new_string' WHERE class_id = $class_id";

    if ($conn->query($sql) === TRUE) {
    }else {
      echo "Error: " . $sql . "<br>" . $conn->error;
    }
  }
}

/*
--------------------------------------------
STUDENT INVOICE GENERATION
--------------------------------------------
*/
function getStudentInvoice($id){

  global $conn, $count;

  $total = null;
  $totalpayment = null;

  $sql = "SELECT * FROM students WHERE student_id = $id";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $admission_number = $row['admission_number'];
    $name = $row['name'];
    $class = getClass($row['class_id']);
    $accountno = $row['accountno'];
    $invoice_items = $row['invoice_items'];
  }

  $values = explode(',', $invoice_items);



  echo "
    <h5 class='cap formal'><b>Name: </b>$name</h5>
    <h5 class='cap formal'><b>Adm Number: </b>$admission_number</h5>
    <h5 class='cap formal'><b>Class: </b>$class</h5>
    <p class='boundary'></p>
  ";

  echo "<table border='1'>
    <tr>
      <th>#</th>
      <th>item</th>
      <th>cost (Ksh)</th>
    </tr>
  ";

  foreach ($values as $value) {
    $sql = "SELECT * FROM invoice WHERE invoice_id = '$value'";
    $result = $conn->query($sql);

    # get fields into variables
    while($row = $result->fetch_assoc()){
      $count += 1;
      $id = $row['invoice_id'];
      $item = $row['item'];
      $cost = $row['cost'];

      $total += $cost;

      echo "<tr>
        <td>$count</td>
        <td>$item</td>
        <td>$cost</td>
      </tr>";
    }
  }

  echo "<tr>
    <td>#</td>
    <td><b>Grand Total</b></td>
    <td><b>$total</b></td>
  </tr>";

  $sql = "SELECT * FROM student_payments WHERE accountno = '$accountno'";
  $result = $conn->query($sql);

  # get fields into variables
  if($result->num_rows > 0){
    echo "<tr>
      <td><b>Payment Installments</b></td>
      <td><b>Reference no</b></td>
      <td><b>amount</b></td>
    </tr>";
    $count = 0;
    while($row = $result->fetch_assoc()){
      $count += 1;
      $ref = $row['ref'];
      $amount_paid = $row['amount_paid'];

      $totalpayment += $amount_paid;

      echo "
      <tr style='background-color:#82ffac;'>
        <td>Payment Installment $count</td>
        <td>".strtoupper($ref)."</td>
        <td>$amount_paid</td>
      </tr>";
    }

    $balance = $total - $totalpayment;

    echo "<tr>
      <td>#</td>
      <td><b>Total Payment</b></td>
      <td><b>$totalpayment</b></td>
    </tr>
    <tr>
      <td>#</td>
      <td><b>Balance</b></td>
      <td><b>$balance</b></td>
    </tr>
    ";
  }else {
    echo "
    <tr style='background-color:#f79797;'>
      <td>*</td>
      <td>******* No payments have been made *******</td>
      <td>*</td>
    </tr>";
  }

  echo "</table>";


}












/*
--------------------------------------------
PAYMENT FUNCTIONS
--------------------------------------------
*/

# function to get all payments
function getAllPayments(){
  global $conn, $count;

  $sql = "SELECT * FROM student_payments ORDER BY date DESC";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $count += 1;
    $payment_id = $row['payment_id'];
    $accountno = $row['accountno'];
    $ref = $row['ref'];
    $purpose = $row['purpose'];
    $amount_paid = $row['amount_paid'];
    $date = date_format(new dateTime($row['date']), "jS M Y H:i:s");

    echo "
    <div class='col-sm-12 parent'>
      <ul>
        <span>$count</span>
        <li class='cap' style='width:10%;'>$accountno</li>
        <li style='width:10%;'>".strtoupper($ref)."</li>
        <li class='cap'>".getStudentByAccount($accountno)."</li>
        <li class='cap'>$purpose</li>
        <li class='cap'>Ksh $amount_paid</li>
        <li class='cap' style='font-size:12px;'><b>$date</b></li>
        <a class='material-icons right show-rform' title='view receipt' style='cursor:pointer' onclick='return genReceipt($payment_id)'>receipt</a>
      </ul>
    </div>
    ";
  }
}

// PURPOSE DROP DOWN
function purposeDropdown(){

  global $conn;

  $value = null;

  $sql = "SELECT item FROM invoice ORDER BY item ASC";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $item = $row['item'];
    echo "<option value='$item'>$item</option>";
  }
}


# ADD PAYMENT TO DB
if (isset($_POST['make-payment'])) {
  header("Content-Type: application/json; charset=UTF-8");

  global $conn;

  $payment = json_decode($_POST['make-payment']);

  $account = clean_data($payment->account);
  $adm = clean_data(strtolower($payment->adm));
  $name = clean_data($payment->name);
  $amount = clean_data(strtolower($payment->amount));
  $purpose = clean_data(strtolower($payment->purpose));
  $method = clean_data(strtolower($payment->method));

  $ref = stringShuffle($divide = 4);

  #Inserting the user's data into our database
  $sql = "INSERT INTO student_payments ( accountno , ref, purpose, amount_paid, method)
  VALUES ('$account', '$ref', '$purpose', '$amount', '$method')";

  if ($conn->query($sql) === TRUE) {
    updateSchoolBalance($amount, $operation='add');
    echo "payment made successfully";
  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }

}

# GET PAYMENT VALUES FROM DB TO JS
if (isset($_GET['get_payment_data'])) {

  $id = $_GET['get_payment_data'];

  $sql = "SELECT * FROM student_payments WHERE payment_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $id = $row['payment_id'];
    $accountno = $row['accountno'];
    $ref = $row['ref'];
    $purpose = $row['purpose'];
    $amount_paid = $row['amount_paid'];
    $method = $row['method'];
    $date = date_format(new dateTime($row['date']), "jS M Y H:i:s");
  }

  $payment = new stdClass;
  $payment->id = $id;
  $payment->accountno = $accountno;
  $payment->name = getStudentByAccount($accountno);
  $payment->ref = $ref;
  $payment->purpose = $purpose;
  $payment->amount_paid = $amount_paid;
  $payment->method = $method;
  $payment->date = $date;

  echo json_encode($payment);
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// FUNCTION TO GET CURRENT SCHOOL BANK BALANCE
function getSchoolBalance(){
  global $conn;

  $sql = "SELECT balance FROM school_account";
  $result = $conn->query($sql);

  while($row = $result->fetch_assoc()){
    $balance = $row['balance'];
  }

  return $balance;
}


// FUNCTION TO UPDATE SCHOOL BALANCE
function updateSchoolBalance($value, $operation=''){

  global $conn;

  $new_balance = null;

  if ($operation == 'add') {
    $new_balance = getSchoolBalance() + $value;
  }elseif ($operation == 'subtract') {
    $new_balance = getSchoolBalance() - $value;
  }

  $sql = "UPDATE school_account SET `balance` = '$new_balance'";

  if ($conn->query($sql) === TRUE) {
  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }
}

/*
--------------------------------------------
PETTY CASH
--------------------------------------------
*/
// FUNCTION TO GET PETTY CASH
function getPettyCash(){

  global $conn, $count;

  $sql = "SELECT * FROM petty_cash ORDER BY date DESC";
  $result = $conn->query($sql);

  while($row = $result->fetch_assoc()){
    $count += 1;
    $id = $row['petty_id'];
    $amount = $row['amount'];
    $given_to = $row['given_to'];
    $reason = $row['reason'];
    $date = date_format(new dateTime($row['date']), "jS M Y H:i:s");

    echo "
    <div class='col-sm-12 parent'>
      <ul>
        <span>$count</span>
        <li>Ksh $amount</li>
        <li class='cap'>$given_to</li>
        <li>$reason</li>
        <li class='cap'><b>$date</b></li>
      </ul>
    </div>
    ";
  }
}


// ISSUE PETTY CASH PAYMENT
if (isset($_POST['issue-cash'])) {
  header("Content-Type: application/json; charset=UTF-8");

  global $conn;

  $pettycash = json_decode($_POST['issue-cash']);

  $amount = clean_data($pettycash->amount);
  $given = clean_data(strtolower($pettycash->given));
  $reason = clean_data($pettycash->reason);

  if ($amount > getSchoolBalance()) {
    echo 'error';
  }else {

    #Inserting the user's data into our database
    $sql = "INSERT INTO petty_cash ( amount , given_to, reason)
    VALUES ('$amount', '$given', '$reason')";

    if ($conn->query($sql) === TRUE) {
      updateSchoolBalance($amount, $operation='subtract');
      echo "cash issued successfully";
    }else {
      echo "Error: " . $sql . "<br>" . $conn->error;
    }
  }
}


/*
--------------------------------------------
PAYROLL FUNCTIONS
--------------------------------------------
*/
function getPayroll(){

  global $conn, $count;

  $sql = "SELECT * FROM payroll";
  $result = $conn->query($sql);

  while($row = $result->fetch_assoc()){
    $count += 1;
    $id = $row['overhead_id'];
    $overhead = $row['overhead'];
    $amount = $row['amount'];
    $advance = $row['advance'];
    $loan = $row['loan'];
    $nhif = $row['nhif'];
    $nssf = $row['nssf'];
    $tax = $row['tax'];
    $total_deductions = $row['total_deductions'];
    $benefits = $row['benefits'];
    $welfare = $row['welfare'];
    $total_additions = $row['total_additions'];
    $amount_payable = $row['amount_payable'];
    $balance = $row['balance'];
    $received = $row['received'];
    $date = $row['date'];

    $total_deductions = $advance + $loan + $nhif + $nssf + $tax;
    $total_additions = $benefits + $welfare;
    $amount_payable = $amount - $total_deductions + $total_additions;

    $sql = "UPDATE payroll SET `total_deductions` = '$total_deductions', `total_additions` = '$total_additions', `amount_payable` = '$amount_payable'";

    if ($conn->query($sql) === TRUE) {
    }else {
      echo "Error: " . $sql . "<br>" . $conn->error;
    }

    if ($received == 1) {
      $value = 'disabled  checked';
    }else {
      $value = null;
    }


    echo "
    <tr>
      <td style='width:2%;'>$count</td>
      <td class='cap'>$overhead</td>
      <td>$amount</td>
      <td>$advance</td>
      <td>$loan</td>
      <td>$nhif</td>
      <td>$nssf</td>
      <td>$tax</td>
      <td>$total_deductions</td>
      <td>$benefits</td>
      <td>$total_additions</td>
      <td>$amount_payable</td>
      <td>$balance</td>
      <td><input type='checkbox' name='received' $value></td>
      <td>$date</td>
      <td><a class='material-icons right show-delete' title='delete' style='cursor:pointer' onclick='return getPayroll($id)'>edit</a></td>
      <td><a class='material-icons right show-form' title='edit' style='cursor:pointer' onclick='return deleteValue($id)'>delete</a></td>
    </tr>
    ";
  }
}

/*
--------------------------------------------
FINANCIAL REPORT FUNCTIONS
--------------------------------------------
*/

function getFinancialReport(){
  global $conn, $count;

  $grandtotal = null;
  $pettycash = null;
  $overheads = null;

  $items_array = array();
  $amount_array = array();
  $current_month = date('M');

  $sql = "SELECT * FROM invoice";
  $result = $conn->query($sql);

  while($row = $result->fetch_assoc()){
    $items_array[] = $row['item'];
  }
  echo "
    <div class='report col-sm-12'>
    <div class='col-sm-12'>
    <h3>$current_month Financial Report</h3>
    <div class='col-sm-6'>
    <h5 style='background-color:#82ffac;'><b>Total Income</b></h5>
    <table border='1'>
    <tr>
      <th>Item</th>
      <th>Price (Ksh)</th>
    </tr>";

  foreach ($items_array as $item) {

    $total = null;

    $sql = "SELECT * FROM student_payments WHERE purpose = '$item' ORDER BY amount_paid DESC";
    $result = $conn->query($sql);

    if($result->num_rows > 0){
      while($row = $result->fetch_assoc()){
        $amount_paid = $row['amount_paid'];
        $total += $amount_paid;
      }
    }else {
      $total = 0;
    }

    $grandtotal += $total;

    echo "
    <tr>
      <td class='cap'>$item</td>
      <td>$total</td>
    </tr>";

  }

  $sql = "SELECT * FROM petty_cash";
  $result = $conn->query($sql);

  while($row = $result->fetch_assoc()){
    $amount = $row['amount'];
    $pettycash += $amount;
  }

  $sql = "SELECT * FROM payroll WHERE received = 1";
  $result = $conn->query($sql);

  while($row = $result->fetch_assoc()){
    $amount = $row['amount_payable'];
    $overheads += $amount;
  }

  $expenditure = $overheads + $pettycash;
  $month_margin = $grandtotal - $expenditure;

  if ($month_margin < 0) {
    $style = "loss";
  }else {
    $style = "profit";
  }

  echo "
  <tr>
    <td><b>Total Income</b></td>
    <td><b>$grandtotal</b></td>
  </tr>
  </table>
  </div>
  <div class='col-sm-6'>
  <h5 style='background-color:#f79797'><b>Total Expenditure</b></h5>
  <table border='1'>
  <tr>
    <th>Item</th>
    <th>Price (Ksh)</th>
  </tr>
  <tr>
    <td>Overheads</td>
    <td>$overheads</td>
  </tr>
  <tr>
    <td>Petty Cash</td>
    <td>$pettycash</td>
  </tr>
  <tr>
    <td><b>Total Expenditure</b></td>
    <td><b>$expenditure</b></td>
  </tr>
  </table>
  <p>&nbsp</p>
  </div>
  </div>
  <div class='col-sm-6 final'>
    <h4>$current_month's Profit / Loss Margin: <span class='$style'><b>Ksh $month_margin</b></span><h4>
  </div>
  </div>
  ";
}

/*
--------------------------------------------
BUDGET FUNCTIONS
--------------------------------------------
*/
function getBudget(){
  global $conn;
  $grandtotal = null;
  $current_year = date('Y');

  for ($i=1; $i <= 3; $i++) {
    $total = null;

    $sql = "SELECT * FROM budget WHERE term = $i AND year ='$current_year' ORDER BY item ASC";
    $result = $conn->query($sql);

    if($result->num_rows > 0){
      $count = 0;
      echo "
      <div class='border'>
      <h4 style='text-align:center;'>Term $i Budget</h4>
      <div class='parent formal col-sm-12'><b>
        <ul>
          <span>#</span>
          <li>item</li>
          <li>price (Ksh)</li>
        </ul></b>
      </div>
      ";

      while($row = $result->fetch_assoc()){
        $count += 1;
        $id = $row['item_id'];
        $item = $row['item'];
        $price = $row['price'];
        $term = $row['term'];
        $year = $row['year'];

        if ($i >= getCurrentTerm()) {
          $value = "<a class='material-icons right show-delete' title='delete' style='cursor:pointer' onclick='return deleteValue($id)'>delete</a>
          <a class='material-icons right show-form' title='edit' style='cursor:pointer' onclick='return getBudgetData($id)'>edit</a>";
        }else {
          $value = null;
        }

        $total += $price;

        echo "
        <div class='col-sm-12 parent'>
          <ul>
            <span>$count</span>
            <li class='cap'>$item</li>
            <li class='cap'>$price</li>
            $value
          </ul>
        </div>
        ";
      }

      $grandtotal += $total;

      echo "
      <div class='col-sm-12 parent' style='background-color:#000; color:#fff;border:none;'>
        <ul>
          <b>
          <span>#</span>
          <li style='width:20%;'>Term $i Budget</li>
          <li>Ksh ".$total."</li>
          </b>
        </ul>
      </div>
      <p>&nbsp</p>
      </div>";
    }
  }

  echo "
  <div class='border'>
  <h4 style='text-align:center;'>$current_year Budget Total</h4>
  <div class='col-sm-12 parent' style='background-color:#000; color:#fff;border:none;'>
    <ul>
      <b>
      <span>#</span>
      <li style='width:20%;'>$current_year Budget Total</li>
      <li>Ksh ".$grandtotal."</li>
      </b>

    </ul>
  </div>
  <p>&nbsp</p>
  </div>";
}

# ADD PAYMENT TO DB
if (isset($_POST['add-budget'])) {
  header("Content-Type: application/json; charset=UTF-8");

  global $conn;

  $budgetItem = json_decode($_POST['add-budget']);

  $item = clean_data($budgetItem->item);
  $price = clean_data(strtolower($budgetItem->price));
  $term = clean_data($budgetItem->term);

  $current_year = date('Y');

  #Inserting the user's data into our database
  $sql = "INSERT INTO budget ( item , price, term, year)
  VALUES ('$item', '$price', '$term', '$current_year')";

  if ($conn->query($sql) === TRUE) {
    echo "item added successfully";
  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }

}

# GET PAYMENT VALUES FROM DB TO JS
if (isset($_GET['get_budget_data'])) {

  $id = $_GET['get_budget_data'];

  $sql = "SELECT * FROM budget WHERE item_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $id = $row['item_id'];
    $item = $row['item'];
    $price = $row['price'];
    $term = $row['term'];
    $year = $row['year'];
  }

  $budgetItem = new stdClass;
  $budgetItem->id = $id;
  $budgetItem->item = $item;
  $budgetItem->price = $price;
  $budgetItem->term = $term;
  $budgetItem->year = $year;

  echo json_encode($budgetItem);
}

# EDIT BUDGET ITEM
if (isset($_POST['edit-budget-update'])) {
  header("Content-Type: application/json; charset=UTF-8");

  global $conn;

  $budgetItem = json_decode($_POST['edit-budget-update']);

  $item_id = clean_data(strtolower($budgetItem->id));
  $item = clean_data(strtolower($budgetItem->item));
  $price = clean_data($budgetItem->price);
  $term = clean_data($budgetItem->term);

  #Inserting the user's data into our database
  $sql = "UPDATE budget SET `item` = '$item', `price` = '$price',  `term`='$term' WHERE item_id = '$item_id'";

  if ($conn->query($sql) === TRUE) {
    echo "item edited successfully";
  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }
}

# DELETE BUDGET
if (isset($_GET['delete_budget'])) {
  $id = $_GET['delete_budget'];

  deleteUser('budget', 'item_id', $id, 'item');
}


 ?>
