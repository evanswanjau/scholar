<?php

require_once 'infused_cogs.php'; // The Link


// LOGIN USER
function loginUser(){
  if (isset($_POST['login'])) {
    global $conn;

    $errors = array(); #initialize array to store our errors

    #username
    if($_POST['username'] != ''){
      $username = clean_data($_POST['username']);
    }else {
      $errors[] = "<p class='error'>username is required</p>";
    }

    #password
    if($_POST['password'] != ''){
      $password = (clean_data($_POST['password']));
    }else {
      $errors[] = "<p class='error'>password is required</p>";
    }

    if($errors == []){

      // LOGIN STUDENT
      $query = "SELECT * FROM students WHERE admission_number = '$username'";
      $result = $conn->query($query);

      if($result->num_rows == 1){
        while($row = $result->fetch_assoc()) {
              $hash = $row['password'];
            }

            if ($hash == '') {
              $_SESSION['student'] = $username;
              $_SESSION['user'] = $username;
              header('Location: signup');
            }else {
              if (password_verify($password, $hash)) {

                setcookie('students', $username, time() + 43200, "/");

                header('Location: accounts/student');

              } else {
                echo '<p class="error">Invalid login details</p>';
              }
            }

      }else if($result->num_rows < 1){

        // LOGIN PARENT
        $query = "SELECT * FROM parents WHERE email = '$username' OR phone = '$username'";
        $result = $conn->query($query);

        if($result->num_rows == 1){
          while($row = $result->fetch_assoc()) {
                $phone = $row['phone'];
                $hash = $row['password'];
              }

              if ($hash == '') {
                $_SESSION['parent'] = $phone;
                $_SESSION['user'] = $username;
                header('Location: signup');
              }else {
                if (password_verify($password, $hash)) {

                  setcookie('parents', $phone, time() + 43200, "/");

                  header('Location: accounts/parent');

                } else {
                  echo '<p class="error">Invalid login details</p>';
                }
              }

        }else if($result->num_rows < 1){

          // LOGIN TEACHER
          $query = "SELECT * FROM teachers WHERE email = '$username' OR phone = '$username'";
          $result = $conn->query($query);

          if($result->num_rows == 1){
            while($row = $result->fetch_assoc()) {
                  $hash = $row['password'];
                  $phone = $row['phone'];
                }

                if ($hash == '') {
                  $_SESSION['teacher'] = $phone;
                  $_SESSION['user'] = $username;
                  header('Location: signup');
                }else {
                  if (password_verify($password, $hash)) {

                    setcookie('teachers', $phone, time() + 43200, "/");

                    header('Location: accounts/teacher');

                  } else {
                    echo '<p class="error">Invalid login details</p>';
                  }
                }

          }else if($result->num_rows < 1){
            // LOGIN ADMIN
            $query = "SELECT * FROM admin WHERE username = '$username'";
            $result = $conn->query($query);

            if($result->num_rows == 1){
              while($row = $result->fetch_assoc()) {
                    $role = $row['role'];
                    $hash = $row['password'];
                  }

                  if ($hash == '') {

                    if ($role == 'admin') {
                      $_SESSION['admin'] = $username;
                      $_SESSION['user'] = $username;
                    }elseif ($role == 'accountant') {
                      $_SESSION['accountant'] = $username;
                      $_SESSION['user'] = $username;
                    }elseif ($role == 'librarian') {
                      $_SESSION['librarian'] = $username;
                      $_SESSION['user'] = $username;
                    }elseif ($role == 'manager') {
                      $_SESSION['manager'] = $username;
                      $_SESSION['user'] = $username;
                    }else {
                      $_SESSION['student'] = null;
                    }

                    header('Location: signup');

                  }else {
                    if (password_verify($password, $hash)) {

                      if ($role == 'admin') {
                        setcookie('admin', $username, time() + 43200, "/");
                        header('Location: accounts/admin');
                      }elseif ($role == 'accountant') {
                        setcookie('accountant', $username, time() + 43200, "/");
                        header('Location: accounts/admin/accounts/accountant');
                      }elseif ($role == 'librarian') {
                        setcookie('librarian', $username, time() + 43200, "/");
                        header('Location: accounts/admin/accounts/librarian');
                      }elseif ($role == 'manager') {
                        setcookie('manager', $username, time() + 43200, "/");
                        header('Location: accounts/admin/accounts/manager');
                      }else {
                        echo "<p class='error'>no define role</p>";
                      }

                    } else {
                      echo '<p class="error">Invalid login details</p>';
                    }
                  }
            }else if($result->num_rows < 1){
              echo '<p class="error">That user does not exist</p>';
            }
          }
        }
      }
    }else{
      foreach ($errors as $error) {
        echo $error;
      }
    }
  }
}


/*
--------------------------------------------
UPDATE FUNCTION(S)
--------------------------------------------
*/
# update function
function UpdateDB($table = '', $clausevalue='', $value, $clause, $variable){

  global $conn;

  $success = null;

  $sql = "UPDATE $table SET $clausevalue = '$value' WHERE $clause = '$variable'";

  if ($conn->query($sql) == TRUE) {
    $success = TRUE;
  }else{
    $success = FALSE;
  }

  return $success;
}

// UPDATE USER DATA
function updateUserData($account = '', $clause, $id){

  $errors = array();

  if (isset($_POST['update-profile'])) {

    // FULL NAME
    if ($_POST['name'] == '') {
      $errors[] = "<p class='error'>Your name can't be empty</p>";
    }else {
      $name = clean_data($_POST['name']);
      UpdateDB($table = $account, $clausevalue='name', $name, $clause, $id);
    }

    // EMAIL
    if (isset($_POST['email'])) {
      $email = clean_data($_POST['email']);
      UpdateDB($table = $account, $clausevalue='email', $email, $clause, $id);
    }

    // PHONE
    if (isset($_POST['phone_number'])) {
      $phone = clean_data($_POST['phone_number']);
      UpdateDB($table = $account, $clausevalue='phone', $phone, $clause, $id);
    }

    // ADDRESS
    if (isset($_POST['address'])) {
      $address = clean_data($_POST['address']);
      UpdateDB($table = $account, $clausevalue='address', $address, $clause, $id);
    }

    // PROFESSION
    if (isset($_POST['profession'])) {
      $profession = clean_data($_POST['profession']);
      UpdateDB($table = $account, $clausevalue='profession', $profession, $clause, $id);
    }

    // USERNAME
    if (isset($_POST['username'])) {
      $profession = clean_data($_POST['username']);
      UpdateDB($table = $account, $clausevalue='username', $profession, $clause, $id);
    }



    // PARENT IMPORTANCE
    if ($account == 'parents') {

      // FULL NAME
      if ($_POST['phone_number'] == '') {
        $errors[] = "<p class='error'>Phone number can't be empty</p>";
      }else {
        $phone = clean_data($_POST['phone_number']);
        UpdateDB($table = $account, $clausevalue='phone', $phone, $clause, $id);
      }
    }


    // ADMIN IMPORTANCE
    if ($account == 'admin') {

      // FULL NAME
      if ($_POST['username'] == '') {
        $errors[] = "<p class='error'>username can't be empty</p>";
      }else {
        $username = clean_data($_POST['username']);
        UpdateDB($table = $account, $clausevalue='username', $phone, $clause, $id);
      }
    }


    if ($errors == []) {
      if ($success = True) {
        echo "<p class='error'>Update Successful</p>";
        //header('refresh:2; url=../myaccount');
      }
    }else {
      foreach ($errors as $error) {
        echo $error;
      }
    }
  }

}


# function to change password
function changePassword($dbpassword, $id, $clause='', $clausevalue=''){

  global $conn;

  # if change password is set
  if (isset($_POST['change-password'])){

    $errors = array();

    # password
    if ($_POST['current'] == '') {
      $errors[] = "<p class='error'>Your need to input your current password</p>";
    }else {
      $oldpassword = $_POST['current'];

      if (password_verify($oldpassword, $dbpassword)) {
        # new username
        if ($_POST['password'] == '') {
          $errors[] = "<p class='error'>Your need to input your new password</p>";
        }else {
          $newpassword = clean_data($_POST['password']);
          if (strlen($newpassword) < 6) {
            $errors[] = "<p class='error'>Password cannot be less than 6 characters</p>";
          }else {
            # confirm password
            if ($_POST['confirm'] == '') {
              $errors[] = "<p class='error'>Your need confirm your password</p>";
            }else {
              $confirmpassword = clean_data($_POST['confirm']);

              # confirm passwords are similar
              if ($newpassword == $confirmpassword) {
                $password = password_hash($newpassword, PASSWORD_BCRYPT);

                $sql = "UPDATE $clause SET password = '$password' WHERE $clausevalue = $id";

                if ($conn->query($sql) === TRUE){
                  echo "<p class='success'>password changed successfully</p>";
                  header('refresh:2; url=../myaccount');
                }else {
                  echo "Error: " . $sql . "<br>" . $conn->error;
                }
              }
            }
          }
        }
      }else {
        $errors[] = "<p class='error'>Password is not similar to old password</p>";
      }
    }

  foreach ($errors as $error) {
    echo $error;
  }
}
}


// teacher security function
function teacherSecurity($link=''){
  if (!isset($_COOKIE['teachers'])) {
    header('location: '.$link.'');
  }
}

// parent security function
function parentSecurity($link=''){
  if (!isset($_COOKIE['parents'])) {
    header('location: '.$link.'');
  }
}

// parent security function
function studentSecurity($link=''){
  if (!isset($_COOKIE['students'])) {
    header('location: '.$link.'');
  }
}

// librarian security function
function librarianSecurity($link=''){
  if (!isset($_COOKIE['librarian'])) {
    header('location: '.$link.'');
  }
}

// admin security function
function adminSecurity($link=''){
  if (!isset($_COOKIE['admin'])) {
    header('location: '.$link.'');
  }
}

// accountant security function
function accountantSecurity($link=''){
  if (!isset($_COOKIE['accountant'])) {
    header('location: '.$link.'');
  }
}

// manager security function
function managerSecurity($link=''){
  if (!isset($_COOKIE['manager'])) {
    header('location: '.$link.'');
  }
}



// unctio to logout user
function logOut($link=''){
  if (isset($_GET['logout'])) {
    setcookie('teachers', "", time() - 43200, "/");
    setcookie('parents', "", time() - 43200, "/");
    setcookie('students', "", time() - 43200, "/");
    setcookie('librarian', "", time() - 43200, "/");
    setcookie('admin', "", time() - 43200, "/");
    setcookie('accountant', "", time() - 43200, "/");
    setcookie('manager', "", time() - 43200, "/");
    header('location: '.$link.'');
  }
}


/*
---------------------------------------------
SIGN UP FUNCTION
---------------------------------------------
*/
function signUp(){

  if (isset($_POST['signup'])) {
    global $conn;

    $errors = array(); #initialize array to store our errors

    #username
    if($_POST['username'] != ''){
      $username = clean_data($_POST['username']);
    }else {
      $errors[] = "<p class='error'>username is required</p>";
    }

    # assign username to session user
    if (isset($_SESSION['user'])) {
      if ($_SESSION['user'] == $username) {
        $username = $_SESSION['user'];
      }
    }


    if ($_POST['password'] == '') {
      $errors[] = "<p class='error'>Your need to input your new password</p>";
    }else {
      $newpassword = clean_data($_POST['password']);
      if (strlen($newpassword) < 6) {
        $errors[] = "<p class='error'>Password cannot be less than 6 characters</p>";
      }else {
        # confirm password
        if ($_POST['confirm'] == '') {
          $errors[] = "<p class='error'>Your need confirm your password</p>";
        }else {
          $confirmpassword = clean_data($_POST['confirm']);

          # confirm passwords are similar
          if ($newpassword == $confirmpassword) {
            $password = password_hash($newpassword, PASSWORD_BCRYPT);

            if (isset($_SESSION['student'])) {
              $sql = "UPDATE students SET password = '$password' WHERE admission_number = '$username'";

              if ($conn->query($sql) === TRUE){
                setcookie('students', $username, time() + 43200, "/");
                echo "<p class='success'>password created successfully</p>";
                header('refresh:2; url=../accounts/student');
              }else {
                echo "Error: " . $sql . "<br>" . $conn->error;
              }
            }elseif (isset($_SESSION['teacher'])) {
              $phone = $_SESSION['teacher'];

              $sql = "UPDATE teachers SET password = '$password' WHERE email = '$username' OR phone = '$username'";

              if ($conn->query($sql) === TRUE){
                setcookie('teachers', $phone, time() + 43200, "/");
                echo "<p class='success'>password created successfully</p>";
                header('refresh:2; url=../accounts/teacher');
              }else {
                echo "Error: " . $sql . "<br>" . $conn->error;
              }
            }elseif (isset($_SESSION['parent'])) {
              $phone = $_SESSION['parent'];
              $sql = "UPDATE parents SET password = '$password' WHERE email = '$username' OR phone = '$username'";

              if ($conn->query($sql) === TRUE){
                setcookie('parents', $phone, time() + 43200, "/");
                echo "<p class='success'>password created successfully</p>";
                header('refresh:2; url=../accounts/parent');
              }else {
                echo "Error: " . $sql . "<br>" . $conn->error;
              }
            }elseif (isset($_SESSION['admin'])) {
              $sql = "UPDATE admin SET password = '$password' WHERE username = '$username'";

              if ($conn->query($sql) === TRUE){
                setcookie('admin', $username, time() + 43200, "/");
                echo "<p class='success'>password created successfully</p>";
                header('refresh:2; url=../accounts/admin');
              }else {
                echo "Error: " . $sql . "<br>" . $conn->error;
              }
            }elseif (isset($_SESSION['accountant'])) {
              $sql = "UPDATE admin SET password = '$password' WHERE username = '$username'";

              if ($conn->query($sql) === TRUE){
                setcookie('accountant', $username, time() + 43200, "/");
                echo "<p class='success'>password created successfully</p>";
                header('refresh:2; url=../accounts/admin/accounts/accountant');
              }else {
                echo "Error: " . $sql . "<br>" . $conn->error;
              }
            }elseif (isset($_SESSION['manager'])) {
              $sql = "UPDATE admin SET password = '$password' WHERE username = '$username'";

              if ($conn->query($sql) === TRUE){
                setcookie('manager', $username, time() + 43200, "/");
                echo "<p class='success'>password created successfully</p>";
                header('refresh:2; url=../accounts/admin/accounts/manager');
              }else {
                echo "Error: " . $sql . "<br>" . $conn->error;
              }
            }elseif (isset($_SESSION['librarian'])) {
              $sql = "UPDATE admin SET password = '$password' WHERE username = '$username'";

              if ($conn->query($sql) === TRUE){
                setcookie('librarian', $username, time() + 43200, "/");
                echo "<p class='success'>password created successfully</p>";
                header('refresh:2; url=../accounts/admin/accounts/librarian');
              }else {
                echo "Error: " . $sql . "<br>" . $conn->error;
              }
            }else {
              header('location: ../');
            }
          }
        }
      }
    }

  }
}

 ?>
