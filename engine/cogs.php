<?php

require_once 'infused_cogs.php';
/*
------------------------------------------
GET IMAGE
------------------------------------------
*/
function getImage($link=''){
  global $conn;
  $sql = "SELECT * FROM school_info WHERE item = 'logo'";
  $result = $conn->query($sql);

  # get fields into variables
  if($result->num_rows > 0){
    while($row = $result->fetch_assoc()){
      $image = $row['description'];
    }

    echo "<img src='$link/images/$image' alt='school logo'>";
  }else {
    echo "<img src='$link/images/logo.png' alt='school logo'>";
  }
}

/*
--------------------------------------------
ALGORITHM TO GET CURRENT TERM
--------------------------------------------
*/
function getCurrentTerm(){

  $term = null;
  $current_month = date('m');

  if ($current_month == 1 || $current_month == 2 || $current_month == 3  || $current_month == 4) {
    $term = 1;
  }elseif ($current_month == 5 || $current_month == 6 || $current_month == 6  || $current_month == 8) {
    $term = 2;
  }else {
    $term = 3;
  }

  return $term;
}

/*
--------------------------------------------
GET DORMITORY COUNT FUNCTION
--------------------------------------------
*/
function getDormitoryCount($id){
  global $conn;
  $sql = "SELECT * FROM students WHERE dormitory_id = '$id'";
  $result = $conn->query($sql);

  $count = $result->num_rows;

  return $count;
}


/*
--------------------------------------------
DROP DOWN LIST FUNCTIONS
--------------------------------------------
*/
// TEACHER OPTIONS
function getTeacherOptions(){
  global $conn;

  $sql = "SELECT * FROM teachers ORDER BY name ASC";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $id = $row['teacher_id'];
    $name = $row['name'];

    echo "
    <option value='$id'>$name</option>
    ";
  }
}

// CLASS OPTIONS
function getClassOptions(){
  global $conn;

  $sql = "SELECT * FROM classes ORDER BY level ASC";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $id = $row['class_id'];
    $level = getLevel($row['level']);
    $section = getSection($row['section']);
    $teacher = getTeacher($row['class_teacher']);

    echo "<option value='$id'> $level $section</option>";
  }
}

// SECTION OPTIONS
function getSectionOptions(){

  global $conn;

  $sql = "SELECT * FROM sections ORDER BY name ASC";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $id = $row['section_id'];
    $name = $row['name'];

    echo "
    <option value='$id'>$name</option>
    ";
  }
}

// LEVEL OPTIONS
function getLevelOptions(){

  global $conn;

  $sql = "SELECT * FROM levels WHERE level_name != 'admission' ORDER BY level_name ASC";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $id = $row['level_id'];
    $name = $row['level_name'];

    echo "
    <option value='$id'>$name</option>
    ";
  }
}

// PARENT OPTIONS
function getParentOptions(){
  global $conn;

  $sql = "SELECT * FROM parents ORDER BY name ASC";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $id = $row['parent_id'];
    $name = $row['name'];

    echo "
    <option value='$id'>$name ".getParentPhone($id)."</option>
    ";
  }
}

// STUDENT OPTIONS
function getStudentOptions(){
  global $conn;

  $sql = "SELECT * FROM students ORDER BY name ASC";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $id = $row['student_id'];
    $name = $row['name'];

    echo "
    <option value='$id'>$name </option>
    ";
  }
}

// CLUB OPTIONS
function getClubOptions(){
  global $conn;

  $sql = "SELECT * FROM clubs ORDER BY name ASC";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $id = $row['club_id'];
    $name = $row['name'];

    echo "
    <option value='$id'>$name</option>
    ";
  }
}

// DORM OPTIONS
function getDormOptions(){
  global $conn;

  $sql = "SELECT * FROM dormitories ORDER BY name ASC";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $id = $row['dormitory_id'];
    $name = $row['name'];

    echo "
    <option value='$id'>$name</option>
    ";
  }
}

// ROUTE OPTIONS
function getRouteOptions(){
  global $conn;

  $sql = "SELECT * FROM transport ORDER BY name ASC";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $id = $row['transport_id'];
    $name = $row['name'];

    echo "
    <option value='$id'>$name</option>
    ";
  }
}

// EXAM OPTIONS
function getExamOptions(){
  global $conn;
  $current_term = getCurrentTerm();
  $current_year = date('Y');

  $sql = "SELECT * FROM exam WHERE YEAR(date) = '$current_year' AND term >= '$current_term' ORDER BY name ASC";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $id = $row['exam_id'];
    $name = $row['name'];
    $term = $row['term'];

    echo "
    <option value='$id' class='cap'>Term $term $name</option>
    ";
  }
}


// GET SUBJECTS
function getSubjectOptions(){
  global $conn;

  $sql = "SELECT * FROM subjects ORDER BY name ASC";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $id = $row['subject_id'];
    $name = $row['name'];

    echo "
    <option value='$id'>$name</option>
    ";
  }
}


# budget options
function getTermOptions(){

  for ($i=1; $i <= 3; $i++) {

    if ($i >= getCurrentTerm()) {
      echo "
      <option value='$i'>Term $i</option>
      ";
    }
  }
}

/*
--------------------------------------------
DELETE FUNCTION
--------------------------------------------
*/
function deleteUser($table, $field, $id, $message){
  global $conn;

  $sql = "DELETE FROM $table WHERE $field = '$id'";
  if ($conn->query($sql) === TRUE) {

    echo "$message deletion successful";

  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }
}


/*
--------------------------------------------
GET CLASS LIST FUNCTION
--------------------------------------------
*/
function getClassList($link='', $path=''){
  global $conn;
  $sql = "SELECT * FROM levels WHERE level_name != 'admission'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $id = $row['level_id'];
    $name = $row['level_name'];

    echo "
    <a href='$path../students/$link?level=$id'><li class='cap' style='background-color:#131519; padding-left:15%;'> $name</li></a>
    ";
  }
}

function getLeveList(){
  global $conn, $count;
  $sql = "SELECT * FROM levels WHERE level_name != 'admission'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $count += 1;
    $id = $row['level_id'];
    $name = $row['level_name'];

    echo "
    <a href='?level=$id'>
    <div class='parent col-sm-12' style='padding:1%;cursor:pointer'>
    <span> $count </span>
    <li class='cap'>$name</li>
    </div>
    </a>
    ";
  }
}


/*
--------------------------------------------
STUDENT PROFILE FUNCTIONS
--------------------------------------------
*/
// GET STUDENT PROFILE
function getStudentProfile($id){
  global $conn, $count;

  $value = null;
  $value2 = null;

  $sql = "SELECT * FROM students WHERE student_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $count += 1;
    $id = $row['student_id'];
    $admission_number = $row['admission_number'];
    $name = $row['name'];
    $location = $row['location'];
    $phone = $row['phone'];
    $email = $row['email'];
    $birthday = $row['birthday'];
    $class_id = $row['class_id'];
    $club_id = $row['club_id'];
    $parent_id = $row['parent_id'];
    $dormitory_id = $row['dormitory_id'];
    $transport_id = $row['transport_id'];

    if (authorizeParent($parent_id) == true || authorizeStudent($admission_number) == true || isset($_COOKIE['manager']) || isset($_COOKIE['admin']) || isset($_COOKIE['accountant'])) {
      $value = "
      <p><b>Email: </b>$email</p>
      <p class='cap'><b>Phone: </b>$phone</p>
      <p class='cap'><b>Location: </b>$location</p>
      ";

      $value2 = "<p class='cap'><b>Parent: </b>".getParent($parent_id)."</p>
      <p><b>Parent's phonenumber: </b>".getParentPhone($parent_id)."</p>
      ";
    }


    echo "
    <div class='margin'>
      <h4><span>Personal Information</span></h4>
      <p class='cap'><b>Name: </b>$name</p>
      <p class='cap'><b>Adm No: </b>$admission_number</p>
      $value
      <p class='cap'><b>Birthday: </b>$birthday</p>
    </div>

    <div class='margin'>
      <h4><span>School Information</span></h4>
      <p class='cap'><b>Class: </b>".getClass($class_id)."</p>
      $value2
      <p class='cap'><b>Club: </b>".getClub($club_id)."</p>
      <p class='cap'><b>Transport Route: </b>".getTransportRoute($transport_id)."</p>
    </div>

    ";
  }
}

// PROFILE PARENT SECURITY
function authorizeParent($parent_id){

  global $conn;
  $authorise = null;

  $sql = "SELECT phone FROM parents WHERE parent_id = '$parent_id'";
  $result = $conn->query($sql);

  if($result->num_rows == 1){
    while($row = $result->fetch_assoc()){
      $phone = $row['phone'];
    }

    if (isset($_COOKIE['parents']) && $_COOKIE['parents'] == $phone) {
      $authorise = true;
    }else {
      $authorise = false;
    }
  }else {
    $authorise = false;
  }

  return $authorise;

}


// PROFILE STUDENT SECURITY
function authorizeStudent($adm){
  $authorise = null;

  if (isset($_COOKIE['students']) && $_COOKIE['students'] == $adm) {
    $authorise = true;
  }else {
    $authorise = false;
  }

  return $authorise;
}



// GET BORROWED BOOKS
function getStudentLibraryInfo($id){

  global $conn;

  $count = 0;

  $sql = "SELECT * FROM borrowed_books WHERE student_id = '$id' AND status = 'borrowed' ORDER BY date_issued ASC";
  $result = $conn->query($sql);

  # get fields into variables
  if($result->num_rows > 0){
    while($row = $result->fetch_assoc()){
      $count += 1;
      $id = $row['issue_id'];
      $serialcode = $row['serial_code'];
      $bookid = $row['book_id'];
      $studentid = $row['student_id'];
      $date_issued = $row['date_issued'];

      if (timeSpan($date_issued) < 1) {
        $value = 'overdue';
        $style = "style='background-color:#ff3c3c; color:#fff'";
      }else {
        $value = timeSpan($date_issued).' left';
        $style = "style='background-color:#999; color:#fff;'";
      }

      echo "
        <div class='parent' $style>
          <ul>
          <span style='padding:0% 1%;'>$count<span>
          <li>".strtoupper($serialcode)."</li>
          <li class='cap'>".getBook($bookid)."</li>

          <li>".$value."</li>
          </ul>
        </div>
      ";
    }
  }else {
    echo "<p style='color:grey'>No books borrowed</p>";
  }
}


// FUNCTION TO ADD (S) IN THE END
function addS($time){
  if ($time != 1) {
    $s = 's';
  }else {
    $s = '';
  }

  return $s;
}

// DIFFERENCE IN TIME
function timeSpan($date){

  date_default_timezone_set('Africa/Nairobi');
  $current_date = date('Y-m-d H:i:s');

  $seconds  = strtotime($date) + 2592000;

  $seconds = $seconds - strtotime($current_date);

    $months = floor($seconds / (3600*24*30));
    $day = floor($seconds / (3600*24));
    $hours = floor($seconds / 3600);
    $mins = floor(($seconds - ($hours*3600)) / 60);
    $secs = floor($seconds % 60);

    if($seconds < 60)
        $time = $secs." second".addS($secs);
    else if($seconds < 60*60 )
        $time = $mins." min".addS($mins);
    else if($seconds < 24*60*60)
        $time = $hours." hour".addS($hours);
    else if($seconds < 30*24*60*60)
        $time = $day." day".addS($day);
    else
        $time = $months." month".addS($months);

    return $time;
}

#difference in time
function otherTimeSpan($date){

  date_default_timezone_set('Africa/Nairobi');
  $current_date = date('Y-m-d H:i:s');

  $seconds  = strtotime(date($current_date)) - strtotime($date);

    $months = floor($seconds / (3600*24*30));
    $day = floor($seconds / (3600*24));
    $hours = floor($seconds / 3600);
    $mins = floor(($seconds - ($hours*3600)) / 60);
    $secs = floor($seconds % 60);

    if($seconds < 60)
        $time = $secs." second".addS($secs)." ago";
    else if($seconds < 60*60 )
        $time = $mins." min".addS($mins)." ago";
    else if($seconds < 24*60*60)
        $time = $hours." hour".addS($hours)." ago";
    else if($seconds < 30*24*60*60)
        $time = $day." day".addS($day)." ago";
    else
        $time = $months." month".addS($months)." ago";

    return $time;
}


#difference in time
function timeDifference($date){

  date_default_timezone_set('Africa/Nairobi');
  $current_date = date('Y-m-d H:i:s');

  $seconds  = strtotime(date($current_date)) - strtotime($date);

  $months = floor($seconds / (3600*24*30));
  $day = floor($seconds / (3600*24));
  $hours = floor($seconds / 3600);
  $mins = floor(($seconds - ($hours*3600)) / 60);
  $secs = floor($seconds % 60);

  if($seconds < 60)
      $time = 'second';
  else if($seconds < 60*60 )
      $time = 'minute';
  else if($seconds < 24*60*60)
      $time = 'hour';
  else if($seconds < 30*24*60*60)
      $time = 'day';
  else
      $time = 'month';

  return $time;
}


// FUNCTION THAT GETS CLASS WHERE TEACHER IS CLASS TEACHER
function getClassForClassTeacher($teacher_id){

  global $conn;

  $sql = "SELECT class_id FROM classes WHERE class_teacher = '$teacher_id'";
  $result = $conn->query($sql);

  # get fields into variables
  if($result->num_rows > 0){
    while($row = $result->fetch_assoc()){
      $class_id = $row['class_id'];
    }
    $value = getClass($class_id);
  }else {
    $value =  "<span style='color:grey;'>unassigned</span>";
  }


  return $value;
}


/*
--------------------------------------------
QUICK GET FUNCTIONS
--------------------------------------------
*/
# function to get single section
function getSection($id){
  global $conn;

  $sql = "SELECT name FROM sections WHERE section_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $name = $row['name'];
  }

  return $name;
}

# function to get level
function getLevel($id){
  global $conn;

  $sql = "SELECT level_name FROM levels WHERE level_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $name = $row['level_name'];
  }

  return $name;
}

# function to get single teacher
function getTeacher($id){
  global $conn;

  $sql = "SELECT name FROM teachers WHERE teacher_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  if($result->num_rows > 0){
    while($row = $result->fetch_assoc()){
      $name = $row['name'];
    }
  }else {
    $name = '<span style="color:grey;">unassigned</span>';
  }

  return $name;
}

# function to get single teacher
function getTeacherByNumber($phone){
  global $conn;

  $sql = "SELECT name FROM teachers WHERE phone = '$phone'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $name = $row['name'];
  }

  if ($phone == 0) {
    $name = '<span style="color:grey;">unassigned</span>';
  }

  return $name;
}

# function to get single student
function getStudent($id){
  global $conn;

  $sql = "SELECT name FROM students WHERE student_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $name = $row['name'];
  }

  if ($id == 0) {
    $name = '<span style="color:grey;">unassigned</span>';
  }

  return $name;
}

# function to get single student by account number
function getStudentByAccount($id){
  global $conn;

  $sql = "SELECT name FROM students WHERE accountno = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $name = $row['name'];
  }

  if ($id == 0) {
    $name = '<span style="color:grey;">unassigned</span>';
  }

  return $name;
}

# get student admission number
function getStudentAdm($id){
  global $conn;

  $sql = "SELECT admission_number FROM students WHERE student_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $adm = $row['admission_number'];
  }

  if ($id == 0) {
    $adm = '<span style="color:grey;">unassigned</span>';
  }

  return $adm;
}

# get book
function getBook($id){
  global $conn;

  $sql = "SELECT name FROM books WHERE book_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $name = $row['name'];
  }

  if ($id == 0) {
    $name = '<span style="color:grey;">unassigned</span>';
  }

  return $name;
}

# function to get single parent
function getParent($id){
  global $conn;

  $sql = "SELECT name FROM parents WHERE parent_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $name = $row['name'];
  }

  if ($id == 0) {
    $name = '<span style="color:grey;">unassigned</span>';
  }

  return $name;
}

# function to get parent phone number
function getParentPhone($id){
  global $conn;

  $sql = "SELECT phone FROM parents WHERE parent_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $phone = $row['phone'];
  }

  if ($id == 0) {
    $phone = '<span style="color:grey;">unassigned</span>';
  }

  return $phone;
}



# get class name
function getClass($id){

  global $conn;
  $sql = "SELECT * FROM classes WHERE class_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $name = getLevel($row['level']);
    $section = getSection($row['section']);
    $name = $name.' '.$section;
  }

  if ($id == 0) {
    $name = '<span style="color:grey;">unassigned</span>';
  }

  return $name;
}

# get club
function getClub($id){

  global $conn;
  $sql = "SELECT * FROM clubs WHERE club_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $name = $row['name'];
  }

  if ($id == 0) {
    $name = '<span style="color:grey;">unassigned</span>';
  }

  return $name;

}

# get club
function getDorm($id){

  global $conn;
  $sql = "SELECT * FROM dormitories WHERE dormitory_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $name = $row['name'];
  }

  if ($id == 0) {
    $name = '<span style="color:grey;">unassigned</span>';
  }

  return $name;

}

# get trnasport route
function getTransportRoute($id){
  global $conn;
  $sql = "SELECT * FROM transport WHERE transport_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $name = $row['name'];
  }

  if ($id == 0) {
    $name = '<span style="color:grey;">unassigned</span>';
  }

  return $name;
}

# get subject
function getSubject($id){

  global $conn;
  $sql = "SELECT * FROM subjects WHERE subject_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $name = $row['name'];
  }

  if ($id == 0) {
    $name = '<span style="color:grey;">unassigned</span>';
  }

  return $name;
}

# get Exam
function getExam($id){
  global $conn;
  $sql = "SELECT * FROM exam WHERE exam_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $name = $row['name'];
  }

  if ($id == 0) {
    $name = '<span style="color:grey;">unassigned</span>';
  }

  return $name;
}

# get Exam
function getExamTerm($id){
  global $conn;
  $sql = "SELECT * FROM exam WHERE exam_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $term = $row['term'];
  }

  if ($id == 0) {
    $name = '<span style="color:grey;">unassigned</span>';
  }

  return $term;
}


/*
---------------------------------------------------
UNIQUE NUMBER IIDENTIFIER
---------------------------------------------------
*/
function uniqueNumber($phone){

  global $conn;
  $phone_number_array = array(); # array to store contacts

  # contacts from students
  $sql = "SELECT phone FROM students";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $phone_number_array[] = $row['phone'];
  }

  # get contacts from tachers table
  $sql = "SELECT phone FROM teachers";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $phone_number_array[] = $row['phone'];
  }

  #
  $sql = "SELECT phone FROM parents";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $phone_number_array[] = $row['phone'];
  }

  # check if number exists in array
  if(in_array($phone,$phone_number_array) === false){
    return "<p class='error'>That number already exists</p>";
  }
}





 ?>
