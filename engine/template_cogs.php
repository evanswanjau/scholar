<?php

 require_once 'infused_cogs.php'; //////////////////////////////////////////////

/*
---------------------------------------------
CHECK IF THERE'S AN INTERNET CONNECTION
---------------------------------------------
*/

function internetConnection(){

    $status = null;
    $connected = @fsockopen("www.google.com", 80);

    if ($connected){
        $status = 'online';
        fclose($connected);
    }else{
        $status = 'offline';
    }

    return $status;
}


/*
---------------------------------------------
RETURN HEADER TEMPLATE
---------------------------------------------
*/
function getHeadTemplate($path = ''){

  echo '
  <link rel="stylesheet" href="'.$path.'frameworks/bootstrap-theme.min.css">
  <link rel="stylesheet" href="'.$path.'frameworks/bootstrap.min.css">
  <link rel="stylesheet" href="'.$path.'frameworks/jquery-ui.min.css">
  <script type="text/javascript" src="'.$path.'frameworks/bootstrap.js"></script>
  <script type="text/javascript" src="'.$path.'frameworks/jquery-1.11.3.min.js"></script>
  <script type="text/javascript" src="'.$path.'frameworks/jquery-ui.min.js"></script>
  <script type="text/javascript" src="'.$path.'frameworks/jquery.js"></script>
  <script src="'.$path.'js/Chart.bundle.js"></script>
  <script>
    var myChart = new Chart(ctx, {...});
  </script>
  <style media="screen">
    @font-face {
      font-family: nunito;
      src: url('.$path.'css/Nunito-Regular.ttf);
    }
    *{
      font-family:nunito;
    }

    @font-face {
      font-family: "Material Icons";
      font-style: normal;
      font-weight: 400;
      src: url('.$path.'frameworks/iconfont/MaterialIcons-Regular.eot); /* For IE6-8 */
      src: local("Material Icons"),
      local("MaterialIcons-Regular"),
      url('.$path.'frameworks/iconfont/MaterialIcons-Regular.woff2) format("woff2"),
      url('.$path.'frameworks/iconfont/MaterialIcons-Regular.woff) format("woff"),
      url('.$path.'frameworks/iconfont/MaterialIcons-Regular.ttf) format("truetype");
    }

    .material-icons {
      font-family: "Material Icons";
      font-weight: normal;
      font-style: normal;
      font-size: 24px;  /* Preferred icon size */
      display: inline-block;
      line-height: 1;
      text-transform: none;
      letter-spacing: normal;
      word-wrap: normal;
      white-space: nowrap;
      direction: ltr;

      /* Support for all WebKit browsers. */
      -webkit-font-smoothing: antialiased;
      /* Support for Safari and Chrome. */
      text-rendering: optimizeLegibility;

      /* Support for Firefox. */
      -moz-osx-font-smoothing: grayscale;

      /* Support for IE. */
      font-feature-settings: "liga";
    }

  </style>
  ';
}

 ?>
