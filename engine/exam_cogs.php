<?php

require_once 'infused_cogs.php';

/*
--------------------------------------------
DISPLAY MARK FUNCTIONS
--------------------------------------------
*/
function getMarks($subject_id, $class_id, $exam_id, $studentid, $count){

  global $conn;

  $value = 0;
  $inputvalue = 1;
  $edit = 2;
  $update = 3;

  $sql = "SELECT * FROM marks WHERE exam_id = '$exam_id' AND subject_id = '$subject_id' AND class_id = '$class_id'  AND student_id = '$studentid'";
  $result = $conn->query($sql);

  # get fields into variables
  if($result->num_rows > 0){
    while($row = $result->fetch_assoc()){
      $id = $row['mark_id'];
      $subject_id= $row['subject_id'];
      $student_id = $row['student_id'];
      $class_id = $row['class_id'];
      $points = $row['points'];
      $exam_id = $row['exam_id'];
      $added_by = $row['added_by'];
      $status = $row['status'];

      $editor = ($status == 0) ? "<li>$added_by</li>" : "<li>$added_by <span style='color:green;font-size:7px;float:right;'><b>edited</b></span></li>" ;

      $value += $id;
      $inputvalue += $id + 10;
      $edit += $id + 24;
      $update += $id + 35;

      /*  onmouseleave='return rechangeValue($point, $pointed, $clicker)'*/
      echo "
      <div class='col-sm-12 parent divine'  onmouseleave='return hideItems($value, $inputvalue, $update)'>
      <form method='post' action='' name='myForm'>
        <ul>
          <span>$count</span>
          <li style='width:10%;'>".getStudentAdm($studentid)."</li>
          <li class='cap'>".getStudent($studentid)."</li>
          <li>".getSubject($subject_id)."</li>
          <li style='width:10%;' class='issue'><span id='$value'>$points</span> <input id='$inputvalue' style='display:none;' type='number' name='points' value='$points'></li>
          <li style='width:10%;'>".getGrade($points)."</li>
          $editor
          <li style='width:10%;'>
          <input type='hidden' name='markid' value='$id'>
          <a class='material-icons right' id='$edit' title='edit' onclick='return revealEditItems($value, $inputvalue, $update)'>edit</a>
          <input type='submit' style='opacity:0;' id='$update' class='updateEdit' value='edit' name='update-marks'>
          </li>
        </ul>
        </form>
      </div>";
    }
  }else {
    echo "
    <div class='col-sm-12 parent'>
    <form method='post' action=''>
      <ul>
        <span>$count</span>
        <li style='width:10%;'>".getStudentAdm($studentid)."</li>
        <li class='cap'>".getStudent($studentid)."</li>
        <li>".getSubject($subject_id)."</li>
        <li style='width:10%;'>
        <input type='number' name='points' value='0'>
        <input type='hidden' name='studentid' value='$studentid'>
        <input type='hidden' name='subject_id' value='$subject_id'>
        <input type='hidden' name='class_id' value='$class_id'>
        <input type='hidden' name='exam_id' value='$exam_id'>
        </li>
        <li style='width:10%;color:grey;'>undefined</li>
        <li><input type='submit' name='add-marks' value='add'></li>
      </ul>
      </form>
    </div>";
  }
}


/*
--------------------------------------------
ADD MARKS
--------------------------------------------
*/
function addMarks(){
  global $conn;

  if (isset($_COOKIE['teachers'])) {
    $username = $_COOKIE['teachers'];
    $username = getTeacherByNumber($username);
  }else {
    $username = null;
  }

  if (isset($_POST['add-marks']) && $_POST['points'] > -1 && $_POST['points'] < 101) {

    $student_id = $_POST['studentid'];
    $subject_id = $_POST['subject_id'];
    $class_id = $_POST['class_id'];
    $points = $_POST['points'];
    $exam_id = $_POST['exam_id'];

    $sql = "INSERT INTO marks ( subject_id, student_id, class_id, points, exam_id, added_by)
    VALUES ('$subject_id', '$student_id', '$class_id', '$points', '$exam_id', '$username')";

    if ($conn->query($sql) === TRUE) {
    }else {
      echo "Error: " . $sql . "<br>" . $conn->error;
    }
  }
}

# call add marks function
addMarks();

/*
--------------------------------------------
GRADE MARKS
--------------------------------------------
*/
# get mark grade
function getGrade($mark){

  global $conn;
  $sql = "SELECT * FROM grade";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $name = $row['name'];
    $min = $row['mark_from'];
    $max = $row['mark_upto'];

    if ($mark >= $min && $mark <= $max) {
      $grade = $name;
    }
  }

  return $grade;
}

# function to get comments
function getComments($grade){
  global $conn;
  $sql = "SELECT * FROM grade WHERE name = '$grade'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $comment = $row['comment'];
  }

  return $comment;
}

/*
------------------------------------------------
EDIT MARKS
------------------------------------------------
*/
function updateMarks(){
  global $conn;

  if (isset($_COOKIE['teachers'])) {
    $username = $_COOKIE['teachers'];
    $username = getTeacherByNumber($username);
  }else {
    $username = null;
  }

  if (isset($_POST['update-marks'])  && $_POST['points'] > -1 && $_POST['points'] < 101) {
    $markid = $_POST['markid'];
    $points = $_POST['points'];

    $sql = "UPDATE marks SET `points` = '$points', `added_by` = '$username',  `status`= 1 WHERE mark_id = '$markid'";

    if ($conn->query($sql) === TRUE) {
    }else {
      echo "Error: " . $sql . "<br>" . $conn->error;
    }
  }
}

updateMarks();



/*
------------------------------------------------
EXAM FUNCTIONS
------------------------------------------------
*/
function getExams($admin = false){

  global $conn, $count;

  $sql = "SELECT * FROM exam";
  $result = $conn->query($sql);

  # get fields into variables
  if($result->num_rows > 0){
    echo '
    <button class="button right show-form">add new exam</button>
    <br><br>
    <div class="formal parent">
      <ul><b>
        <li>Exam Name</li>
        <li>Term</li>
        <li>Date</li></b>
      </ul>
    </div>
    ';
    while($row = $result->fetch_assoc()){
      $count += 1;
      $id = $row['exam_id'];
      $name = $row['name'];
      $term = $row['term'];
      $date = date_format(new dateTime($row['date']), "jS M Y H:i:s");

      if ($admin == true) {
        $value = "<a class='material-icons right show-delete' title='delete' style='cursor:pointer' onclick='return deleteValue($id)'>delete</a>
        <a class='material-icons right show-form' title='edit' style='cursor:pointer' onclick='return getExamData($id)'>edit</a>";
      }

      echo "
      <div class='col-sm-12 parent'>
        <ul>
          <span>$count</span>
          <li class='cap'>$name</li>
          <li>Term $term</li>
          <li><b>$date</b></li>
          $value
        </ul>
      </div>";
    }
  }else {
    echo '
    <div class="add-items">
      <h1>Your exam list is empty</h1>
      <button class="show-form">add new exam</button>
    </div>';
  }
}


# ADD EXAM TO DB
if (isset($_POST['add-exam'])) {
  header("Content-Type: application/json; charset=UTF-8");
  global $conn;

  $exam = json_decode($_POST['add-exam']);

  $name = clean_data(strtolower($exam->name));
  $term = clean_data(strtolower($exam->term));

  #Inserting the user's data into our database
  $sql = "INSERT INTO exam (name, term)
  VALUES ('$name', '$term')";

  if ($conn->query($sql) === TRUE) {
    echo "exam added successfully";
  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }

}

# GET EXAM VALUES FROM DB TO JS
if (isset($_GET['get_exam_data'])) {

  $id = $_GET['get_exam_data'];

  $sql = "SELECT * FROM exam WHERE exam_id = '$id'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $id = $row['exam_id'];
    $name = $row['name'];
    $term = $row['term'];
  }

  $exam = new stdClass;
  $exam->id = $id;
  $exam->name = $name;
  $exam->term = $term;

  echo json_encode($exam);

}


// EDIT EXAM
if (isset($_POST['edit-exam-update'])) {
  header("Content-Type: application/json; charset=UTF-8");
  global $conn;

  $exam = json_decode($_POST['edit-exam-update']);

  $id = clean_data(strtolower($exam->id));
  $name = clean_data(strtolower($exam->name));
  $term = clean_data($exam->term);

  #Inserting the user's data into our database
  $sql = "UPDATE Exam SET `name` = '$name', `term` = '$term' WHERE exam_id = '$id'";

  if ($conn->query($sql) === TRUE) {
    echo "exam edited successfully";
  }else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }

}

// DELETE NOTICE
if (isset($_GET['delete_exam'])) {
  $id = $_GET['delete_exam'];

  deleteUser('exam', 'exam_id', $id, 'exam');
}


// GET TERM LIST FOR EXAM DROPDOWN
function getExamDropDown($link='', $path=''){
  $current_year = date('Y');

  global $conn;

  for ($i=1; $i <= 3; $i++) {

    echo "<a class='open-exam-list-$i'><li class='cap' style='background-color:#15181c;cursor:pointer; padding-left:15%;'>Term $i</li></a><ul class='exam-list-$i'>";

    $sql = "SELECT * FROM exam WHERE YEAR(date)='$current_year' AND term = $i ORDER BY name DESC";
    $result = $conn->query($sql);

    # get fields into variables
    if($result->num_rows > 0){
      while($row = $result->fetch_assoc()){
        $id = $row['exam_id'];
        $name = $row['name'];

        echo "
        <a href='$path $link?exam_id=$id'><li class='cap' style='background-color:#131519; padding-left:15%;'> $name</li></a>
        ";
      }
    }
    echo "</ul>";
  }
}

// SUBJECTS
function getClassListLinks($exam_id){
  global $conn, $count;

  $sql = "SELECT * FROM classes ORDER BY level ASC";
  $result = $conn->query($sql);

  if($result->num_rows > 0){
    # get fields into variables
    while($row = $result->fetch_assoc()){
      $count += 1;
      $id = $row['class_id'];
      $level = $row['level'];
      $section = $row['section'];
      $class_teacher = $row['class_teacher'];

      echo "
      <a href='?exam_id=$exam_id&class=$id'>
      <div class='col-sm-12 parent' style='cursor:pointer;padding-bottom:1%;'>
        <span>$count</span>
        <li class='cap'>".getLevel($level)."</li>
        <li class='cap'>".getSection($section)."</li>
        <li class='cap'>".getTeacher($class_teacher)."</li>
      </div>";
    }
  }else {
    echo "<p style='color:grey'>No assigned sections in this level</p>";
  }
}

// GET CLASS LISt BY EXAM CLASS
function getStudentsForExam($exam_id, $class_id){
  $total_marks = null;
  $total_marks_info = array();

  global $conn, $count;

  $sql = "SELECT * FROM students WHERE class_id = $class_id ORDER BY name ASC";
  $result = $conn->query($sql);

  if($result->num_rows > 0){

    echo "<table border='1'>";
    echo "<tr><th>Position</th><th>Student Name</th>";
    foreach (getSubjectMarks() as $subject_id) {
       echo "<th>".getSubject($subject_id)."</th>";
    }
    echo "<th>Total Marks</th>
    <th>Grade</th>
    </tr>";

    # get fields into variables
    while($row = $result->fetch_assoc()){
      $student_id = $row['student_id'];

      $subjects = implode(',',getSubjectMarks());

      foreach (getSubjectMarks() as $subject_id) {
         $total_marks += examPerformance($exam_id,$subject_id, $student_id);
      }
      $total_marks = intVal($total_marks / count(getSubjectMarks()));
      $total_marks_info[] = array('student_id' => $student_id, 'subjects' => $subjects, 'total_marks' => $total_marks);
    }

    // sort array
    $total= array();
    foreach ($total_marks_info as $key => $row){
      $total[$key] = $row['total_marks'];
    }
    array_multisort($total, SORT_DESC, $total_marks_info);

    # echo out as a table
    for ($i=0; $i < count($total_marks_info); $i++) {
      $count += 1;

      $subject_id = getSubjectMarks()[$i];
      $student_id = $total_marks_info[$i]['student_id'];

      echo "<tr>
      <td>$count</td>
      <td class='cap'><a href='report-form/?exam-card=$student_id'>".getStudent($total_marks_info[$i]['student_id'])."</a></td>
      ";

      foreach (getSubjectMarks() as $subject_id) {
        echo "<td>".examPerformance($exam_id,$subject_id, $student_id)."</td>";
      }

      $total_marks = $total_marks_info[$i]['total_marks'];

      echo "<td>$total_marks%</td>
          <td>".getGrade($total_marks)."</td>
      </tr>";
    }
    echo "</table><p>&nbsp</p>";
  }else {
    echo "no students in that category";
  }
}


// FUNCTION TO GET SUBJECT MARKS
function getSubjectMarks(){

  $subject_array = array();

  global $conn;

  $sql = "SELECT * FROM subjects ORDER BY name ASC";
  $result = $conn->query($sql);

  if($result->num_rows > 0){
    # get fields into variables
    while($row = $result->fetch_assoc()){
      $subject_array[] = $row['subject_id'];
    }
  }else {
    $subject_array = null;
  }

  return $subject_array;
}

// SHOW EXAM PERFORMANCE
function examPerformance($exam_id,$subject_id, $student_id){
  $subject_points = null;

  global $conn, $count;

  $sql = "SELECT * FROM marks WHERE exam_id = '$exam_id' AND subject_id = '$subject_id' AND student_id = '$student_id'";
  $result = $conn->query($sql);

  if($result->num_rows > 0){
    while($row = $result->fetch_assoc()){
      $id = $row['mark_id'];
      $subject_id= $row['subject_id'];
      $studentid = $row['student_id'];
      $class_id = $row['class_id'];
      $points = $row['points'];
      $exam_id = $row['exam_id'];
      $added_by = $row['added_by'];
      $status = $row['status'];

      $subject_points = $points;
    }
  }else {
    $subject_points = 0;
  }

  return $subject_points;
}

// function to get report form
function getReportCard($student_id){
  global $conn;

  $current_term = getCurrentTerm();
  $current_year = date('Y');
  $exam_id_array = array();

  $sql = "SELECT * FROM exam WHERE term = '$current_term'AND YEAR(date) = '$current_year' ORDER BY name DESC";
  $result = $conn->query($sql);

  while($row = $result->fetch_assoc()){
    $exam_id_array[] = $row['exam_id'];
  }

  echo "<table border='1'>
  <tr>
    <th>Subjects</th>
  ";

  foreach ($exam_id_array as $exam) {
    echo "<th class='cap'>".getExam($exam)."</th>
    <th>Grade</th>
    <th>Comments</th>";
  }
  echo "</tr><tr>";

  foreach (getSubjectMarks() as $key) {
    echo "<td>".getSubject($key)."</td>";
    foreach ($exam_id_array as $exam) {
      $sql = "SELECT * FROM marks WHERE exam_id = '$exam' AND subject_id = '$key' AND student_id = $student_id";
      $result = $conn->query($sql);
      if($result->num_rows > 0){
        while($row = $result->fetch_assoc()){
          echo "<td>".$row['points']."</td>";
          echo "<td>".getGrade($row['points'])."</td>";
          echo "<td>".getComments(getGrade($row['points']))."</td>";
        }
      }else {
        echo "<td></td><th>  </th><th> </th>";
      }
    }
    echo "</tr>";
  }

  echo "</table>";

}



 ?>
