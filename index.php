<?php require_once 'engine/infused_cogs.php'; ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Login</title>
    <?php getHeadTemplate(); ?>
    <link rel="stylesheet" href="css/main.css">
    <script type="text/javascript" src="js/query.js"></script>
  </head>
  <body>

    <!-- home page -->
    <section>
      <div class="container-fluid welcome-to-login">
        <div class="row welcome-section">
          <div class="col-sm-7 text">
            <h1>Welcome to Scholar</h1>
            <p>School Management System</p>
          </div>

          <!-- Login Form -->
          <div class="col-sm-5 form">
            <form class="ui-form" action="" method="post">
              <h4>Login</h4>
              <?php loginUser();?>
              <p>use email / phone number / admission number (for students)</p>
              <input type="text" name="username" placeholder="email, phonenumber, admission number"><br><br>
              <p>password</p>
              <input type="password" name="password" placeholder="use your current password"><br><br>
              <p><a href="#">Forgot password?</a></p>
              <input type="submit" name="login" value="login"><br><br>
            </form>
          </div>
        </div>
      </div>
    </section>
  </body>
</html>
