<?php
 require_once '../../../engine/infused_cogs.php';

 parentSecurity($link='../../../');
 logOut($link='../../../');

 ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <?php getHeadTemplate($path='../../../'); ?>
    <link rel="stylesheet" href="../../../css/main.css">
    <script type="text/javascript" src="../../../js/script.js"></script>
  </head>
  <body>

    <div class="top-menu">
      <ul>
        <a href="../"><li style="width:50%;font-size:16px;"><i class="material-icons left"> account_balance</i>&nbsp;&nbsp;Scholar School Management System</li></a>
        <span>
          <a href="../messages"><li><i class="material-icons top" title="notifications">notifications_none</i></li></a>
          <a href="../myaccount"><li><i class="material-icons top" title="my account">account_circle</i></li></a>
          <a href='?logout'><li><i class="material-icons top" title="logout">exit_to_app</i></li></a>
        </span>
      </ul>
    </div>

    <div class="container-fluid">
      <div class="row">

        <div class="col-sm-3 side-menu">
          <img src="../../../images/logo.png" alt="">
          <!-- our menu -->
          <ul>
            <a href="../messages"><li><i class="material-icons">notifications_none</i>messages</li></a>
            <a href="../noticeboard"><li><i class="material-icons">chat</i>noticeboard</li></a>
            <a class="open-class" style="cursor:pointer;"><li><i class="material-icons">group</i>students</li></a>
            <ul class="class-list"><?php getClassList(); ?></ul>
            <a href="../finance"><li class="current"><i class="material-icons">attach_money</i>finance</li></a>
            <a href="../myaccount"><li><i class="material-icons">account_circle</i>my account</li></a>
          </ul>
        </div>

        <div class="col-sm-9 other-side">
          <h3>Student Invoice</h3>
          <div class="invoice-page">
            <h3 class="formal">JUHUDI SECONDARY SCHOOL</h3>
            <p>P.O. BOX 65 KIAMBU</p>
            <p>CALL 0788445522</p>
            <h3 class="bold formal">STUDENT INVOICE</h3>
            <?php

            if (isset($_COOKIE['parents'])) {
              $username = $_COOKIE['parents'];

              $sql = "SELECT * FROM parents WHERE phone = '$username'";
              $result = $conn->query($sql);

              # get fields into variables
              while($row = $result->fetch_assoc()){
                $id = $row['parent_id'];
              }

              $sql = "SELECT * FROM students WHERE parent_id = '$id'";
              $result = $conn->query($sql);

              while($row = $result->fetch_assoc()){
                $student_id = $row['student_id'];
                getStudentInvoice($student_id);
              }
            }


            ?>
          </div>
        </div>

      </div>
    </div>
  </body>
</html>
