<?php
 require_once '../../../engine/infused_cogs.php';

 studentSecurity($link='../../../');
 logOut($link='../../../');

 if (isset($_COOKIE['students'])) {
   $username = $_COOKIE['students'];

   $sql = "SELECT * FROM students WHERE admission_number = '$username'";
   $result = $conn->query($sql);

   # get fields into variables
   while($row = $result->fetch_assoc()){
     $count += 1;
     $id = $row['student_id'];
     $admission_number = $row['admission_number'];
     $name = $row['name'];
     $address = $row['address'];
     $phone = $row['phone'];
     $email = $row['email'];
     $birthday = $row['birthday'];
     $dbpassword = $row['password'];
   }
 }
 ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>My Account</title>
    <?php getHeadTemplate($path='../../../'); ?>
    <link rel="stylesheet" href="../../../css/main.css">
    <script type="text/javascript" src="../../../js/script.js"></script>
  </head>
  <body>

    <div class="top-menu">
      <ul>
        <a href="../"><li style="width:50%;font-size:16px;"><i class="material-icons left"> account_balance</i>&nbsp;&nbsp;Scholar School Management System</li></a>
        <span>
          <a href="../messages"><li><i class="material-icons top" title="notifications">notifications_none</i></li></a>
          <a href="../myaccount"><li><i class="material-icons top" title="my account">account_circle</i></li></a>
          <a href='?logout'><li><i class="material-icons top" title="logout">exit_to_app</i></li></a>
        </span>
      </ul>
    </div>

    <div class="container-fluid">
      <div class="row">

        <div class="col-sm-3 side-menu">
          <img src="../../../images/logo.png" alt="">
          <!-- our menu -->
          <ul>
            <a href="../messages"><li><i class="material-icons">notifications_none</i>messages</li></a>
            <a href="../noticeboard"><li><i class="material-icons">chat</i>noticeboard</li></a>
            <a class="open-class" style="cursor:pointer;"><li><i class="material-icons">group</i>students</li></a>
            <ul class="class-list"><?php getClassList(); ?></ul>
            <a href="../myaccount"><li class="current"><i class="material-icons">account_circle</i>my account</li></a>
          </ul>
        </div>

        <div class="col-sm-9 other-side">
          <h3>My account</h3>

          <!-- edit personal information -->
          <form class="ui-form" action="" method="post">
            <?php updateUserData($account = 'students', $clause='student_id', $id); ?>
            <input type="text" name="name" disabled placeholder="full name" value="<?php if (empty($name)){ $name=null;}else {echo $name; } ?>"><br><br>
            <input type="text" name="adm" disabled placeholder="admission_number" value="<?php if (empty($admission_number)){ $admission_number=null;}else {echo $admission_number; } ?>"><br><br>
            <input type="email" name="email" disabled placeholder="email" value="<?php if (empty($email)){ echo null;}else {echo $email; } ?>"><br><br>
            <input type="text" name="phone_number" disabled placeholder="phone_number" value="<?php if (empty($phone)){ echo null;}else {echo $phone; } ?>"><br><br>
            <input type="text" name="address" disabled placeholder="address" value="<?php if (empty($address)){ echo null;}else {echo $address; } ?>"><br><br>
            <input type="submit" name="update-profile" disabled value="update">
          </form>

          <!-- change password -->
          <form class="ui-form" action="" method="post">
            <?php changePassword($dbpassword, $id, $clause='parents', $clausevalue='parent_id'); ?>
            <input type="password" name="current" placeholder="current password"><br><br>
            <input type="password" name="password" placeholder="new password"><br><br>
            <input type="password" name="confirm" placeholder="confirm password"><br><br>
            <input type="submit" name="change-password" value="change password">
          </form>
        </div>

      </div>
    </div>
  </body>
</html>
