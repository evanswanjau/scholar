<?php require_once '../../../../engine/infused_cogs.php'; studentSecurity($link='../../../../'); logOut($link='../../../../');?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Exam Perfomance</title>
    <?php getHeadTemplate($path='../../../../'); ?>
    <link rel="stylesheet" href="../../../../css/main.css">
    <script type="text/javascript" src="../../../../js/script.js"></script>
    <script type="text/javascript" src="../../../../js/uncategorized.js"></script>
  </head>
  <body>

    <div class="top-menu">
      <ul>
        <a href="../"><li style="width:50%;font-size:16px;"><i class="material-icons left"> account_balance</i>&nbsp;&nbsp;Scholar School Management System</li></a>
        <span>
          <a href="#"><li><i class="material-icons top" title="notifications">notifications_none</i></li></a>
          <a href="../myaccount"><li><i class="material-icons top" title="my account">account_circle</i></li></a>
          <a href='?logout'><li><i class="material-icons top" title="logout">exit_to_app</i></li></a>
        </span>
      </ul>
    </div>

    <div class="container-fluid">
      <div class="row">

        <div class="col-sm-3 side-menu">
          <img src="../../../../images/logo.png" alt="">
          <!-- our menu -->
          <ul>
            <a href="../messages"><li><i class="material-icons">notifications_none</i>messages</li></a>
            <a href="../noticeboard"><li><i class="material-icons">chat</i>noticeboard</li></a>
            <a class="open-class" style="cursor:pointer;"><li><i class="material-icons">group</i>students</li></a>
            <ul class="class-list"><?php getClassList(); ?></ul>
            <a style="cursor:pointer;" class="open-term-list"><li class="current"><i class="material-icons">event_note</i>exams</li></a>
            <ul class="term-list"><?php getExamDropDown($path='../'); ?></ul>
            <a href="../myaccount"><li><i class="material-icons">account_circle</i>my account</li></a>
          </ul>
        </div>

          <div class="col-sm-9 other-side">
            <h3>Report Form</h3>
            <br>
            <div class="report-card" id='printableArea'>
              <?php if (isset($_GET['exam-card'])): ?>
                <h4 class="formal">JUHUDI SECONDARY SCHOOL</h4>
                <p>P.O. BOX 65 KIAMBU</p>
                <p>CALL 0788445522</p>
                <h4 class="bold formal">STUDENT REPORT CARD</h4>
                <?php $id = $_GET['exam-card'];
                $sql = "SELECT * FROM students WHERE student_id = $id";
                $result = $conn->query($sql);

                # get fields into variables
                while($row = $result->fetch_assoc()){
                  $admission_number = $row['admission_number'];
                  $name = $row['name'];
                  $class = getClass($row['class_id']);
                }

                echo "
                  <h5 class='cap formal'><b>Name: </b>$name</h5>
                  <h5 class='cap formal'><b>Adm Number: </b>$admission_number</h5>
                  <h5 class='cap formal'><b>Class: </b>$class</h5>
                  <p class='boundary'></p>
                ";

                 ?>
                <?php getReportCard($id); ?>
                <div class="ui-form">
                  <input type="submit" class="nonPrintable" onclick="printDiv()" value="print exam report"/>
                </div>
              <?php endif; ?>

            </div>
          </div>

      </div>
    </div>
  </body>
</html>
