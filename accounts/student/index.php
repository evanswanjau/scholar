<?php require_once '../../engine/infused_cogs.php';  studentSecurity($link='../../'); logOut($link='../../../'); ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Student Menu</title>
    <?php getHeadTemplate($path='../../'); ?>
    <link rel="stylesheet" href="../../css/main.css">
    <script type="text/javascript" src="../../js/script.js"></script>
  </head>
  <body style="background-color:#fff!important;">

    <div class="container-fluid major-menu">
      <div class="row inner-holder">
        <a href="messages">
        <div class="link" style="background-color:#555;border:2px solid #555;">
          <p>messages</p>
        </div>
        </a>
        <a href="noticeboard">
        <div class="link" style="background-color:#191B4C;border:2px solid #191B4C;">
          <p>noticeboard</p>
        </div>
        </a>
        <a href="students">
        <div class="link" style="background-color:#00aa10;border:2px solid #00aa10;margin-right:8%; margin-top:2%;">
          <p>students</p>
        </div>
        </a>
        <a href="myaccount">
        <div class="link" style="background-color:#006ac4;margin-left:-7%;border:2px solid #006ac4;">
          <p>myaccount</p>
        </div>
        </a>
      </div>
    </div>

  </body>
</html>
