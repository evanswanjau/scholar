<?php require_once '../../../../engine/infused_cogs.php'; teacherSecurity($link='../../../../'); logOut($link='../../../../');?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Student Profile</title>
    <?php getHeadTemplate($path='../../../../'); ?>
    <link rel="stylesheet" href="../../../../css/main.css">
    <script type="text/javascript" src="../../../../js/script.js"></script>
    <style media="screen">
      .parent li{
        width:14%;
      }
    </style>
  </head>
  <body>

    <div class="top-menu">
      <ul>
        <a href="../../"><li style="width:50%;font-size:16px;"><i class="material-icons left"> account_balance</i>&nbsp;&nbsp;Scholar School Management System</li></a>
        <span>
          <a href="../../messages"><li><i class="material-icons top" title="notifications">notifications_none</i></li></a>
          <a href="../../myaccount"><li><i class="material-icons top" title="my account">account_circle</i></li></a>
          <a href='?logout'><li><i class="material-icons top" title="logout">exit_to_app</i></li></a>
        </span>
      </ul>
    </div>

    <div class="container-fluid">
      <div class="row">

        <div class="col-sm-3 side-menu">
          <img src="../../../../images/logo.png" alt="">
          <!-- our menu -->
          <ul>
            <a class="open-class" style="cursor:pointer;"><li class="current"><i class="material-icons">group</i>students</li></a>
            <ul class="class-list"><?php getClassList($link='../'); ?></ul>
            <a href="../../teachers"><li><i class="material-icons">group</i>teachers</li></a>
            <a href="../../noticeboard"><li><i class="material-icons">chat</i>noticeboard</li></a>
            <a href="../../exams/choose-class"><li><i class="material-icons">event_note</i>exams</li></a>
            <a href="../../myaccount"><li><i class="material-icons">account_circle</i>my account</li></a>
          </ul>
        </div>

          <div class="col-sm-9 other-side">

            <h3>Student Profile</h3>
            <?php
            if (isset($_GET['student_id']) && $_GET['student_id'] != '') {
              getStudentProfile($_GET['student_id']);

              $id = $_GET['student_id'];

              $sql = "SELECT * FROM students WHERE student_id = '$id'";
              $result = $conn->query($sql);
              while($row = $result->fetch_assoc()){
                $parent_id = $row['parent_id'];
                $admission_number = $row['admission_number'];
                $medicalinfo = $row['medical_information'];
                $medicalcontact = $row['medical_contact'];
              }
            }else {
              header('location: ../../students');
            }


            if (authorizeParent($parent_id) == true || authorizeStudent($admission_number) == true || isset($_COOKIE['manager']) || isset($_COOKIE['admin']) || isset($_COOKIE['accountant'])) {
              echo "
              <div class='margin'>
                <h4><span>Library Information</span></h4>
                <div class='parent formal'>
                  <ul><b>
                    <span> # </span>
                    <li>issue code</li>
                    <li>book name</li>
                    <li>days remaining</li></b>
                  </ul>
                </div>
              ";

              if (isset($_GET['student_id']) && $_GET['student_id'] != '') {
                getStudentLibraryInfo($_GET['student_id']);
              }else {
                header('location: ../../students');
              }

              echo "</div>";

              echo "
              <div class='margin'>
                <h4><span>Medical Information</span></h4>
                <p><b>Medical Info: </b>$medicalinfo</p>
                <p><b>Medical Contact: </b>$medicalcontact</p>
              ";
            }

            ?>
      </div>
    </div>
  </body>
</html>
