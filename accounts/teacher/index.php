<?php require_once '../../engine/infused_cogs.php';  teacherSecurity($link='../../'); logOut($link='../../../');?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Teacher Menu</title>
    <?php getHeadTemplate($path='../../'); ?>
    <link rel="stylesheet" href="../../css/main.css">
    <script type="text/javascript" src="../../js/script.js"></script>
  </head>
  <body style="background-color:#fff!important;">

    <div class="container-fluid">
      <div class="row major-menu">
        <div class="row inner-holder">
          <a href="messages">
          <div class="link" style="background-color:#555;border:2px solid #555;">
            <p>messages</p>
          </div>
          </a>
          <a href="noticeboard">
          <div class="link" style="background-color:#191B4C;border:2px solid #191B4C;">
            <p>noticeboard</p>
          </div>
          </a>
          <a href="students">
          <div class="link" style="background-color:#00aa10;border:2px solid #00aa10;margin-right:25%;">
            <p>students</p>
          </div>
          </a>
          <a href="exams/choose-class">
          <div class="link" style="background-color:#ff3f3f;border:2px solid #ff3f3f;">
            <p>exams</p>
          </div>
          </a>
          <a href="myaccount">
          <div class="link" style="background-color:#006ac4;margin-top:-10%;border:2px solid #006ac4;">
            <p>myaccount</p>
          </div>
          </a>
        </div>
      </div>
    </div>

  </body>
</html>
