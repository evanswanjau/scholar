<?php require_once '../../../../engine/infused_cogs.php'; teacherSecurity($link='../../../../'); logOut($link='../../../../');?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Manage Exams</title>
    <?php getHeadTemplate($path='../../../../'); ?>
    <link rel="stylesheet" href="../../../../css/main.css">
    <script type="text/javascript" src="../../../../js/script.js"></script>
    <script type="text/javascript" src="../../../../js/uncategorized.js"></script>
  </head>
  <body>

    <div class="top-menu">
      <ul>
        <a href="../"><li style="width:50%;font-size:16px;"><i class="material-icons left"> account_balance</i>&nbsp;&nbsp;Scholar School Management System</li></a>
        <span>
          <a href="#"><li><i class="material-icons top" title="notifications">notifications_none</i></li></a>
          <a href="../myaccount"><li><i class="material-icons top" title="my account">account_circle</i></li></a>
          <a href='?logout'><li><i class="material-icons top" title="logout">exit_to_app</i></li></a>
        </span>
      </ul>
    </div>

    <div class="container-fluid">
      <div class="row">

        <div class="col-sm-3 side-menu">
          <img src="../../../../images/logo.png" alt="">
          <!-- our menu -->
          <ul>
            <a class="open-class" style="cursor:pointer;"><li><i class="material-icons">group</i>students</li></a>
            <ul class="class-list"><?php getClassList($link='../'); ?></ul>
            <a href="../../teachers"><li><i class="material-icons">group</i>teachers</li></a>
            <a href="../../noticeboard"><li><i class="material-icons">chat</i>noticeboard</li></a>
            <a style="cursor:pointer;" class="open-exam-options"><li class="current"><i class="material-icons">event_note</i>exams</li></a>
            <ul class="exam-options">
              <a href="../../exams/choose-class"><li class="current" style='background-color:#191c21;'>manage exams</li></a>
              <a><li class="open-term-list" style="cursor:pointer;background-color:#191c21;">exam performance</li></a>
              <ul class="term-list"><?php getExamDropDown($path='../performance/'); ?></ul>
            </ul>
            <a href="../../myaccount"><li><i class="material-icons">account_circle</i>my account</li></a>
          </ul>
        </div>

          <div class="col-sm-9 other-side">
            <div class="main-message" id="main">
              <p id="main-message"></p>
            </div>
            <?php if (isset($_GET['exam']) && $_GET['exam'] != '' && isset($_GET['class']) && $_GET['class'] != '' && isset($_GET['subject']) && $_GET['subject'] != ''): ?>
              <?php
              $exam_id = $_GET['exam'];
              $class_id = $_GET['class'];
              $subject_id = $_GET['subject'];

              echo '<h3 class="cap">'.getClass($class_id).' Term '. getExamTerm($exam_id) .' '. getSubject($subject_id) .' '. getExam($exam_id) . ' Exams </h3>';
              echo "<div class='view-parents'> <a href='../choose-class'><button class='button right'>choose other class</button></a><br><br><br>";
              echo "
              <div class='parent formal col-sm-12'>
                <ul><b>
                  <span>#</span>
                  <li style='width:10%;'>adm no</li>
                  <li>name</li>
                  <li>subject</li>
                  <li style='width:10%;'>points</li>
                  <li style='width:10%;'>grade</li>
                  <li>added by</li>
                  </b>
                </ul>
              </div>";

              $sql = "SELECT * FROM students WHERE class_id = '$class_id' ORDER BY admission_number ASC";
              $result = $conn->query($sql);

              if($result->num_rows > 0){
                # get fields into variables
                while($row = $result->fetch_assoc()){
                  $count += 1;
                  $studentid = $row['student_id'];
                  $admission_number = $row['admission_number'];
                  $name = $row['name'];

                  getMarks($subject_id, $class_id, $exam_id, $studentid, $count);
                }
              }else {
                echo "<p style='color:grey'>No students in this category</p>";
              }

              echo "</div>";


               ?>
            <?php else: ?>
              <h3>Choose Exam - Class - Subject</h3>
              <!-- choose class form -->
              <form class="ui-form" action="" method="get">
                <select class="dropper" name="exam">
                  <option value="">choose exam</option>
                  <?php getExamOptions(); ?>
                </select><br><br>
                <select class="droppepr" name="class">
                  <option value="">choose class</option>
                  <?php getClassOptions(); ?>
                </select><br><br>
                <select class="" name="subject">
                  <option value="">choose subject</option>
                  <?php getSubjectOptions(); ?>
                </select><br><br>
                <input type="submit" name="submit-details" value="submit">
              </form>
            <?php endif; ?>
          </div>

      </div>
    </div>
  </body>
</html>
