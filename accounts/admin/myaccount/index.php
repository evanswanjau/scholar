<?php
require_once '../../../engine/infused_cogs.php';

adminSecurity($link='../../../');
logOut($link='../../../');

if (isset($_COOKIE['admin'])) {
  $username = $_COOKIE['admin'];

  $sql = "SELECT * FROM admin WHERE username = '$username'";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $count += 1;
    $id = $row['admin_id'];
    $name = $row['name'];
    $user = $row['username'];
    $email = $row['email'];
    $dbpassword = $row['password'];
    $phone = $row['phone_number'];
  }
}

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>My Account</title>
    <?php getHeadTemplate($path='../../../'); ?>
    <link rel="stylesheet" href="../../../css/main.css">
    <script type="text/javascript" src="../../../js/query.js"></script>
  </head>
  <body>

    <div class="top-menu">
      <ul>
        <a href="../"><li style="width:50%;font-size:16px;"><i class="material-icons left"> account_balance</i>&nbsp;&nbsp;Scholar School Management System</li></a>
        <span>
          <a href="#"><li><i class="material-icons right" title="notifications">notifications_none</i></li></a>
          <a href="../myaccount"><li><i class="material-icons right" title="my account">account_circle</i></li></a>
          <a href='?logout'><li><i class="material-icons right" title="logout">exit_to_app</i></li></a>
        </span>
      </ul>
    </div>

    <div class="container-fluid">
      <div class="row">

        <div class="col-sm-3 side-menu">
          <img src="../../../images/logo.png" alt="">
          <!-- our menu -->
          <ul>
            <a class="open-mini-class" style="cursor:pointer;"><li><i class="material-icons">group</i>students</li></a>
            <ul class="mini-class-list">
              <a href="../students/add-student"><li style='padding-left:15%;background-color:#191c21;'>add student</li></a>
              <a class="open-class"><li style='padding-left:15%; cursor:pointer; background-color:#191c21;'>view students</li></a>
            </ul>
            <ul class="class-list"><?php getClassList(); ?></ul>
            <a href="../teachers"><li><i class="material-icons">group</i>teachers</li></a>
            <a href="../parents"><li><i class="material-icons">group</i>parents</li></a>
            <a class="open-this-class" style="cursor:pointer;"><li><i class="material-icons">account_balance</i>classes</li></a>
            <ul class="this-class">
            <a href="../classes/manage-classes"><li style='background-color:#131519; padding-left:15%;'>manage classes</li></a>
            <a href="../classes/manage-sections"><li style='background-color:#131519; padding-left:15%;'>manage sections</li></a>
            <a href="../classes/manage-levels"><li style='background-color:#131519; padding-left:15%;'>manage levels</li></a>
            </ul>
            <a href="../noticeboard"><li><i class="material-icons">chat</i>noticeboard</li></a>
            <a href="../exams"><li><i class="material-icons">event_note</i>exams</li></a>
            <a href="../library"><li><i class="material-icons">library_books</i>library</li></a>
            <a href="../clubs"><li><i class="material-icons">extension</i>clubs</li></a>
            <a href="../dormitories"><li><i class="material-icons">airline_seat_individual_suite</i>dormitories</li></a>
            <a href="../transport"><li><i class="material-icons">directions_bus</i>transport</li></a>
            <a href="../admin-accounts"><li><i class="material-icons">account_box</i>admin accounts</li></a>
            <a href="../myaccount"><li class="current"><i class="material-icons">account_circle</i>my account</li></a>
          </ul>
        </div>

          <div class="col-sm-9 other-side">
            <h3>My Account</h3>

            <!-- edit personal information -->
            <form class="ui-form" action="" method="post">
              <?php updateUserData($account = 'admin', $clause='admin_id', $id); ?>
              <input type="text" name="name" placeholder="full name" value="<?php if (empty($name)){ $name=null;}else {echo $name; } ?>"><br><br>
              <input type="email" name="email" placeholder="email" value="<?php if (empty($email)){ echo null;}else {echo $email; } ?>"><br><br>
              <input type="text" name="phone_number" placeholder="phone_number" value="<?php if (empty($phone)){ echo null;}else {echo $phone; } ?>"><br><br>
              <input type="text" name="username" placeholder="username" value="<?php if (empty($user)){ echo null;}else {echo $user; } ?>"><br><br>
              <input type="submit" name="update-profile" value="update">
            </form>

            <!-- change password -->
            <form class="ui-form" action="" method="post">
              <?php changePassword($dbpassword, $id, $clause='admin', $clausevalue='admin_id'); ?>
              <input type="password" name="current" placeholder="current password"><br><br>
              <input type="password" name="password" placeholder="new password"><br><br>
              <input type="password" name="confirm" placeholder="confirm password"><br><br>
              <input type="submit" name="change-password" value="change password">
            </form>
          </div>
      </div>
    </div>
    <!-- javascript functions -->
    <script type="text/javascript" src="../../../js/script.js"></script>
  </body>
</html>
