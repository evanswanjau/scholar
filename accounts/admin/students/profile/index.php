<?php require_once '../../../../engine/infused_cogs.php';  adminSecurity($link='../../../../'); logOut($link='../../../../');?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Student Profile</title>
    <?php getHeadTemplate($path='../../../../'); ?>
    <link rel="stylesheet" href="../../../../css/main.css">
    <script type="text/javascript" src="../../../../js/script.js"></script>
    <style media="screen">
      .parent li{
        width:14%;
      }
    </style>
  </head>
  <body>

    <div class="top-menu">
      <ul>
        <a href="../../"><li style="width:50%;font-size:16px;"><i class="material-icons left"> account_balance</i>&nbsp;&nbsp;Scholar School Management System</li></a>
        <span>
          <a href="#"><li><i class="material-icons top" title="notifications">notifications_none</i></li></a>
          <a href="../../myaccount"><li><i class="material-icons top" title="my account">account_circle</i></li></a>
          <a href='?logout'><li><i class="material-icons top" title="logout">exit_to_app</i></li></a>
        </span>
      </ul>
    </div>

    <div class="container-fluid">
      <div class="row">

        <div class="col-sm-3 side-menu">
          <img src="../../../../images/logo.png" alt="">
          <!-- our menu -->
          <ul>
            <a class="open-mini-class" style="cursor:pointer;"><li  class="current"><i class="material-icons">group</i>students</li></a>
            <ul class="mini-class-list">
              <a href="../../students/add-student"><li style='padding-left:15%;background-color:#191c21;'>add student</li></a>
              <a class="open-class"><li class="current" style='padding-left:15%; cursor:pointer; background-color:#191c21;'>view students</li></a>
            </ul>
            <ul class="class-list"><?php getClassList($link='../'); ?></ul>
            <a href="../../teachers"><li><i class="material-icons">group</i>teachers</li></a>
            <a href="../../parents"><li><i class="material-icons">group</i>parents</li></a>
            <a class="open-this-class" style="cursor:pointer;"><li><i class="material-icons">account_balance</i>classes</li></a>
            <ul class="this-class">
            <a href="../../classes/manage-classes"><li style='background-color:#131519; padding-left:15%;'>manage classes</li></a>
            <a href="../../classes/manage-sections"><li style='background-color:#131519; padding-left:15%;'>manage sections</li></a>
            <a href="../../classes/manage-levels"><li style='background-color:#131519; padding-left:15%;'>manage levels</li></a>
            </ul>
            <a href="../../noticeboard"><li><i class="material-icons">chat</i>noticeboard</li></a>
            <a href="../../exams"><li><i class="material-icons">event_note</i>exams</li></a>
            <a href="../../library"><li><i class="material-icons">library_books</i>library</li></a>
            <a href="../../clubs"><li><i class="material-icons">extension</i>clubs</li></a>
            <a href="../../dormitories"><li><i class="material-icons">airline_seat_individual_suite</i>dormitories</li></a>
            <a href="../../transport"><li><i class="material-icons">directions_bus</i>transport</li></a>
            <a href="../../admin-accounts"><li><i class="material-icons">account_box</i>admin accounts</li></a>
            <a href="../../myaccount"><li><i class="material-icons">account_circle</i>my account</li></a>
          </ul>
        </div>

          <div class="col-sm-9 other-side">

            <h3>Student Profile</h3>
            <?php
            if (isset($_GET['student_id']) || $_GET['student_id'] != '') {
              getStudentProfile($_GET['student_id']);

              $id = $_GET['student_id'];

              $sql = "SELECT * FROM students WHERE student_id = '$id'";
              $result = $conn->query($sql);
              while($row = $result->fetch_assoc()){
                $parent_id = $row['parent_id'];
                $medicalinfo = $row['medical_information'];
                $medicalcontact = $row['medical_contact'];

                if ($medicalinfo == '') {
                  $medicalinfo = '<span style="color:grey;">Undefined</span>';
                  $medicalcontact = '<span style="color:grey;">Undefined</span>';
                }
              }
            }else {
              header('location: ../../students');
            }

            ?>
            <div class='margin'>
              <h4><span>Library Information</span></h4>
              <div class="parent formal">
                <ul><b>
                  <span> # </span>
                  <li>issue code</li>
                  <li>book name</li>
                  <li>days remaining</li></b>
                </ul>
              </div>
              <?php

              if (isset($_GET['student_id']) || $_GET['student_id'] != '') {
                getStudentLibraryInfo($_GET['student_id']);
              }
               ?>
            </div>

            <?php
            echo "
            <div class='margin'>
              <h4><span>Medical Information</span></h4>
              <p><b>Medical Info: </b>$medicalinfo</p>
              <p><b>Medical Contact: </b>$medicalcontact</p>
              </div>
            ";

              ?>


          </div>

      </div>
    </div>
  </body>
</html>
