<?php require_once '../../../../engine/infused_cogs.php';  adminSecurity($link='../../../../'); logOut($link='../../../../');?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Add Student</title>
    <?php getHeadTemplate($path='../../../../'); ?>
    <link rel="stylesheet" href="../../../../css/main.css">
    <!-- javascript functions -->
    <script type="text/javascript" src="../../../../js/script.js"></script>
    <style media="screen">
      input{
        text-align: center;
        background-color:#fff;
        padding:2%;
        box-shadow: 0 0 3px 2px rgba(0, 0, 0, 0.1);
        margin: auto;
      }
    </style>
  </head>
  <body>

    <div class="top-menu">
      <ul>
        <a href="../"><li style="width:50%;font-size:16px;"><i class="material-icons left"> account_balance</i>&nbsp;&nbsp;Scholar School Management System</li></a>
        <span>
          <a href="#"><li><i class="material-icons top" title="notifications">notifications_none</i></li></a>
          <a href="../myaccount"><li><i class="material-icons top" title="my account">account_circle</i></li></a>
          <a href='?logout'><li><i class="material-icons top" title="logout">exit_to_app</i></li></a>
        </span>
      </ul>
    </div>

    <div class="container-fluid">
      <div class="row">

        <div class="col-sm-3 side-menu">
          <img src="../../../../images/logo.png" alt="">
          <!-- our menu -->
          <ul>
            <a class="open-mini-class" style="cursor:pointer;"><li><i class="material-icons">group</i>students</li></a>
            <ul class="mini-class-list">
              <a href="../../students/add-student"><li style='padding-left:15%;background-color:#191c21;'>add student</li></a>
              <a class="open-class"><li style='padding-left:15%; cursor:pointer; background-color:#191c21;'>view students</li></a>
            </ul>
            <ul class="class-list"><?php getClassList($link='../'); ?></ul>
            <a href="../../teachers"><li><i class="material-icons">group</i>teachers</li></a>
            <a href="../../parents"><li><i class="material-icons">group</i>parents</li></a>
            <a class="open-this-class" style="cursor:pointer;"><li><i class="material-icons">account_balance</i>classes</li></a>
            <ul class="this-class">
            <a href="../../classes/manage-classes"><li style='background-color:#131519; padding-left:15%;'>manage classes</li></a>
            <a href="../../classes/manage-sections"><li style='background-color:#131519; padding-left:15%;'>manage sections</li></a>
            <a href="../../classes/manage-levels"><li style='background-color:#131519; padding-left:15%;'>manage levels</li></a>
            </ul>
            <a href="../../noticeboard"><li><i class="material-icons">chat</i>noticeboard</li></a>
            <a href="../../exams"><li><i class="material-icons">event_note</i>exams</li></a>
            <a href="../../library"><li><i class="material-icons">library_books</i>library</li></a>
            <a href="../../clubs"><li><i class="material-icons">extension</i>clubs</li></a>
            <a href="../../dormitories"><li><i class="material-icons">airline_seat_individual_suite</i>dormitories</li></a>
            <a href="../../transport"><li><i class="material-icons">directions_bus</i>transport</li></a>
            <a href="../../admin-accounts"><li><i class="material-icons">account_box</i>admin accounts</li></a>
            <a href="../../myaccount"><li><i class="material-icons">account_circle</i>my account</li></a>
          </ul>
        </div>

          <div class="col-sm-9 other-side">
            <!-- div to echo out messages -->
            <div class="main-message" id="main">
              <p id="main-message"></p>
            </div>
            <h3>Add New Student</h3>

            <!-- Add New Student -->
            <section>
              <form class="ui-form" action="" method="post" enctype="multipart/form-data">
                <?php addBulkStudents(); ?>
                <p>Choose csv file</p>
                <input type="file" name="excelfile"><br>
                <input type="submit" name="add-bulk-students" value="add bulk students">
              </form>
            </section>
          </div>
      </div>
    </div>
  </body>
</html>
