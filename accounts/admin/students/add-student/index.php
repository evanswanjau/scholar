<?php
require_once '../../../../engine/infused_cogs.php';
adminSecurity($link='../../../../');
logOut($link='../../../../');

if (isset($_GET['studentid']) && $_GET['studentid'] != '') {
  $id = $_GET['studentid'];
  $sql = "SELECT * FROM students WHERE student_id = $id";
  $result = $conn->query($sql);

  # get fields into variables
  while($row = $result->fetch_assoc()){
    $count += 1;
    $adm = $row['admission_number'];
    $name = $row['name'];
    $location = $row['location'];
    $phone = $row['phone'];
    $email = $row['email'];
    $birthday = $row['birthday'];
    $medicalinfo = $row['medical_information'];
    $medicalcontact = $row['medical_contact'];
    $class_id = $row['class_id'];
    $club_id = $row['club_id'];
    $parent_id = $row['parent_id'];
    $dormitory_id = $row['dormitory_id'];
    $transport_id = $row['transport_id'];
  }
}else {
  header('location: ../../students');
}

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Add Student</title>
    <?php getHeadTemplate($path='../../../../'); ?>
    <link rel="stylesheet" href="../../../../css/main.css">
    <!-- javascript functions -->
    <script type="text/javascript" src="../../../../js/script.js"></script>
    <script type="text/javascript" src="../../../../js/uncategorized.js"></script>
  </head>
  <body>

    <div class="top-menu">
      <ul>
        <a href="../"><li style="width:50%;font-size:16px;"><i class="material-icons left"> account_balance</i>&nbsp;&nbsp;Scholar School Management System</li></a>
        <span>
          <a href="#"><li><i class="material-icons top" title="notifications">notifications_none</i></li></a>
          <a href="../myaccount"><li><i class="material-icons top" title="my account">account_circle</i></li></a>
          <a href='?logout'><li><i class="material-icons top" title="logout">exit_to_app</i></li></a>
        </span>
      </ul>
    </div>

    <div class="container-fluid">
      <div class="row">

        <div class="col-sm-3 side-menu">
          <img src="../../../../images/logo.png" alt="">
          <!-- our menu -->
          <ul>
            <a class="open-mini-class" style="cursor:pointer;"><li class="current"><i class="material-icons">group</i>students</li></a>
            <ul class="mini-class-list">
              <a href="../../students/add-student"><li class="current" style='padding-left:15%;background-color:#191c21;'>add student</li></a>
              <a class="open-class"><li style='padding-left:15%; cursor:pointer; background-color:#191c21;'>view students</li></a>
            </ul>
            <ul class="class-list"><?php getClassList($link='../'); ?></ul>
            <a href="../../teachers"><li><i class="material-icons">group</i>teachers</li></a>
            <a href="../../parents"><li><i class="material-icons">group</i>parents</li></a>
            <a class="open-this-class" style="cursor:pointer;"><li><i class="material-icons">account_balance</i>classes</li></a>
            <ul class="this-class">
            <a href="../../classes/manage-classes"><li style='background-color:#131519; padding-left:15%;'>manage classes</li></a>
            <a href="../../classes/manage-sections"><li style='background-color:#131519; padding-left:15%;'>manage sections</li></a>
            <a href="../../classes/manage-levels"><li style='background-color:#131519; padding-left:15%;'>manage levels</li></a>
            </ul>
            <a href="../../noticeboard"><li><i class="material-icons">chat</i>noticeboard</li></a>
            <a href="../../exams"><li><i class="material-icons">event_note</i>exams</li></a>
            <a href="../../library"><li><i class="material-icons">library_books</i>library</li></a>
            <a href="../../clubs"><li><i class="material-icons">extension</i>clubs</li></a>
            <a href="../../dormitories"><li><i class="material-icons">airline_seat_individual_suite</i>dormitories</li></a>
            <a href="../../transport"><li><i class="material-icons">directions_bus</i>transport</li></a>
            <a href="../../admin-accounts"><li><i class="material-icons">account_box</i>admin accounts</li></a>
            <a href="../../myaccount"><li><i class="material-icons">account_circle</i>my account</li></a>
          </ul>
        </div>

          <div class="col-sm-9 other-side">
            <!-- div to echo out messages -->
            <div class="main-message" id="main">
              <p id="main-message"></p>
            </div>
            <?php if (isset($_GET['studentid']) && $_GET['studentid'] != ''): ?>
              <h3>Edit Student</h3>
            <?php else: ?>
              <h3>Add New Student</h3>
            <?php endif; ?>

            <!-- Add New Student -->
            <section>
              <form class="ui-form" action="" method="post">
                <?php addStudent(); editStudent($id);?>
                <input type="text" name="adm" placeholder="admission number" value="<?php if (empty($adm)){ $adm=null;}else {echo $adm; } ?>"><br><br>
                <input type="text" name="name" placeholder="full name" value="<?php if (empty($name)){ $name=null;}else {echo $name; } ?>"><br><br>
                <input type="text" name="phone" placeholder="phone number" value="<?php if (empty($phone)){ $phone=null;}else {echo $phone; } ?>"><br><br>
                <input type="text" name="email" placeholder="email" value="<?php if (empty($email)){ $email=null;}else {echo $email; } ?>"><br><br>
                <input type="text" name="location" placeholder="location" value="<?php if (empty($location)){ $location=null;}else {echo $location; } ?>"><br><br>
                <span style="margin-right:31%;">date of birth</span><br>
                <input type="date" name="birthday" placeholder="birthday" value="<?php if (empty($birthday)){ $birthday=null;}else {echo $birthday; } ?>"><br><br>
                <textarea name="medicalinfo" rows="4" cols="80" placeholder="medical information"><?php if (empty($medicalinfo)){ $medicalinfo=null;}else {echo $medicalinfo; } ?></textarea><br><br>
                <input type="text" name="medicalcontact" placeholder="medical contact" value="<?php if (empty($medicalcontact)){ $medicalcontact=null;}else {echo $medicalcontact; } ?>"><br><br>
                <select class="dropper" name="parent">
                  <option value="<?php if (empty($parent_id)){ echo null;}else {echo $parent_id; } ?>"><?php if (empty($parent_id)){ echo "choose parent" ;}else {echo getParent($parent_id); } ?></option>
                  <?php getParentOptions(); ?>
                </select><br><br>
                <select class="dropper" name="class">
                  <option value="<?php if (empty($class_id)){ echo null;}else {echo $class_id; } ?>"><?php if (empty($class_id)){ echo "choose class" ;}else {echo getClass($class_id); } ?></option>
                  <?php getClassOptions(); ?>
                </select><br><br>
                <select class="dropper" name="club">
                  <option value="<?php if (empty($club_id)){ echo null;}else {echo $club_id; } ?>"><?php if (empty($club_id)){ echo "choose club" ;}else {echo getClub($club_id); } ?></option>
                  <?php getClubOptions(); ?>
                </select><br><br>
                <select class="dropper" name="dormitory">
                  <option value="<?php if (empty($dormitory_id)){ echo null;}else {echo $dormitory_id; } ?>"><?php if (empty($dormitory_id)){ echo "choose class" ;}else {echo getDorm($dormitory_id); } ?></option>
                  <?php getDormOptions(); ?>
                </select><br><br>
                <select class="dropper" name="transport" onchange="return chooseWay()">
                  <option value="<?php if (empty($transport_id)){ echo null;}else {echo $transport_id; } ?>"><?php if (empty($transport_id)){ echo "choose class" ;}else {echo getTransportRoute($transport_id); } ?></option>
                  <?php getRouteOptions(); ?>
                </select><br><br>
                <select class="dropper" name="way" style="visibility:hidden;" id="way">
                  <option value="">choose way</option>
                  <option value="oneway">one way</option>
                  <option value="twoway"> two way</option>
                </select><br><br>
                <?php if (isset($_GET['studentid']) && $_GET['studentid'] != ''): ?>
                  <input type="submit" name="edit-student" value="edit student">
                <?php else: ?>
                  <input type="submit" name="add-student" value="add student">
                <?php endif; ?>
              </form>
            </section>
          </div>
      </div>
    </div>
  </body>
</html>
