<?php
  require_once '../../../../../engine/infused_cogs.php'; accountantSecurity($link='../../../../../'); logOut($link='../../../../../');

 if (isset($_COOKIE['accountant'])) {

   $username = $_COOKIE['accountant'];

   $sql = "SELECT * FROM admin WHERE username = '$username'";
   $result = $conn->query($sql);

   # get fields into variables
   while($row = $result->fetch_assoc()){
     $id = $row['admin_id'];
     $name = $row['name'];
     $user = $row['username'];
     $email = $row['email'];
     $dbpassword = $row['password'];
     $phone = $row['phone_number'];
   }
 }
 ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>My Account</title>
    <?php getHeadTemplate($path='../../../../../'); ?>
    <link rel="stylesheet" href="../../../../../css/main.css">
    <script type="text/javascript" src="../../../../../js/script.js"></script>
  </head>
  <body>

    <div class="top-menu">
      <ul>
        <a href="../"><li style="width:50%;font-size:16px;"><i class="material-icons left"> account_balance</i>&nbsp;&nbsp;Scholar School Management System</li></a>
        <span>
          <li class="fold">School Balance: <b>Ksh <?php echo getSchoolBalance(); ?></b></li>
          <a href="#"><li><i class="material-icons top" title="notifications">notifications_none</i></li></a>
          <a href="../myaccount"><li><i class="material-icons top" title="my account">account_circle</i></li></a>
          <a href='?logout'><li><i class="material-icons top" title="logout">exit_to_app</i></li></a>
        </span>
      </ul>
    </div>

    <div class="container-fluid">
      <div class="row">

        <div class="col-sm-3 side-menu">

          <img src="../../../../../images/logo.png" alt="">
          <!-- our menu -->
          <ul>
            <!-- our menu -->
            <ul>
              <a class="open-class" style="cursor:pointer;"><li><i class="material-icons">group</i>students</li></a>
              <ul class="class-list"><?php getClassList(); ?></ul>
              <a href="../teachers"><li><i class="material-icons">group</i>teachers</li></a>
              <a href="../parents"><li><i class="material-icons">group</i>parents</li></a>
              <a href="../noticeboard"><li><i class="material-icons">chat</i>noticeboard</li></a>
              <a class="open-invoice" style="cursor:pointer;"><li><i class="material-icons">receipt</i>invoice</li></a>
              <ul class="invoice-list">
                <a href="../invoice/invoice-list"><li style='background-color:#131519; padding-left:15%;'>invoice list</li></a>
                <a href="../invoice/invoices"><li style='background-color:#131519; padding-left:15%;'>invoices</li></a>
                <a href="../invoice/special-invoice"><li style='background-color:#131519; padding-left:15%;'>special invoice</li></a>
              </ul>
              <a href="../payments"><li><i class="material-icons">payment</i>payments</li></a>
              <a href="../payroll"><li><i class="material-icons">work</i>payroll</li></a>
              <a href="../petty-cash"><li><i class="material-icons">account_balance_wallet</i>petty cash</li></a>
              <a href="../financial-report"><li><i class="material-icons">content_paste</i>financial report</li></a>
              <a href="../budget"><li><i class="material-icons">attach_money</i>budget</li></a>
              <a href="../vendors"><li><i class="material-icons">group</i>vendors</li></a>
              <a href="../myaccount"><li  class="current"><i class="material-icons">account_circle</i>my account</li></a>
            </ul>
          </ul>
        </div>

        <div class="col-sm-9 other-side">
          <h3>Manage Profile</h3>

          <!-- edit personal information -->
          <form class="ui-form" action="" method="post">
            <?php updateUserData($account = 'admin', $clause='admin_id', $id); ?>
            <input type="text" name="name" placeholder="full name" value="<?php if (empty($name)){ $name=null;}else {echo $name; } ?>"><br><br>
            <input type="email" name="email" placeholder="email" value="<?php if (empty($email)){ echo null;}else {echo $email; } ?>"><br><br>
            <input type="text" name="phone_number" placeholder="phone_number" value="<?php if (empty($phone)){ echo null;}else {echo $phone; } ?>"><br><br>
            <input type="text" name="username" placeholder="username" value="<?php if (empty($user)){ echo null;}else {echo $user; } ?>"><br><br>
            <input type="submit" name="update-profile" value="update">
          </form>

          <!-- change password -->
          <form class="ui-form" action="" method="post">
            <?php changePassword($dbpassword, $id, $clause='admin', $clausevalue='admin_id'); ?>
            <input type="password" name="current" placeholder="current password"><br><br>
            <input type="password" name="password" placeholder="new password"><br><br>
            <input type="password" name="confirm" placeholder="confirm password"><br><br>
            <input type="submit" name="change-password" value="change password">
          </form>
        </div>

      </div>
    </div>
  </body>
</html>
