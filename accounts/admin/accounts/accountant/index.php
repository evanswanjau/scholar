<?php require_once '../../../../engine/infused_cogs.php'; accountantSecurity($link='../../../../'); logOut($link='../../../../');?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Accountant Menu</title>
    <?php getHeadTemplate($path='../../../../'); ?>
    <link rel="stylesheet" href="../../../../css/main.css">
    <script type="text/javascript" src="../../../../js/script.js"></script>
    <style media="screen">
      .major-menu{
        padding:5%;
      }

      .inner-holder{
        height:600px!important;
        width:600px!important;
      }
    </style>
  </head>
  <body style="background-color:#fff!important;">

    <div class="container-fluid major-menu">
      <div class="row inner-holder">
        <a href="students">
        <div class="link" style="background-color:#00aa10;border:2px solid #00aa10; margin-top:-4%; margin-left:26%;position:absolute;">
          <p>students</p>
        </div>
        </a>
        <a href="teachers">
        <div class="link" style="background-color:#555;border:2px solid #555;margin-right:13%;">
          <p>teachers</p>
        </div>
        </a>
        <a href="parents">
        <div class="link" style="background-color:#191B4C;border:2px solid #191B4C;margin-left:12%;margin-top:5%;">
          <p>parents</p>
        </div>
        </a>
        <a href="myaccount">
        <div class="link" style="background-color:#ff3f3f;border:2px solid #ff3f3f;margin-right:13%;">
          <p>myaccount</p>
        </div>
        </a>
        <a href="invoice/invoice-list">
        <div class="link" style="background-color:#006ac4;border:2px solid #006ac4;margin-left:35%;">
          <p>invoice list</p>
        </div>
        </a>
        <a href="noticeboard">
        <div class="link" style="background-color:#7100b9;border:2px solid #7100b9;margin-right:25%;">
          <p>noticeboard</p>
        </div>
        </a>
        <a href="payments">
        <div class="link" style="background-color:#b4a500;border:2px solid #b4a500;margin-left:10%;">
          <p>payments</p>
        </div>
        </a>
        <a href="payroll">
        <div class="link" style="background-color:#f800af;border:2px solid #f800af;position:absolute;margin-top:20%;margin-left:-42%!important;">
          <p>payroll</p>
        </div>
        </a>
        <a href="petty-cash">
        <div class="link" style="background-color:#79240a;border:2px solid #79240a;margin-top:19%;margin-left:-68%!important; position:absolute;">
          <p>petty cash</p>
        </div>
        </a>
      </div>
    </div>

  </body>
</html>
