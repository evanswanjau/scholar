<?php require_once '../../../../../engine/infused_cogs.php'; accountantSecurity($link='../../../../../'); logOut($link='../../../../../');?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Payroll</title>
    <?php getHeadTemplate($path='../../../../../'); ?>
    <link rel="stylesheet" href="../../../../../css/main.css">
    <script type="text/javascript" src="../../../../../js/script.js"></script>
  </head>
  <body>

    <div class="top-menu">
      <ul>
        <a href="../"><li style="width:50%;font-size:16px;"><i class="material-icons left"> account_balance</i>&nbsp;&nbsp;Scholar School Management System</li></a>
        <span>
          <li class="fold">School Balance: <b>Ksh <?php echo getSchoolBalance(); ?></b></li>
          <a href="#"><li><i class="material-icons top" title="notifications">notifications_none</i></li></a>
          <a href="../myaccount"><li><i class="material-icons top" title="my account">account_circle</i></li></a>
          <a href='?logout'><li><i class="material-icons top" title="logout">exit_to_app</i></li></a>
        </span>
      </ul>
    </div>

    <!-- Petty Cash Form -->
    <section>
      <div class="add-parent post-form">
        <form class="ui-form" action="" method="post" name="myForm">
          <div class="material-icons right closeit">close</div>
          <p id="messages"></p>
          <input type="text" name="overhead" placeholder="overhead"><br><br>
          <input type="text" name="amount" placeholder="amount"><br><br>
          <input type="text" name="advance" placeholder="advance"><br><br>
          <input type="text" name="loan" placeholder="loan"><br><br>
          <input type="text" name="nssf" placeholder="NSSF"><br><br>
          <input type="text" name="nhif" placeholder="NHIF"><br><br>
          <input type="text" name="tax" placeholder="Tax"><br><br>
          <input type="text" name="benefits" placeholder="benefits"><br><br>
          <input type="text" name="welfare" placeholder="welfare"><br><br>
          <input type="submit" name="add-payroll" value="add overhead" onclick="return addOverhead()">
        </form>
      </div>
    </section>

    <div class="container-fluid">
      <div class="row">

        <div class="col-sm-3 side-menu">
          <img src="../../../../../images/logo.png" alt="">
          <!-- our menu -->
          <ul>
            <a class="open-class" style="cursor:pointer;"><li><i class="material-icons">group</i>students</li></a>
            <ul class="class-list"><?php getClassList(); ?></ul>
            <a href="../teachers"><li><i class="material-icons">group</i>teachers</li></a>
            <a href="../parents"><li><i class="material-icons">group</i>parents</li></a>
            <a href="../noticeboard"><li><i class="material-icons">chat</i>noticeboard</li></a>
            <a class="open-invoice" style="cursor:pointer;"><li><i class="material-icons">receipt</i>invoice</li></a>
            <ul class="invoice-list">
              <a href="../invoice/invoice-list"><li style='background-color:#131519; padding-left:15%;'>invoice list</li></a>
              <a href="../invoice/invoices"><li style='background-color:#131519; padding-left:15%;'>invoices</li></a>
              <a href="../invoice/special-invoice"><li style='background-color:#131519; padding-left:15%;'>special invoice</li></a>
            </ul>
            <a href="../payments"><li><i class="material-icons">payment</i>payments</li></a>
            <a href="../payroll"><li class="current"><i class="material-icons">work</i>payroll</li></a>
            <a href="../petty-cash"><li><i class="material-icons">account_balance_wallet</i>petty cash</li></a>
            <a href="../financial-report"><li><i class="material-icons">content_paste</i>financial report</li></a>
            <a href="../budget"><li><i class="material-icons">attach_money</i>budget</li></a>
            <a href="../vendors"><li><i class="material-icons">group</i>vendors</li></a>
            <a href="../myaccount"><li><i class="material-icons">account_circle</i>my account</li></a>
          </ul>
        </div>

          <div class="col-sm-9 other-side">
            <h3>Payroll</h3>

            <!-- display all the overheads -->
            <section>
              <div class="view-parents">
                <button class="button right show-form">make new payment</button>
                <div class="payroll-table">
                  <table>
                    <tr>
                      <th>#</th>
                      <th>overhead</th>
                      <th>amount</th>
                      <th>advance</th>
                      <th>loan</th>
                      <th>nhif</th>
                      <th>nssf</th>
                      <th>tax</th>
                      <th>total deductions</th>
                      <th>benefits</th>
                      <th>total additions</th>
                      <th>amount payable</th>
                      <th>balance</th>
                      <th>received</th>
                      <th>date</th>
                      <th>edit</th>
                      <th>delete</th>
                    </tr>
                    <?php getPayroll(); ?>
                  </table>
                </div>
              </div>
            </section>
          </div>

      </div>
    </div>
  </body>
</html>
