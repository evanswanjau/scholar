<?php require_once '../../../../../engine/infused_cogs.php'; accountantSecurity($link='../../../../../'); logOut($link='../../../../../');?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Student Payments</title>
    <?php getHeadTemplate($path='../../../../../'); ?>
    <link rel="stylesheet" href="../../../../../css/main.css">
    <script type="text/javascript" src="../../../../../js/script.js"></script>
    <script type="text/javascript" src="../../../../../js/payment.js"></script>
    <style type="text/css" media="print">
      @page { size: landscape; }
    </style>
  </head>
  <body>

    <div class="top-menu">
      <ul>
        <a href="../"><li style="width:50%;font-size:16px;"><i class="material-icons left"> account_balance</i>&nbsp;&nbsp;Scholar School Management System</li></a>
        <span>
          <li class="fold">School Balance: <b>Ksh <?php echo getSchoolBalance(); ?></b></li>
          <a href="#"><li><i class="material-icons top" title="notifications">notifications_none</i></li></a>
          <a href="../myaccount"><li><i class="material-icons top" title="my account">account_circle</i></li></a>
          <a href='?logout'><li><i class="material-icons top" title="logout">exit_to_app</i></li></a>
        </span>
      </ul>
    </div>

    <!-- Receipt Form-->
    <section>
      <div class="add-parent receipt-form">
        <div class="ui-form" id="printableArea">
          <div class="print">
            <div class="material-icons right closeit nonPrintable">close</div>
            <h3>JUHUDI SECONDARY SCHOOL</h3>
            <p>P.O. BOX 65 KIAMBU</p>
            <p>CALL 0788445522</p>
            <h3 class="bold">PAYMENT RECEIPT</h3><br>
            <p><b>ACCOUNT NO: </b> <span id="account"></span></p>
            <p><b>REF NO: </b> <span id="ref"></span></p>
            <p><b>STUDENT NAME: </b><span id="name"></span></p>
            <p><b>AMOUNT PAID: </b><span id="amount"></span></p>
            <p><b>PURPOSE: </b><span id="purpose"></span></p>
            <p><b>METHOD OF PAYMENT: </b><span id="method"></span></p>
            <p><b>DATE PAID: </b><span id="date"></span></p>
            <br>
            <input type="submit" class="nonPrintable" onclick="printDiv()" value="print receipt"/>
            <h6>Scholar School Management System</h6>
          </div>
        </div>
      </div>
    </section>

    <!-- Form to add the parents -->
    <section>
      <div class="add-parent post-form">
        <form class="ui-form" action="" method="post" name="myForm">
          <div class="material-icons right closeit">close</div>
          <p id="messages"></p>
          <input type="text" name="studentsearch" placeholder="search student" onkeyup="getAllStudentList(this.value)"><br><br>
          <div class="issue-div">
            <p id="student_issued"></p>
          </div>
          <br><br>
          <input type="hidden" name="accountid" value="">
          <input type="text" name="adm" placeholder="admission number"><br><br>
          <input type="text" name="name" placeholder="student name"><br><br>
          <input type="text" name="amount" placeholder="amount"><br><br>
          <select class="dropper" name="purpose">
            <option value=""> - choose option - </option>
            <?php purposeDropdown(); ?>
          </select><br><br>
          <select class="dropper" name="method">
            <option value="">- method of payment -</option>
            <option value="cash">cash</option>
            <option value="receipt">bank slip</option>
          </select><br><br>
          <input type="submit" name="make-payment" value="make Payment" onclick="return makePayment()">
        </form>
      </div>
    </section>

    <div class="container-fluid">
      <div class="row">

        <div class="col-sm-3 side-menu">
          <img src="../../../../../images/logo.png" alt="">
          <!-- our menu -->
          <ul>
            <a class="open-class" style="cursor:pointer;"><li><i class="material-icons">group</i>students</li></a>
            <ul class="class-list"><?php getClassList($link='../students/'); ?></ul>
            <a href="../teachers"><li><i class="material-icons">group</i>teachers</li></a>
            <a href="../parents"><li><i class="material-icons">group</i>parents</li></a>
            <a href="../noticeboard"><li><i class="material-icons">chat</i>noticeboard</li></a>
            <a class="open-invoice" style="cursor:pointer;"><li><i class="material-icons">receipt</i>invoice</li></a>
            <ul class="invoice-list">
              <a href="../invoice/invoice-list"><li style='background-color:#131519; padding-left:15%;'>invoice list</li></a>
              <a href="../invoice/invoices"><li style='background-color:#131519; padding-left:15%;'>invoices</li></a>
              <a href="../invoice/special-invoice"><li style='background-color:#131519; padding-left:15%;'>special invoice</li></a>
            </ul>
            <a href="../payments"><li class="current"><i class="material-icons">payment</i>payments</li></a>
            <a href="../payroll"><li><i class="material-icons">work</i>payroll</li></a>
            <a href="../petty-cash"><li><i class="material-icons">account_balance_wallet</i>petty cash</li></a>
            <a href="../financial-report"><li><i class="material-icons">content_paste</i>financial report</li></a>
            <a href="../budget"><li><i class="material-icons">attach_money</i>budget</li></a>
            <a href="../vendors"><li><i class="material-icons">group</i>vendors</li></a>
            <a href="../myaccount"><li><i class="material-icons">account_circle</i>my account</li></a>
          </ul>
        </div>

          <div class="col-sm-9 other-side">
            <!-- div to echo out messages -->
            <div class="main-message" id="main">
              <p id="main-message"></p>
            </div>
            <h3>Student Payments</h3>

            <!-- display all the parent information-->
            <section>
              <div class="view-parents">
                <button class="button right show-form">make new payment</button>
                <br><br>
                <div class="parent formal">
                  <ul><b>
                    <span> # </span>
                    <li style="width:10%;">account no</li>
                    <li style="width:10%;">ref no</li>
                    <li>account name</li>
                    <li>purpose</li>
                    <li>amount paid</li>
                    <li>date paid</li>
                  </b>
                  </ul>
                </div>
                <?php getAllPayments(); ?>
              </div>
            </section>
          </div>

      </div>
    </div>
  </body>
</html>
