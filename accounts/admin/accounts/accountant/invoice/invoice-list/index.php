<?php require_once '../../../../../../engine/infused_cogs.php'; accountantSecurity($link='../../../../../../'); logOut($link='../../../../../../');?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Invoice Items</title>
    <?php getHeadTemplate($path='../../../../../../'); ?>
    <link rel="stylesheet" href="../../../../../../css/main.css">
    <script type="text/javascript" src="../../../../../../js/script.js"></script>
    <script type="text/javascript" src="../../../../../../js/invoice_items.js"></script>
  </head>
  <body>

    <div class="top-menu">
      <ul>
        <a href="../"><li style="width:50%;font-size:16px;"><i class="material-icons left"> account_balance</i>&nbsp;&nbsp;Scholar School Management System</li></a>
        <span>
          <li class="fold">School Balance: <b>Ksh <?php echo getSchoolBalance(); ?></b></li>
          <a href="#"><li><i class="material-icons top" title="notifications">notifications_none</i></li></a>
          <a href="../myaccount"><li><i class="material-icons top" title="my account">account_circle</i></li></a>
          <a href='?logout'><li><i class="material-icons top" title="logout">exit_to_app</i></li></a>
        </span>
      </ul>
    </div>

    <!-- Form to delete the parents -->
    <section>
      <div class="delete-form">
        <form class="ui-form" action="" method="post" name="deleteForm">
          <div class="material-icons right closeit">close</div>
          <i class="material-icons" style="color:#f14949; font-size:50px;">error</i>
          <h3>Are you sure you want to delete this item?</h3><br>
          <input type="hidden" name="id" value="">
          <button class="button yes" type="button" name="delete" style="background-color:#f14949;"onclick="return deleteItem()">yes</button>
          <button class="button no" type="button" name="button">no</button>
        </form>
      </div>
    </section>

    <!-- Form to add the parents -->
    <section>
      <div class="add-parent post-form">
        <form class="ui-form" action="" method="post" name="myForm">
          <div class="material-icons right closeit">close</div>
          <p id="messages"></p>
          <input type="hidden" name="id" value="">
          <input type="text" name="item" placeholder="item"><br><br>
          <input type="text" name="price" placeholder="price"><br><br>
          <input class='add' type="submit" name="add-item" value="add item" onclick="return addItem()">
          <input class="edit" type="submit" name="edit-item" value="edit item" onclick="return editItem()">
        </form>
      </div>
    </section>

    <div class="container-fluid">
      <div class="row">

        <div class="col-sm-3 side-menu">
          <img src="../../../../../../images/logo.png" alt="">
          <!-- our menu -->
          <ul>
            <a class="open-class" style="cursor:pointer;"><li><i class="material-icons">group</i>students</li></a>
            <ul class="class-list"><?php getClassList($link='../../students/'); ?></ul>
            <a href="../../teachers"><li><i class="material-icons">group</i>teachers</li></a>
            <a href="../../parents"><li><i class="material-icons">group</i>parents</li></a>
            <a href="../../noticeboard"><li><i class="material-icons">chat</i>noticeboard</li></a>
            <a class="open-invoice" style="cursor:pointer;"><li class="current"><i class="material-icons">receipt</i>invoice</li></a>
            <ul class="invoice-list">
              <a href="../../invoice/invoice-list"><li class="current" style='background-color:#131519; padding-left:15%;'>invoice list</li></a>
              <a href="../../invoice/invoices"><li style='background-color:#131519; padding-left:15%;'>invoices</li></a>
              <a href="../../invoice/special-invoice"><li style='background-color:#131519; padding-left:15%;'>special invoice</li></a>
            </ul>
            <a href="../../payments"><li><i class="material-icons">payment</i>payments</li></a>
            <a href="../../payroll"><li><i class="material-icons">work</i>payroll</li></a>
            <a href="../../petty-cash"><li><i class="material-icons">account_balance_wallet</i>petty cash</li></a>
            <a href="../../financial-report"><li><i class="material-icons">content_paste</i>financial report</li></a>
            <a href="../../budget"><li><i class="material-icons">attach_money</i>budget</li></a>
            <a href="../../vendors"><li><i class="material-icons">group</i>vendors</li></a>
            <a href="../../myaccount"><li><i class="material-icons">account_circle</i>my account</li></a>
          </ul>
        </div>

          <div class="col-sm-9 other-side">
            <!-- div to echo out messages -->
            <div class="main-message" id="main">
              <p id="main-message"></p>
            </div>
            <h3>Invoice list</h3>

            <!-- display all the parent information-->
            <section>
              <div class="view-parents">
                <button class="button right show-form">add new item</button>
                <div class="parent formal">
                  <ul><b>
                    <span> # </span>
                    <li>item</li>
                    <li>cost</li></b>
                  </ul>
                </div>
                <?php getInvoiceItems(); ?>
              </div>
            </section>
          </div>

      </div>
    </div>
  </body>
</html>
