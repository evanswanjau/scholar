<?php require_once '../../../../../engine/infused_cogs.php'; managerSecurity($link='../../../../../'); logOut($link='../../../../../');?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Teachers</title>
    <?php getHeadTemplate($path='../../../../../'); ?>
    <link rel="stylesheet" href="../../../../../css/main.css">
    <script type="text/javascript" src="../../../../../js/script.js"></script>
  </head>
  <body>

    <div class="top-menu">
      <ul>
        <a href="../"><li style="width:50%;font-size:16px;"><i class="material-icons left"> account_balance</i>&nbsp;&nbsp;Scholar School Management System</li></a>
        <span>
          <a href="#"><li><i class="material-icons top" title="notifications">notifications_none</i></li></a>
          <a href="../myaccount"><li><i class="material-icons top" title="my account">account_circle</i></li></a>
          <a href='?logout'><li><i class="material-icons top" title="logout">exit_to_app</i></li></a>
        </span>
      </ul>
    </div>

    <div class="container-fluid">
      <div class="row">

        <div class="col-sm-3 side-menu">
          <img src="../../../../../images/logo.png" alt="">
          <!-- our menu -->
          <ul>
            <a href="../dashboard"><li class="current"><i class="material-icons">dashboard</i>dashboard</li></a>
            <a href="../noticeboard"><li><i class="material-icons">chat</i>noticeboard</li></a>
            <a href="../financial-report"><li><i class="material-icons">content_paste</i>financial report</li></a>
            <a href="../budget"><li><i class="material-icons">attach_money</i>budget</li></a>
            <a href="../myaccount"><li><i class="material-icons">account_circle</i>my account</li></a>
          </ul>
        </div>

          <div class="col-sm-9 other-side">
            <div class="col-sm-12">
              <div class="row order">
                <div class="col-sm-3 quick">
                  <h3>students</h3><br>
                  <img src="../../../../../images/admin2.png" alt="students"><br><br>
                  <h1><?php getCount('students') ?></h1>
                </div>
                <div class="col-sm-3 quick">
                  <h3>teachers</h3><br>
                  <img src="../../../../../images/admin2.png" alt="teachers"><br><br>
                  <h1><?php getCount('teachers') ?></h1>
                </div>
                <div class="col-sm-3 quick">
                  <h3>parents</h3><br>
                  <img src="../../../../../images/admin2.png" alt="parents"><br><br>
                  <h1><?php getCount('parents') ?></h1>
                </div>
                <div class="col-sm-3 quick">
                  <h3>admins</h3><br>
                  <img src="../../../../../images/admin2.png" alt="admins"><br><br>
                  <h1><?php getCount('admin') ?></h1>
                </div>
              </div>
              <div class="row order">
                <div class="col-sm-12 balance">
                  <h1><b><i class="material-icons left" style="font-size:1.2em;">attach_money</i>SCHOOL BALANCE: </b>Ksh <?php echo getSchoolBalance(); ?></h1>
                </div>
              </div>
              <div class="row today-money">
                <div class="col-sm-6 today" style='background-color:#82ffac;'>
                  <h1><img src="../../../../../images/daily.png" alt="daily earnings" ><span class="right"><h2>Today's Earnings: Ksh <?php echo getTodayIncome(); ?></h2></span></h1>
                </div>
                <div class="col-sm-6 today" style='background-color:#f79797'>
                  <h1><img src="../../../../../images/daily.png" alt="daily expenditure"><span class="right"><h2>Today's Expenditure: Ksh <?php echo getTodayExpenditure(); ?></h2></span></h1>
                </div>
              </div>
            </div>
          </div>

      </div>
    </div>
  </body>
</html>
