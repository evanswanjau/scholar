<?php require_once '../../../../../engine/infused_cogs.php'; managerSecurity($link='../../../../../'); logOut($link='../../../../../');?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Financial Report</title>
    <?php getHeadTemplate($path='../../../../../'); ?>
    <link rel="stylesheet" href="../../../../../css/main.css">
    <script type="text/javascript" src="../../../../../js/script.js"></script>
  </head>
  <body>

    <div class="top-menu">
      <ul>
        <a href="../"><li style="width:50%;font-size:16px;"><i class="material-icons left"> account_balance</i>&nbsp;&nbsp;Scholar School Management System</li></a>
        <span>
          <a href="#"><li><i class="material-icons top" title="notifications">notifications_none</i></li></a>
          <a href="../myaccount"><li><i class="material-icons top" title="my account">account_circle</i></li></a>
          <a href='?logout'><li><i class="material-icons top" title="logout">exit_to_app</i></li></a>
        </span>
      </ul>
    </div>

    <div class="container-fluid">
      <div class="row">

        <div class="col-sm-3 side-menu">
          <img src="../../../../../images/logo.png" alt="">
          <!-- our menu -->
          <ul>
            <a href="../dashboard"><li><i class="material-icons">dashboard</i>dashboard</li></a>
            <a href="../noticeboard"><li><i class="material-icons">chat</i>noticeboard</li></a>
            <a href="../financial-report"><li class="current"><i class="material-icons">content_paste</i>financial report</li></a>
            <a href="../budget"><li><i class="material-icons">attach_money</i>budget</li></a>
            <a href="../myaccount"><li><i class="material-icons">account_circle</i>my account</li></a>
          </ul>
        </div>

          <div class="col-sm-9 other-side">
            <h3>Financial Report</h3>

            <!-- display all the overheads -->
            <section>
              <div class="view-parents">
                <?php getFinancialReport(); ?>
              </div>
            </section>
          </div>

      </div>
    </div>
  </body>
</html>
