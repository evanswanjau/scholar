<?php require_once '../../../../engine/infused_cogs.php'; librarianSecurity($link='../../../../'); logOut($link='../../../../');?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Librarian Menu</title>
    <?php getHeadTemplate($path='../../../../'); ?>
    <link rel="stylesheet" href="../../../../css/main.css">
    <script type="text/javascript" src="../../../../js/script.js"></script>
  </head>
  <body style="background-color:#fff!important;">

    <div class="container-fluid major-menu">
      <div class="row inner-holder">
        <a href="noticeboard">
        <div class="link" style="background-color:#191B4C;border:2px solid #191B4C;">
          <p>noticeboard</p>
        </div>
        </a>
        <a href="students">
        <div class="link" style="background-color:#00aa10;border:2px solid #00aa10;margin-left:5%;">
          <p>students</p>
        </div>
        </a>
        <a href="library/books">
        <div class="link" style="background-color:#ff3f3f;border:2px solid #ff3f3f;margin-right:35%;">
          <p>books</p>
        </div>
        </a>
        <a href="library/borrowed-books">
        <div class="link" style="background-color:#007f81;border:2px solid #007f81;">
          <p>borrowed books</p>
        </div>
        </a>
        <a href="library/borrow-history">
        <div class="link" style="background-color:#555;border:2px solid #555;margin-top:5%;margin-right:4%;">
          <p>borrow history</p>
        </div>
        </a>
        <a href="myaccount">
        <div class="link" style="background-color:#006ac4;border:1px solid #006ac4;margin-top:-10%;">
          <p>myaccount</p>
        </div>
        </a>
      </div>
    </div>

  </body>
</html>
