<?php require_once '../../../../../../engine/infused_cogs.php'; librarianSecurity($link='../../../../../../'); logOut($link='../../../../../../');?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Library - Borrow History</title>
    <?php getHeadTemplate($path='../../../../../../'); ?>
    <link rel="stylesheet" href="../../../../../../css/main.css">
  </head>
  <body>

    <div class="top-menu">
      <ul>
        <a href="../../"><li style="width:50%;font-size:16px;"><i class="material-icons left"> account_balance</i>&nbsp;&nbsp;Scholar School Management System</li></a>
        <span>
          <a href="#"><li><i class="material-icons top" title="notifications">notifications_none</i></li></a>
          <a href="../../myaccount"><li><i class="material-icons top" title="my account">account_circle</i></li></a>
          <a href='?logout'><li><i class="material-icons top" title="logout">exit_to_app</i></li></a>
        </span>
      </ul>
    </div>

    <div class="container-fluid">
      <div class="row">

        <div class="col-sm-3 side-menu">
          <img src="../../../../../../images/logo.png" alt="">
          <!-- our menu -->
          <ul>
            <a class="open-class" style="cursor:pointer;"><li><i class="material-icons">group</i>students</li></a>
            <ul class="class-list"><?php getClassList($link='../../students/'); ?></ul>
            <a href="../../noticeboard"><li><i class="material-icons">chat</i>noticeboard</li></a>
            <a class="open-books" style="cursor:pointer;"><li class="current"><i class="material-icons">library_books</i>library</li></a>
            <ul class="book-list">
              <a href="../../library/books"><li style='background-color:#131519; padding-left:15%;'>books</li></a>
              <a href="../../library/borrowed-books"><li style='background-color:#131519; padding-left:15%;'>borrowed books</li></a>
              <a href="../../library/borrow-history"><li class="current" style='background-color:#131519; padding-left:15%;'>borrow history</li></a>
            </ul>
            <a href="../../myaccount"><li><i class="material-icons">account_circle</i>my account</li></a>
          </ul>
        </div>

          <div class="col-sm-9 other-side">
            <!-- div to echo out messages -->
            <div class="main-message" id="main">
              <p id="main-message"></p>
            </div>
            <h3>Library - Borrow History</h3>

            <!-- display all the parent information -->
            <section>
              <div class="view-parents">
                <div class="parent formal">
                  <ul><b>
                    <span> # </span>
                    <li>issue code</li>
                    <li>book name</li>
                    <li style="width:10%">adm no</li>
                    <li>student name</li>
                    <li>date Issued</li>
                    <li>status</li>
                  </b>
                  </ul>
                </div>
                <?php getBorrowHistory(); ?>
              </div>
            </section>
          </div>
      </div>
    </div>
    <!-- javascript functions -->
    <script type="text/javascript" src="../../../../../../js/script.js"></script>
    <script type="text/javascript" src="../../../../../../js/library.js"></script>
  </body>
</html>
