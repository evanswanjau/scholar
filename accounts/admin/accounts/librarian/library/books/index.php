<?php require_once '../../../../../../engine/infused_cogs.php'; librarianSecurity($link='../../../../../../'); logOut($link='../../../../../../');?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Library Books</title>
    <?php getHeadTemplate($path='../../../../../../'); ?>
    <link rel="stylesheet" href="../../../../../../css/main.css">
    <!-- javascript functions -->
    <script type="text/javascript" src="../../../../../../js/script.js"></script>
    <script type="text/javascript" src="../../../../../../js/library.js"></script>
  </head>
  <body>


    <div class="top-menu">
      <ul>
        <a href="../../"><li style="width:50%;font-size:16px;"><i class="material-icons left"> account_balance</i>&nbsp;&nbsp;Scholar School Management System</li></a>
        <span>
          <a href="#"><li><i class="material-icons top" title="notifications">notifications_none</i></li></a>
          <a href="../myaccount"><li><i class="material-icons top" title="my account">account_circle</i></li></a>
          <a href='?logout'><li><i class="material-icons top" title="logout">exit_to_app</i></li></a>
        </span>
      </ul>
    </div>

    <div class="container-fluid">
      <div class="row">

        <!-- Form to delete the parents -->
        <section>
          <div class="delete-form">
            <form class="ui-form" action="" method="post" name="deleteForm">
              <div class="material-icons right closeit">close</div>
              <i class="material-icons" style="color:#f14949; font-size:50px;">error</i>
              <h3>Are you sure you want to delete this book?</h3><br>
              <input type="hidden" name="id" value="">
              <button class="button yes" type="button" name="delete" style="background-color:#f14949;"onclick="return deleteBook()">yes</button>
              <button class="button no" type="button" name="button">no</button>
            </form>
          </div>
        </section>

        <!-- Form to add the parents -->
        <section>
          <div class="add-book post-form">
            <form class="ui-form" action="" method="post" name="myForm">
              <div class="material-icons right closeit">close</div>
              <p id="messages"></p>
              <input type="hidden" name="id" value="">
              <input type="text" name="name" placeholder="book name"><br><br>
              <input type="text" name="author" placeholder="author"><br><br>
              <textarea name="description" rows="4" cols="30" placeholder="Please write a short descrition"></textarea><br><br>
              <input type="text" name="price" placeholder="price"><br><br>
              <input type="text" name="count" placeholder="number of books"><br><br>
              <input class="add" type="submit" name="add-book" value="add book" onclick="return addBook()">
              <input class="edit" type="submit" name="edit-book" value="edit book" onclick="return editBook()">
            </form>
          </div>
        </section>

        <!-- Form to add the parents -->
        <section>
          <div class="issue-form">
            <form class="ui-form" action="" method="post" name="issueForm">
              <div class="material-icons right closeit">close</div>
              <p id="issuemessages"></p>
              <input type="text" name="studentsearch" placeholder="search student" onkeyup="getAllStudentList(this.value)"><br><br>
              <div class="issue-div">
                <p id="student_issued"></p>
              </div>
              <br><br>
              <input type="hidden" name="accountid" value="">
              <input type="text" name="adm" placeholder="admission number"><br><br>
              <input type="text" name="studentname" placeholder="student name"><br><br>
              <input type="hidden" name="studentid" value="">
              <input type="hidden" name="bookid" value="">
              <input type="hidden" name="serialcode" value="">
              <div class="issue-div">
                <p id="code" class="cap"></p>
                <p id="bookname" class="cap"></p>
                <p id="author" class="cap"></p>
              </div><br>
              <input class="add" type="submit" name="issue-book" value="issue book" onclick="return issueBook()">
            </form>
          </div>
        </section>

        <div class="col-sm-3 side-menu">
          <img src="../../../../../../images/logo.png" alt="">
          <!-- our menu -->
          <ul>
            <a class="open-class" style="cursor:pointer;"><li><i class="material-icons">group</i>students</li></a>
            <ul class="class-list"><?php getClassList($link='../../students/'); ?></ul>
            <a href="../../noticeboard"><li><i class="material-icons">chat</i>noticeboard</li></a>
            <a class="open-books" style="cursor:pointer;"><li class="current"><i class="material-icons">library_books</i>library</li></a>
            <ul class="book-list">
              <a href="../../library/books"><li class="current" style='background-color:#131519; padding-left:15%;'>books</li></a>
              <a href="../../library/borrowed-books"><li style='background-color:#131519; padding-left:15%;'>borrowed books</li></a>
              <a href="../../library/borrow-history"><li style='background-color:#131519; padding-left:15%;'>borrow history</li></a>
            </ul>
            <a href="../../myaccount"><li><i class="material-icons">account_circle</i>my account</li></a>
          </ul>
        </div>

          <div class="col-sm-9 other-side">
            <!-- div to echo out messages -->
            <div class="main-message" id="main">
              <p id="main-message"></p>
            </div>
            <h3>Library</h3>

            <!-- display all the parent information -->
            <section>
              <div class="view-parents">
                <button class="button right show-form">add new book</button>
                <br><br>
                <div class="parent formal">
                  <ul><b>
                    <span> # </span>
                    <li>name</li>
                    <li>author</li>
                    <li>description</li>
                    <li style="width:10%;">price</li>
                    <li style='width:8%;'>original count</li>
                    <li style='width:8%;'>book count</li>
                    <li style='width:8%;'>issued</li>
                  </b>
                  </ul>
                </div>
                <?php getBooks(); ?>
              </div>
            </section>
          </div>
      </div>
    </div>
  </body>
</html>
