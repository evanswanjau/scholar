<?php
 require_once '../../../../../engine/infused_cogs.php';

librarianSecurity($link='../../../../../');
logOut($link='../../../../../');

 if (isset($_COOKIE['librarian'])) {
   $username = $_COOKIE['librarian'];

   $sql = "SELECT * FROM admin WHERE username = '$username'";
   $result = $conn->query($sql);

   # get fields into variables
   while($row = $result->fetch_assoc()){
     $count += 1;
     $id = $row['admin_id'];
     $name = $row['name'];
     $user = $row['username'];
     $email = $row['email'];
     $dbpassword = $row['password'];
     $phone = $row['phone_number'];
   }
 }
 ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>My Account</title>
    <?php getHeadTemplate($path='../../../../../'); ?>
    <link rel="stylesheet" href="../../../../../css/main.css">
    <script type="text/javascript" src="../../../../../js/script.js"></script>
  </head>
  <body>

    <div class="top-menu">
      <ul>
        <a href="../"><li style="width:50%;font-size:16px;"><i class="material-icons left"> account_balance</i>&nbsp;&nbsp;Scholar School Management System</li></a>
        <span>
          <a href="#"><li><i class="material-icons top" title="notifications">notifications_none</i></li></a>
          <a href="../myaccount"><li><i class="material-icons top" title="my account">account_circle</i></li></a>
          <a href='?logout'><li><i class="material-icons top" title="logout">exit_to_app</i></li></a>
        </span>
      </ul>
    </div>

    <div class="container-fluid">
      <div class="row">

        <div class="col-sm-3 side-menu">

          <img src="../../../../../images/logo.png" alt="">
          <!-- our menu -->
          <ul>
            <a class="open-class" style="cursor:pointer;"><li><i class="material-icons">group</i>students</li></a>
            <ul class="class-list"><?php getClassList($link=''); ?></ul>
            <a href="../noticeboard"><li><i class="material-icons">chat</i>noticeboard</li></a>
            <a class="open-books" style="cursor:pointer;"><li><i class="material-icons">library_books</i>library</li></a>
            <ul class="book-list">
              <a href="../library/books"><li style='background-color:#131519; padding-left:15%;'>books</li></a>
              <a href="../library/borrowed-books"><li style='background-color:#131519; padding-left:15%;'>borrowed books</li></a>
              <a href="../library/borrow-history"><li style='background-color:#131519; padding-left:15%;'>borrow history</li></a>
            </ul>
            <a href="../myaccount"><li class="current"><i class="material-icons">account_circle</i>my account</li></a>>
          </ul>
        </div>

        <div class="col-sm-9 other-side">
          <h3>Manage Profile</h3>

          <!-- edit personal information -->
          <form class="ui-form" action="" method="post">
            <?php updateUserData($account = 'admin', $clause='admin_id', $id); ?>
            <input type="text" name="name" placeholder="full name" value="<?php if (empty($name)){ $name=null;}else {echo $name; } ?>"><br><br>
            <input type="email" name="email" placeholder="email" value="<?php if (empty($email)){ echo null;}else {echo $email; } ?>"><br><br>
            <input type="text" name="phone_number" placeholder="phone_number" value="<?php if (empty($phone)){ echo null;}else {echo $phone; } ?>"><br><br>
            <input type="text" name="username" placeholder="username" value="<?php if (empty($user)){ echo null;}else {echo $user; } ?>"><br><br>
            <input type="submit" name="update-profile" value="update">
          </form>

          <!-- change password -->
          <form class="ui-form" action="" method="post">
            <?php changePassword($dbpassword, $id, $clause='admin', $clausevalue='admin_id'); ?>
            <input type="password" name="current" placeholder="current password"><br><br>
            <input type="password" name="password" placeholder="new password"><br><br>
            <input type="password" name="confirm" placeholder="confirm password"><br><br>
            <input type="submit" name="change-password" value="change password">
          </form>
        </div>

      </div>
    </div>
  </body>
</html>
