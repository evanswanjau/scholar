<?php require_once '../../../engine/infused_cogs.php';  adminSecurity($link='../../../'); logOut($link='../../../');?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Clubs</title>
    <?php getHeadTemplate($path='../../../'); ?>
    <link rel="stylesheet" href="../../../css/main.css">
    <script type="text/javascript" src="../../../js/club.js"></script>
    <script type="text/javascript" src="../../../js/script.js"></script>
  </head>
  <body>

    <div class="top-menu">
      <ul>
        <a href="../"><li style="width:50%;font-size:16px;"><i class="material-icons left"> account_balance</i>&nbsp;&nbsp;Scholar School Management System</li></a>
        <span>
          <a href="#"><li><i class="material-icons top" title="notifications">notifications_none</i></li></a>
          <a href="../myaccount"><li><i class="material-icons top" title="my account">account_circle</i></li></a>
          <a href='?logout'><li><i class="material-icons top" title="logout">exit_to_app</i></li></a>
        </span>
      </ul>
    </div>

    <!-- Form to delete the parents -->
    <section>
      <div class="delete-form">
        <form class="ui-form" action="" method="post" name="deleteForm">
          <div class="material-icons right closeit">close</div>
          <i class="material-icons" style="color:#f14949; font-size:50px;">error</i>
          <h3>Are you sure you want to delete this club?</h3><br>
          <input type="hidden" name="id" value="">
          <button class="button yes" type="button" name="delete" style="background-color:#f14949;"onclick="return deleteClub()">yes</button>
          <button class="button no" type="button" name="button">no</button>
        </form>
      </div>
    </section>

    <!-- Form to add the parents -->
    <section>
      <div class="add-parent post-form">
        <form class="ui-form" action="" method="post" name="myForm">
          <div class="material-icons right closeit">close</div>
          <p id="messages"></p>
          <input type="hidden" name="id" value="">
          <input type="text" name="name" placeholder="club name"><br><br>
          <textarea name="description" rows="3" cols="80" placeholder="write a short descrption"></textarea><br><br>
          <select class="dropper" name="chair">
            <option value="">choose chairman</option>
            <?php getStudentOptions(); ?>
          </select><br><br>
          <select class="dropper" name="vice">
            <option value="">choose vice chairman</option>
            <?php getStudentOptions(); ?>
          </select><br><br>
          <select class="dropper" name="patron">
            <option value="">choose patron</option>
            <?php getTeacherOptions(); ?>
          </select><br><br>
          <input class="add" type="submit" name="add-club" value="add club" onclick="return addClub()">
          <input class="edit" type="submit" name="edit-club" value="edit club" onclick="return editClub()">
        </form>
      </div>
    </section>

    <div class="container-fluid">
      <div class="row">

        <div class="col-sm-3 side-menu">
          <img src="../../../images/logo.png" alt="">
          <!-- our menu -->
          <ul>
            <a class="open-mini-class" style="cursor:pointer;"><li><i class="material-icons">group</i>students</li></a>
            <ul class="mini-class-list">
              <a href="../students/add-student"><li style='padding-left:15%;background-color:#191c21;'>add student</li></a>
              <a class="open-class"><li style='padding-left:15%; cursor:pointer; background-color:#191c21;'>view students</li></a>
            </ul>
            <ul class="class-list"><?php getClassList(); ?></ul>
            <a href="../teachers"><li><i class="material-icons">group</i>teachers</li></a>
            <a href="../parents"><li><i class="material-icons">group</i>parents</li></a>
            <a class="open-this-class" style="cursor:pointer;"><li><i class="material-icons">account_balance</i>classes</li></a>
            <ul class="this-class">
            <a href="../classes/manage-classes"><li style='background-color:#131519; padding-left:15%;'>manage classes</li></a>
            <a href="../classes/manage-sections"><li style='background-color:#131519; padding-left:15%;'>manage sections</li></a>
            <a href="../classes/manage-levels"><li style='background-color:#131519; padding-left:15%;'>manage levels</li></a>
            </ul>
            <a href="../noticeboard"><li><i class="material-icons">chat</i>noticeboard</li></a>
            <a href="../exams"><li><i class="material-icons">event_note</i>exams</li></a>
            <a href="../library"><li><i class="material-icons">library_books</i>library</li></a>
            <a href="../clubs"><li class="current"><i class="material-icons">extension</i>clubs</li></a>
            <a href="../dormitories"><li><i class="material-icons">airline_seat_individual_suite</i>dormitories</li></a>
            <a href="../transport"><li><i class="material-icons">directions_bus</i>transport</li></a>
            <a href="../admin-accounts"><li><i class="material-icons">account_box</i>admin accounts</li></a>
            <a href="../myaccount"><li><i class="material-icons">account_circle</i>my account</li></a>
          </ul>
        </div>

          <div class="col-sm-9 other-side">
            <!-- div to echo out messages -->
            <div class="main-message" id="main">
              <p id="main-message"></p>
            </div>
            <h3>Clubs</h3>

            <!-- display all the parent information -->
            <section>
              <div class="view-parents">
                <?php
                if (isset($_GET['club']) && $_GET['club'] != '') {
                  echo "
                  <button class='button right show-form'>add new club</button>
                  <br><br>
                  <div class='parent formal'>
                    <ul><b>
                      <span> # </span>
                      <li>name</li>
                      <li style='width:10%;'>adm no</li>
                      <li>email</li>
                      <li style='width:10%;'>phone number</li>
                    </b>
                    </ul>
                  </div>
                  ";
                  getStudents($admin=false, $clause='club_id', $_GET['club'], $link='../students');

                }else {
                  getClubs();
                }

                ?>
              </div>
            </section>
          </div>
      </div>
    </div>
  </body>
</html>
