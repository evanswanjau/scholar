
<?php require_once '../engine/infused_cogs.php';?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Sign Up</title>
    <?php getHeadTemplate('../'); ?>
    <link rel="stylesheet" href="../css/main.css">
    <script type="text/javascript" src="../js/query.js"></script>
  </head>
  <body>

    <!-- Sign Up page -->
    <section>
      <div class="container-fluid welcome-to-login">
        <div class="row welcome-section">
          <div class="col-sm-7 text">
            <h1>Welcome to Scholar</h1>
            <p>School Management System</p>
          </div>

          <!-- Login Form -->
          <div class="col-sm-5 form">
            <form class="ui-form" action="" method="post">
              <h4>create an account</h4>
              <?php signUp();?>
              <p>use email / phone number / admission number (for students)</p>
              <input type="text" name="username" placeholder="email, phonenumber, admission number" value="<?php if (isset($_SESSION['user'])) {echo $_SESSION['user'];} ?>"><br><br>
              <p>create password</p>
              <input type="password" name="password" placeholder="create a password"><br><br>
              <input type="password" name="confirm" placeholder="confirm password"><br><br>
              <input type="submit" name="signup" value="sign up"><br><br>
              <p>Already have an account? <a href="../">login now</a></p>
            </form>
          </div>
        </div>
      </div>
    </section>
  </body>
</html>
